package com.ck.it.androidapps.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.main.SalesActivity;
import com.goka.blurredgridmenu.GridMenu;
import com.goka.blurredgridmenu.GridMenuFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Creef on 12/12/2017.
 */

public class MonitoringFragment extends Fragment {
    private View view;
    private GridMenuFragment mGridMenuFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.monitoring_fragment, container, false);
        initview();
        return view;
    }

    void initview() {
        mGridMenuFragment = GridMenuFragment.newInstance(R.drawable.splash);
        ((FragmentActivity) view.getContext()).getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, mGridMenuFragment)
                .addToBackStack(null)
                .commit();
        setupGrdMenu();
        mGridMenuFragment.setOnClickMenuListener(new GridMenuFragment.OnClickMenuListener() {
            @Override
            public void onClickMenu(GridMenu gridMenu, int position) {
                if (gridMenu.getTitle().equals("Sales")) {
                    startActivityForResult(new Intent(view.getContext(), SalesActivity.class), 0);
                }
            }
        });
    }

    private void setupGrdMenu() {
        List<GridMenu> menus = new ArrayList<>();
        menus.add(new GridMenu("Sales", R.drawable.ico));
        mGridMenuFragment.setupMenu(menus);
    }
}
