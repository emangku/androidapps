package com.ck.it.androidapps.fragment.FS;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;


@SuppressWarnings("deprecation")
public class FSInvestasiFragments extends Fragment implements View.OnClickListener{
    private View view;
    EditText viewRent, viewRenovation, viewEquipment;
    TextView viewTotal;
    Button viewBtnInvestasi;
    int Rent = 0;
    int Renovation = 0;
    int Equipment = 0;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fs_form_fragment, container, false);
        init();
        return view;
    }

    void init(){
        getActivity().setTitle("FS Investasi");
        viewTotal = (TextView) view.findViewById(R.id.viewTotal);
        viewRent = (EditText) view.findViewById(R.id.viewRent);
        viewRent.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                viewTotal.setText("");
                addTotalInvest();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @SuppressLint("SetTextI18n")
            @Override
            public void afterTextChanged(Editable s) {
              if (Rent != 0 && viewRent.getText().length() >= 0){
                  Rent = Integer.parseInt(viewRent.getText().toString());
                  viewTotal.setText(Integer.toString(Rent));
              }else{
                  Rent = 0;
                  viewTotal.setText(Integer.toString(Rent));
              }
            }
        });
        viewRenovation = (EditText) view.findViewById(R.id.viewRenovation);
        viewRenovation.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                viewTotal.setText(Integer.toString(Rent));
                addTotalInvest();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Renovation != 0 && Rent != 0 && viewRenovation.getText().length() >=0){
//                    addTotalInvest();
                    Rent = Integer.parseInt(viewRent.getText().toString());
                    Renovation = Integer.parseInt(viewRenovation.getText().toString());
                    viewTotal.setText(Integer.toString(Rent + Renovation));
                }else{
                    Renovation = 0;
                    viewTotal.setText(Integer.toString(Rent + Renovation));
                }

            }
        });
        viewEquipment = (EditText) view.findViewById(R.id.viewEquipment);
        viewEquipment.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                viewTotal.setText(Integer.toString(Rent + Renovation));
                addTotalInvest();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (Equipment != 0 && Renovation != 0 && Rent != 0 && viewRenovation.getText().length() >= 0){
//                    addTotalInvest();
                    Rent = Integer.parseInt(viewRent.getText().toString());
                    Renovation = Integer.parseInt(viewRenovation.getText().toString());
                    Equipment = Integer.parseInt(viewEquipment.getText().toString());
                    viewTotal.setText(Integer.toString(Rent + Renovation + Equipment));
                }else{
                    Equipment = 0;
                    viewTotal.setText(Integer.toString(Rent + Renovation + Equipment));
                }

            }
        });
        viewBtnInvestasi = (Button) view.findViewById(R.id.viewBtnInvestasi);
        viewBtnInvestasi.setOnClickListener(this);
    }

    private String addTotalInvest(){

        if (viewRent.getText().toString() != ""  && viewRent.getText().length() > 0){
            Rent = Integer.parseInt(viewRent.getText().toString());
        }else{
            Rent = 0;
        }

        if (viewRenovation.getText().toString() != "" && viewRenovation.getText().length() > 0){
            Renovation = Integer.parseInt(viewRenovation.getText().toString());
        }else {
            Renovation = 0;
        }

        if (viewEquipment.getText().toString() != "" && viewEquipment.getText().length() > 0){
            Equipment = Integer.parseInt(viewEquipment.getText().toString());
        }else {
            Equipment = 0;
        }

        return Integer.toString(Rent + Renovation + Equipment);
    }
    void saveInvestasi(String url){
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(),"Tunggu...");
        dialog.show();
        AndroidNetworking.post(api.SERVER_API+url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(),"sessionValKeySiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(),"sessionUserID"))
                .addBodyParameter(params.RENT, viewRent.getText().toString())
                .addBodyParameter(params.RENOVATION, viewRenovation.getText().toString())
                .addBodyParameter(params.EQUIPMENT, viewEquipment.getText().toString())
                .addBodyParameter(params.TOTAL_INVESTMENT, viewTotal.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
                                ((FragmentActivity)view.getContext())
                                        .getSupportFragmentManager()
                                        .beginTransaction()
                                        .addToBackStack(null)
                                        .replace(R.id.viewFS,new FSIncomFragments())
                                        .commit();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(),"Opps.. Error "+anError.getMessage(), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewBtnInvestasi:
                if (TextUtils.isEmpty(viewRent.getText().toString())){
                    viewRent.setError("Tidak boleh Kosong...!!");
                    viewRent.requestFocus();
                }else if (TextUtils.isEmpty(viewRenovation.getText().toString())){
                    viewRenovation.setError("Tidak boleh Kosong...!!");
                    viewRenovation.requestFocus();
                }else if (TextUtils.isEmpty(viewEquipment.getText().toString())){
                    viewEquipment.setError("Tidak boleh Kosong...!!");
                    viewEquipment.requestFocus();
                }else if (TextUtils.isEmpty(viewTotal.getText().toString())){
                    Toast.makeText(view.getContext(),"Total Kosong..", Toast.LENGTH_LONG).show();
                }else{
                    saveInvestasi(params.URL_METHOD.SAVE_INVESTASI);
                }
                break;
        }
    }

}
