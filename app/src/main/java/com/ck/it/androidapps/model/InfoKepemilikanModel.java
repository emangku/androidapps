package com.ck.it.androidapps.model;

/**
 * Created by Creef on 12/17/2017.
 */

public class InfoKepemilikanModel {
    private String SITE_CODE;
    private String USER_ID;
    private String NAMA_PEMILIK;
    private String BADAN_HUKUM;
    private String ALAMAT;
    private String NO_TELP_FAX_HO_HP;
    private String EMAIL;

    public InfoKepemilikanModel() {
    }

    public InfoKepemilikanModel(String SITE_CODE, String USER_ID, String NAMA_PEMILIK, String BADAN_HUKUM, String ALAMAT, String NO_TELP_FAX_HO_HP, String EMAIL) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.NAMA_PEMILIK = NAMA_PEMILIK;
        this.BADAN_HUKUM = BADAN_HUKUM;
        this.ALAMAT = ALAMAT;
        this.NO_TELP_FAX_HO_HP = NO_TELP_FAX_HO_HP;
        this.EMAIL = EMAIL;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getNAMA_PEMILIK() {
        return NAMA_PEMILIK;
    }

    public void setNAMA_PEMILIK(String NAMA_PEMILIK) {
        this.NAMA_PEMILIK = NAMA_PEMILIK;
    }

    public String getBADAN_HUKUM() {
        return BADAN_HUKUM;
    }

    public void setBADAN_HUKUM(String BADAN_HUKUM) {
        this.BADAN_HUKUM = BADAN_HUKUM;
    }

    public String getALAMAT() {
        return ALAMAT;
    }

    public void setALAMAT(String ALAMAT) {
        this.ALAMAT = ALAMAT;
    }

    public String getNO_TELP_FAX_HO_HP() {
        return NO_TELP_FAX_HO_HP;
    }

    public void setNO_TELP_FAX_HO_HP(String NO_TELP_FAX_HO_HP) {
        this.NO_TELP_FAX_HO_HP = NO_TELP_FAX_HO_HP;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }
}
