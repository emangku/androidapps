package com.ck.it.androidapps.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.main.BDEquipmentActivity;
import com.ck.it.androidapps.activity.main.BudgetActivity;
import com.ck.it.androidapps.activity.main.CE_BOQKonstruksiActivity;
import com.ck.it.androidapps.activity.main.DesignLayoutActivity;
import com.ck.it.androidapps.activity.main.DocPerjanjianActivity;
import com.ck.it.androidapps.activity.main.LegalAnalisaActivity;
import com.ck.it.androidapps.activity.main.MDEquipmentActivity;
import com.ck.it.androidapps.activity.main.MOU_PT_RSActivity;
import com.ck.it.androidapps.activity.main.MarketingActivity;
import com.ck.it.androidapps.activity.main.SPKActivity;
import com.ck.it.androidapps.fragment.FS.fs_history.FSHistoryActivity;
import com.goka.blurredgridmenu.GridMenu;
import com.goka.blurredgridmenu.GridMenuFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Creef on 12/11/2017.
 */

public class ProjectPlaningFragment extends Fragment {
    private View view;
    private GridMenuFragment mGridMenuFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.project_planning, container, false);
        initview();
        return view;
    }

    void initview() {
        mGridMenuFragment = GridMenuFragment.newInstance(R.drawable.splash);
        ((FragmentActivity) view.getContext()).getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, mGridMenuFragment)
                .addToBackStack(null)
                .commit();
        setupGrdMenu();
        mGridMenuFragment.setOnClickMenuListener(new GridMenuFragment.OnClickMenuListener() {
            @Override
            public void onClickMenu(GridMenu gridMenu, int position) {
                if (gridMenu.getTitle().equals("MOU")) {
                    startActivityForResult(new Intent(view.getContext(), MOU_PT_RSActivity.class), 0);
                } else if (gridMenu.getTitle().equals("Legal Analisis")) {
                    startActivityForResult(new Intent(view.getContext(), LegalAnalisaActivity.class), 1);
                } else if (gridMenu.getTitle().equals("Design Layout")) {
                    startActivityForResult(new Intent(view.getContext(), DesignLayoutActivity.class), 2);
                } else if (gridMenu.getTitle().equals("CE-BOQ")) {
                    startActivityForResult(new Intent(view.getContext(), CE_BOQKonstruksiActivity.class), 3);
                } else if (gridMenu.getTitle().equals("BD Equipment")) {
                    startActivityForResult(new Intent(view.getContext(), BDEquipmentActivity.class), 4);
                } else if (gridMenu.getTitle().equals("MD Equipment")) {
                    startActivityForResult(new Intent(view.getContext(), MDEquipmentActivity.class), 5);
                } else if (gridMenu.getTitle().equals("Marketing")) {
                    startActivityForResult(new Intent(view.getContext(), MarketingActivity.class), 6);
                } else if (gridMenu.getTitle().equals("Budget")) {
                    startActivityForResult(new Intent(view.getContext(), BudgetActivity.class), 7);
                } else if (gridMenu.getTitle().equals("Final FS")) {
                    startActivityForResult(new Intent(view.getContext(), FSHistoryActivity.class), 8);
                } else if (gridMenu.getTitle().equals("SPK")) {
                    startActivityForResult(new Intent(view.getContext(), SPKActivity.class), 9);
                } else if (gridMenu.getTitle().equals("Doc Perjanjian")) {
                    startActivityForResult(new Intent(view.getContext(), DocPerjanjianActivity.class), 10);
                }
            }
        });
    }

    private void setupGrdMenu() {
        List<GridMenu> menus = new ArrayList<>();
        menus.add(new GridMenu("MOU", R.drawable.ico));
        menus.add(new GridMenu("Legal Analisis", R.drawable.ico));
        menus.add(new GridMenu("Design Layout", R.drawable.ico));
        menus.add(new GridMenu("CE-BOQ", R.drawable.ico));
        menus.add(new GridMenu("BD Equipment", R.drawable.ico));
        menus.add(new GridMenu("MD Equipment", R.drawable.ico));
        menus.add(new GridMenu("Marketing", R.drawable.ico));
        menus.add(new GridMenu("Budget", R.drawable.ico));
        menus.add(new GridMenu("Final FS", R.drawable.ico));
        menus.add(new GridMenu("SPK", R.drawable.ico));
        menus.add(new GridMenu("Doc Perjanjian", R.drawable.ico));
        mGridMenuFragment.setupMenu(menus);
    }
}
