package com.ck.it.androidapps.fragment.siteanalysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.util.utils;

/**
 * Created by Creef on 12/6/2017.
 */

public class ParkirFragment extends Fragment implements View.OnClickListener {
    private View view;
    String[] kapasitasparkir = {"-Kapasitas Parkir-","Tidak Ada","1-2  mobil","2-4  mobil","4-6 mobil"};
    ArrayAdapter<String> adapter;
    Spinner viewKapasitasParkir;
    Button viewNextParkir;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.parkir_fragment,container,false);
        initView();
        return view;
    }
    void initView(){
        viewNextParkir = (Button)view.findViewById(R.id.viewNextParkir);
        viewNextParkir.setOnClickListener(this);

        viewKapasitasParkir = (Spinner) view.findViewById(R.id.viewKapasitasParkir);
        adapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner, R.id.txtSpinner,kapasitasparkir);
        viewKapasitasParkir.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextParkir:
                Fragment fragment = new AksesFragment();
                getActivity().setTitle("Akses");
                utils.showFragmentTwo(view.getContext(),fragment);
                break;
        }
    }
}
