package com.ck.it.androidapps.activity.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.adapter.AdapterGalleryView;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.model.ModelGalleryView;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;
import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.GalleryPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import gun0912.tedbottompicker.TedBottomPicker;

@SuppressWarnings("deprecation")
public class LegalAnalisaActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    AlertDialog.Builder builder, builders;
    File file, vFile;
    String opt = "";
    CameraPhoto cameraPhoto;
    GalleryPhoto galleryPhoto;
    public static int CAMERA_REQUEST = 1100;
    public static int GALLERY_REQUEST = 2200;
    String url;
    private RecyclerView rv_gallery;
    private List<ModelGalleryView> list;
    private AdapterGalleryView adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView viewDataKosongViewLigal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_legal_analisa);
        setTitle("Legal");
        initView();
    }

    void initView() {
        cameraPhoto = new CameraPhoto(getApplicationContext());
        galleryPhoto = new GalleryPhoto(getApplicationContext());
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        viewDataKosongViewLigal = (TextView) findViewById(R.id.viewDataKosongViewLigal);
        showDataGalery("get_legal_all_by_docname");
    }

    void showDataGalery(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(LegalAnalisaActivity.this, params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.DOC_NAME, "Legal")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                list = new ArrayList<ModelGalleryView>();
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    ModelGalleryView modelGalleryView = new ModelGalleryView();
                                    JSONObject object = array.getJSONObject(i);
                                    modelGalleryView.setUSER_ID(object.getString("USER_ID"));
                                    modelGalleryView.setDOC_NAME(object.getString("DOC_NAME"));
                                    modelGalleryView.setDOCUMENT(object.getString("DOCUMENT"));
                                    modelGalleryView.setCreated_at(object.getString("created_at"));
                                    modelGalleryView.setUpdated_at(object.getString("updated_at"));
                                    list.add(modelGalleryView);
                                }
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("Error : " + e.fillInStackTrace());
                            dialog.dismiss();
                        }

                        rv_gallery = (RecyclerView) findViewById(R.id.rv_gallery);
                        rv_gallery.setLayoutManager(new GridLayoutManager(LegalAnalisaActivity.this, 2));
                        if (list == null) {
                            viewDataKosongViewLigal.setVisibility(View.VISIBLE);
                            mSwipeRefreshLayout.setVisibility(View.GONE);
                        } else {
                            adapter = new AdapterGalleryView(LegalAnalisaActivity.this, list);
                            rv_gallery.setAdapter(adapter);
                            adapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(LegalAnalisaActivity.this, "opps error : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public void onRefresh() {
        showDataGalery("get_legal_all_by_docname");
        mSwipeRefreshLayout.setRefreshing(false);
    }

    void showGalery(final ImageView img) {

        new TedBottomPicker.Builder(LegalAnalisaActivity.this)
                .setOnImageSelectedListener(new TedBottomPicker.OnImageSelectedListener() {
                    @Override
                    public void onImageSelected(Uri uri) {

                        try {
                            final InputStream inputStream = getContentResolver().openInputStream(uri);
                            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                            img.setImageBitmap(bitmap);
                            url = inputStream.toString();
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }

                        file = new File(URI.create(uri.toString()));

                    }
                }).create()
                .show(getSupportFragmentManager());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.doc, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        final View view = LayoutInflater.from(this).inflate(R.layout.uploads_doc_fragement, null, false);
        LayoutInflater inflater = getLayoutInflater();
        final ImageView viewDocuments;
        final ProgressBar viewProgress;
        LinearLayout viewDOCUploads, viewDOCBatal;
        final AppCompatTextView viewtitle;
        final RadioGroup viewGroupRadioLegal;
        Spinner viewDocOptions;
        if (id == R.id.optionUploads) {
            opt = "Legal";
            builder = new AlertDialog.Builder(this);
            builder.setView(view);
            viewDOCUploads = (LinearLayout) view.findViewById(R.id.viewDOCUploads);
            viewDOCBatal = (LinearLayout) view.findViewById(R.id.viewDOCBatal);
            viewProgress = (ProgressBar) view.findViewById(R.id.viewProgress);
            viewtitle = (AppCompatTextView) view.findViewById(R.id.viewtitle);
            viewDocuments = (ImageView) view.findViewById(R.id.viewDocuments);
            viewDocuments.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showGalery(viewDocuments);
                }
            });
            final AlertDialog dialog = builder.create();
            viewDOCUploads.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    viewProgress.setVisibility(View.VISIBLE);
                    viewtitle.setVisibility(View.GONE);
                    AndroidNetworking.upload(api.SERVER_API + "docuploads")
                            .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                            .addMultipartParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionValKeySiteCode"))
                            .addMultipartParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                            .addMultipartParameter(params.DOC_NAME, "Legal")
                            .addMultipartFile(params.DOCUMENT, file)
                            .build()
                            .getAsJSONObject(new JSONObjectRequestListener() {
                                @Override
                                public void onResponse(JSONObject response) {
                                    try {
                                        if (response.getString("status").equals("true")) {
                                            viewProgress.setVisibility(View.GONE);
                                            viewtitle.setVisibility(View.VISIBLE);
                                            onRefresh();
                                            dialog.dismiss();
                                        }
                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                        viewProgress.setVisibility(View.GONE);
                                        viewtitle.setVisibility(View.VISIBLE);
                                    }
                                }

                                @Override
                                public void onError(ANError anError) {
                                    Toast.makeText(LegalAnalisaActivity.this, "Opps Error : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                                    System.out.println("ERROR : " + anError.toString());
                                    viewProgress.setVisibility(View.GONE);
                                    viewtitle.setVisibility(View.VISIBLE);
                                }
                            });

                }
            });
            viewDOCBatal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            dialog.show();
        }
        return super.onOptionsItemSelected(item);
    }

    void viewoption() {
        final View vw = LayoutInflater.from(this).inflate(R.layout.camera_opt_dialog, null, false);
        builders = new AlertDialog.Builder(this);
        builders.setView(vw);
        LinearLayout viewGalery = (LinearLayout) vw.findViewById(R.id.viewGalery);
        LinearLayout viewCamera = (LinearLayout) vw.findViewById(R.id.viewCamera);
        viewCamera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent intent = cameraPhoto.takePhotoIntent();
                    startActivityForResult(intent, CAMERA_REQUEST);
                    cameraPhoto.addToGallery();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
        viewGalery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(galleryPhoto.openGalleryIntent(), GALLERY_REQUEST);
            }
        });
        final AlertDialog mDialog = builders.create();
        mDialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {
                String photoPath = cameraPhoto.getPhotoPath();
                try {
                    Bitmap bitmap = ImageLoader.init().from(photoPath).requestSize(512, 512).getBitmap();
                    GETImages(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            } else if (requestCode == GALLERY_REQUEST) {
                galleryPhoto.setPhotoUri(data.getData());
                String photoPath = galleryPhoto.getPath();
                try {
                    Bitmap bitmap = ImageLoader.init().from(photoPath).requestSize(512, 512).getBitmap();
//                    imageView.setImageBitmap(bitmap);
//                    selectedImage = photoPath;
                    GETImages(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    void GETImages(Bitmap bitmap) {
        View view = LayoutInflater.from(this).inflate(R.layout.uploads_doc_fragement, null, false);
        ImageView img = (ImageView) view.findViewById(R.id.viewDocuments);
        img.setImageBitmap(bitmap);
    }
}
