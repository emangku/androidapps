package com.ck.it.androidapps.adapter;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.model.siteanalysismodel.SiteAnalysisModel;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SiteAnalysisAdapter extends RecyclerView.Adapter<SiteAnalysisAdapter.ViewHolder> {
    View view;
    Context mContext;
    List<SiteAnalysisModel> mlist = new ArrayList<>();
    String opt = "";

    public SiteAnalysisAdapter(Context context, List<SiteAnalysisModel> list) {
        this.mContext = context;
        this.mlist = list;
    }

    @Override
    public SiteAnalysisAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mContext).inflate(R.layout.list_site_analysis, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SiteAnalysisAdapter.ViewHolder holder, final int position) {
        holder.viewTipe.setText(mlist.get(position).getTYPE());
        holder.viewTypeOption.setText(mlist.get(position).getVALUE_OPTION());
        holder.viewKondisi.setText(mlist.get(position).getVALUE_RANGE());
        holder.viewClickUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utils.setDataSession(view.getContext(), "sessionID", mlist.get(position).getID());
                final View mView = LayoutInflater.from(view.getContext()).inflate(R.layout.site_analysis_dialog_update, null, false);
                RadioGroup viewGroupEditSiteA;
                final AppCompatTextView viewIDId, viewIDSITECODE, viewIDUSERID, viewIDSA, viewIDTYPE;
                viewIDId = (AppCompatTextView) mView.findViewById(R.id.viewIDId);
                viewIDId.setText(mlist.get(position).getID());

                viewIDSITECODE = (AppCompatTextView) mView.findViewById(R.id.viewIDSITECODE);
                viewIDSITECODE.setText(mlist.get(position).getSITE_CODE());

                viewIDUSERID = (AppCompatTextView) mView.findViewById(R.id.viewIDUSERID);
                viewIDUSERID.setText(mlist.get(position).getUSER_ID());

                viewIDSA = (AppCompatTextView) mView.findViewById(R.id.viewIDSA);
                viewIDSA.setText(mlist.get(position).getSA_ID());

                viewIDTYPE = (AppCompatTextView) mView.findViewById(R.id.viewIDTYPE);
                viewIDTYPE.setText(mlist.get(position).getTYPE());

                final MaterialEditText viewIDValueOptions;
                viewIDValueOptions = (MaterialEditText) mView.findViewById(R.id.viewIDValueOptions);
                viewIDValueOptions.setText(mlist.get(position).getVALUE_OPTION());

                viewGroupEditSiteA = (RadioGroup) mView.findViewById(R.id.viewGroupEditSiteA);
                final LinearLayout viewIDCancleSiteA, viewIDUpdateSiteA, viewFrmLayout;
                viewIDCancleSiteA = (LinearLayout) mView.findViewById(R.id.viewIDCancleSiteA);
                viewIDUpdateSiteA = (LinearLayout) mView.findViewById(R.id.viewIDUpdateSiteA);
                viewFrmLayout = (LinearLayout) mView.findViewById(R.id.viewFrmLayout);
                final ProgressBar viewDialogSiteAnalysis;
                viewDialogSiteAnalysis = (ProgressBar) mView.findViewById(R.id.viewDialogSiteAnalysis);
                final AlertDialog dialog = utils.customeAlertInputDialog(view.getContext(), mView);
                viewGroupEditSiteA.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                        switch (checkedId) {
                            case R.id.edit_sa_1:
                                opt = "1";
                                break;
                            case R.id.edit_sa_2:
                                opt = "2";
                                break;
                            case R.id.edit_sa_3:
                                opt = "3";
                                break;
                            case R.id.edit_sa_4:
                                opt = "4";
                                break;
                            case R.id.edit_sa_5:
                                opt = "5";
                                break;
                        }
                    }
                });
                viewIDCancleSiteA.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                viewIDUpdateSiteA.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewDialogSiteAnalysis.setVisibility(View.VISIBLE);
                        viewFrmLayout.setVisibility(View.GONE);
                        AndroidNetworking.post(api.SERVER_API + "update_site_analysisID")
                                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                                .addBodyParameter("id", viewIDId.getText().toString())
                                .addBodyParameter(params.SITE_CODE, viewIDSITECODE.getText().toString())
                                .addBodyParameter(params.USER_ID, viewIDUSERID.getText().toString())
                                .addBodyParameter(params.SA_ID, viewIDSA.getText().toString())
                                .addBodyParameter(params.TYPE, viewIDTYPE.getText().toString())
                                .addBodyParameter(params.VALUE_OPTION, viewIDValueOptions.getText().toString())
                                .addBodyParameter(params.VALUE_RANGE, opt)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            if (response.getString("status").equals("true")) {
                                                dialog.cancel();
                                                viewFrmLayout.setVisibility(View.VISIBLE);
                                                viewDialogSiteAnalysis.setVisibility(View.GONE);
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            dialog.cancel();
                                            viewFrmLayout.setVisibility(View.VISIBLE);
                                            viewDialogSiteAnalysis.setVisibility(View.GONE);
                                        }
                                    }

                                    @Override
                                    public void onError(ANError anError) {
                                        Toast.makeText(view.getContext(), "Opps Error", Toast.LENGTH_LONG).show();
                                        dialog.cancel();
                                        viewFrmLayout.setVisibility(View.VISIBLE);
                                        viewDialogSiteAnalysis.setVisibility(View.GONE);
                                    }
                                });
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mlist != null) {
            return mlist.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView viewTipe, viewKondisi, viewTypeOption;
        LinearLayout viewClickUpdate;

        public ViewHolder(View itemView) {
            super(itemView);
            viewTipe = (TextView) view.findViewById(R.id.viewType);
            viewTypeOption = (TextView) view.findViewById(R.id.viewTypeOption);
            viewKondisi = (TextView) view.findViewById(R.id.viewKondisi);
            viewClickUpdate = (LinearLayout) itemView.findViewById(R.id.viewClickUpdate);
        }
    }
}
