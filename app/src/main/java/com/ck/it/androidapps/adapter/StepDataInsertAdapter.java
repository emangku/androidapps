package com.ck.it.androidapps.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.ck.it.androidapps.fragment.DataInsertFragment;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

/**
 * Created by Creef on 12/5/2017.
 */

public class StepDataInsertAdapter extends AbstractFragmentStepAdapter {
    static String CURRENT_STEP_POSITION_KEY = "test";
    static Context context;

    public StepDataInsertAdapter(@NonNull FragmentManager fm, @NonNull Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(@IntRange(from = 0L) int position) {
        final DataInsertFragment step = new DataInsertFragment();
        Bundle b = new Bundle();
        b.putInt(CURRENT_STEP_POSITION_KEY, position);
        step.setArguments(b);
        return step;
    }

    @Override
    public int getCount() {
        return 5;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 5) int position) {
        return new StepViewModel.Builder(context)
                .setTitle(null)
                .create();
    }
}
