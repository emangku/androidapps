package com.ck.it.androidapps.fragment.FS.fs_history;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eko on 06/03/18.
 */

@SuppressWarnings("deprecation")
public class DetailsFS extends AppCompatActivity {
    TextView viewASDPBencmark;
    TextView viewASDPWorse;
    TextView viewASDPAverage;
    TextView viewASDPBest;
    TextView viewSalesBencmark;
    TextView viewSalesWorse;
    TextView viewSalesAverage;
    TextView viewSalesBest;
    TextView viewGPPersenBencmark;
    TextView viewGPPersenWorse;
    TextView viewGPPersenAverage;
    TextView viewGPPersenBest;
    TextView viewGPIDRBencmark ;
    TextView viewGPIDRWorse;;
    TextView viewGPIDRAverage;;
    TextView viewGPIDRBest;;
    TextView viewEmploymentBencmark;;
    TextView viewEmploymentWorse;
    TextView viewEmploymentAverage;
    TextView viewEmploymentBest;
    TextView viewUtilityBencmark;
    TextView viewUtilityWorse;
    TextView viewUtilityAverage;
    TextView viewUtilityBest;
    TextView viewRepairAndMaintenaceBenmark;
    TextView viewRepairAndMaintenaceWorse;
    TextView viewRepairAndMaintenaceAverage;
    TextView viewRepairAndMaintenaceBest;
    TextView viewStoreUseBencmark;
    TextView viewStoreUseWorse;
    TextView viewStoreUseAverage;
    TextView viewStoreUseBest;
    TextView viewOthersBencmark;
    TextView viewOthersWorse;
    TextView viewOthersAverage;
    TextView viewOthersBest;;
    TextView viewRentBencmark;
    TextView viewRentWorse;
    TextView viewRentAverage;
    TextView viewRentBest;;
    TextView viewDescriptionBencmark;
    TextView viewDescriptionWorse;
    TextView viewDescriptionAverage;
    TextView viewDescriptionBest;
    TextView viewTotalCostBencmark;
    TextView viewTotalCostWorse;
    TextView viewTotalCostAverage;
    TextView viewTotalCostBest;
    TextView viewNSCBencmark;
    TextView viewNSCWorse;
    TextView viewNSCAverage;
    TextView viewNSCBest;
    TextView viewPersenToSalesBencmark;
    TextView viewPersenToSalesWorse;
    TextView viewPersenToSalesAverage;
    TextView viewPersenToSalesBest;
    TextView viewCashFlowBencmark;
    TextView viewCashFlowWorse;
    TextView viewCashFlowAverage;
    TextView viewCashFlowBest;
    TextView viewPlayBackYearsBencmark;
    TextView viewPlayBackYearsWorse;
    TextView viewPlayBackYearsAverage;
    TextView viewPlayBackYearsBest;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.show_details_fs_list);
        init();
    }

    void init(){
        viewASDPBencmark = (TextView)findViewById(R.id.viewASDPBencmark);
//        viewASDPBencmark.setText(Integer.toString(Integer.parseInt(list.get(position).getSALES())/30));
        viewASDPWorse = (TextView)findViewById(R.id.viewASDPWorse);
//        viewASDPWorse.setText(list.get(position).getWORSE());
        viewASDPAverage = (TextView)findViewById(R.id.viewASDPAverage);
//        viewASDPAverage.setText(list.get(position).getAVERAGE());
        viewASDPBest = (TextView)findViewById(R.id.viewASDPBest);
//        viewASDPBest.setText(list.get(position).getBEST());

        viewSalesBencmark = (TextView)findViewById(R.id.viewSalesBencmark);
//        viewSalesBencmark.setText(list.get(position).getSALES());
        viewSalesWorse = (TextView)findViewById(R.id.viewSalesWorse);
//        viewSalesWorse.setText(Integer.toString(Integer.parseInt(list.get(position).getWORSE()) * 30));
        viewSalesAverage = (TextView)findViewById(R.id.viewSalesAverage);
//        viewSalesAverage.setText(Integer.toString(Integer.parseInt(list.get(position).getAVERAGE()) * 30));
        viewSalesBest = (TextView)findViewById(R.id.viewSalesBest);
//        viewSalesBest.setText(Integer.toString(Integer.parseInt(list.get(position).getBEST()) * 30));

        viewGPPersenBencmark = (TextView)findViewById(R.id.viewGPPersenBencmark);
//        viewGPPersenBencmark.setText(Integer.toString(Integer.parseInt(list.get(position).getGP())));
        viewGPPersenWorse = (TextView)findViewById(R.id.viewGPPersenWorse);
//        viewGPPersenBencmark.setText(Integer.toString(Integer.parseInt(list.get(position).getGP_STANDARD())));
        viewGPPersenAverage = (TextView)findViewById(R.id.viewGPPersenAverage);
//        viewGPPersenAverage.setText(Integer.toString(Integer.parseInt(list.get(position).getGP_STANDARD())));
        viewGPPersenBest = (TextView)findViewById(R.id.viewGPPersenBest);
//        viewGPPersenBest.setText(Integer.toString(Integer.parseInt(list.get(position).getGP_STANDARD())));

        viewGPIDRBencmark = (TextView)findViewById(R.id.viewGPIDRBencmark);
//        viewGPIDRBencmark.setText(Integer.toString(
//                Integer.parseInt(list.get(position).getSALES()) * (Integer.parseInt(list.get(position).getGP()) / 100)));

        viewGPIDRWorse = (TextView)findViewById(R.id.viewGPIDRWorse);
//        viewGPIDRWorse.setText(Integer.toString(
//                (Integer.parseInt(list.get(position).getWORSE()) * 30) * (Integer.parseInt(list.get(position).getGP()) / 100)));

        viewGPIDRAverage = (TextView)findViewById(R.id.viewGPIDRAverage);
//        viewGPIDRAverage.setText(Integer.toString(
//                (Integer.parseInt(list.get(position).getAVERAGE()) * 30) * (Integer.parseInt(list.get(position).getGP()) / 100)));

        viewGPIDRBest = (TextView)findViewById(R.id.viewGPIDRBest);
//        viewGPIDRBest.setText(Integer.toString(
//                (Integer.parseInt(list.get(position).getBEST()) * 30) * (Integer.parseInt(list.get(position).getGP()) / 100)));

        viewEmploymentBencmark = (TextView)findViewById(R.id.viewEmploymentBencmark);
//        viewEmploymentBencmark.setText(list.get(position).getEMPLOYMENT());

        viewEmploymentWorse = (TextView)findViewById(R.id.viewEmploymentWorse);
//        viewEmploymentWorse.setText();
        viewEmploymentAverage = (TextView)findViewById(R.id.viewEmploymentAverage);
        viewEmploymentBest = (TextView)findViewById(R.id.viewEmploymentBest);

        viewUtilityBencmark = (TextView)findViewById(R.id.viewUtilityBencmark);
        viewUtilityWorse = (TextView)findViewById(R.id.viewUtilityWorse);
        viewUtilityAverage = (TextView)findViewById(R.id.viewUtilityAverage);
        viewUtilityBest = (TextView)findViewById(R.id.viewUtilityBest);

        viewRepairAndMaintenaceBenmark = (TextView)findViewById(R.id.viewRepairAndMaintenaceBenmark);
        viewRepairAndMaintenaceWorse = (TextView)findViewById(R.id.viewRepairAndMaintenaceWorse);
        viewRepairAndMaintenaceAverage = (TextView)findViewById(R.id.viewRepairAndMaintenaceAverage);
        viewRepairAndMaintenaceBest = (TextView)findViewById(R.id.viewRepairAndMaintenaceBest);

        viewStoreUseBencmark = (TextView)findViewById(R.id.viewStoreUseBencmark);
        viewStoreUseWorse = (TextView)findViewById(R.id.viewStoreUseWorse);
        viewStoreUseAverage = (TextView)findViewById(R.id.viewStoreUseAverage);
        viewStoreUseBest = (TextView)findViewById(R.id.viewStoreUseBest);

        viewOthersBencmark = (TextView)findViewById(R.id.viewOthersBencmark);
        viewOthersWorse = (TextView)findViewById(R.id.viewOthersWorse);
        viewOthersAverage = (TextView)findViewById(R.id.viewOthersAverage);
        viewOthersBest = (TextView)findViewById(R.id.viewOthersBest);

        viewRentBencmark = (TextView)findViewById(R.id.viewRentBencmark);
        viewRentWorse = (TextView)findViewById(R.id.viewRentWorse);
        viewRentAverage = (TextView)findViewById(R.id.viewRentAverage);
        viewRentBest = (TextView)findViewById(R.id.viewRentBest);

        viewDescriptionBencmark = (TextView)findViewById(R.id.viewDescriptionBencmark);
        viewDescriptionWorse = (TextView)findViewById(R.id.viewDescriptionWorse);
        viewDescriptionAverage = (TextView)findViewById(R.id.viewDescriptionAverage);
        viewDescriptionBest = (TextView)findViewById(R.id.viewDescriptionBest);

        viewTotalCostBencmark = (TextView)findViewById(R.id.viewTotalCostBencmark);
        viewTotalCostWorse = (TextView)findViewById(R.id.viewTotalCostWorse);
        viewTotalCostAverage = (TextView)findViewById(R.id.viewTotalCostAverage);
        viewTotalCostBest = (TextView)findViewById(R.id.viewTotalCostBest);

        viewNSCBencmark = (TextView)findViewById(R.id.viewNSCBencmark);
        viewNSCWorse = (TextView)findViewById(R.id.viewNSCWorse);
        viewNSCAverage = (TextView)findViewById(R.id.viewNSCAverage);
        viewNSCBest = (TextView)findViewById(R.id.viewNSCBest);

        viewPersenToSalesBencmark = (TextView)findViewById(R.id.viewPersenToSalesBencmark);
        viewPersenToSalesWorse = (TextView)findViewById(R.id.viewPersenToSalesWorse);
        viewPersenToSalesAverage = (TextView)findViewById(R.id.viewPersenToSalesAverage);
        viewPersenToSalesBest = (TextView)findViewById(R.id.viewPersenToSalesBest);

        viewCashFlowBencmark = (TextView)findViewById(R.id.viewCashFlowBencmark);
        viewCashFlowWorse = (TextView)findViewById(R.id.viewCashFlowWorse);
        viewCashFlowAverage = (TextView)findViewById(R.id.viewCashFlowAverage);
        viewCashFlowBest = (TextView)findViewById(R.id.viewCashFlowBest);

        viewPlayBackYearsBencmark = (TextView)findViewById(R.id.viewPlayBackYearsBencmark);
        viewPlayBackYearsWorse = (TextView)findViewById(R.id.viewPlayBackYearsWorse);
        viewPlayBackYearsAverage = (TextView)findViewById(R.id.viewPlayBackYearsAverage);
        viewPlayBackYearsBest = (TextView)findViewById(R.id.viewPlayBackYearsBest);
    }

    void getDetailsFs(String url){
        final ProgressDialog dialog = utils.showLoadDialog(DetailsFS.this, params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API+url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.USER_ID,utils.getDataSession(DetailsFS.this,"sessionUserId"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
                                if (!response.getJSONObject("data").getString("SALES").equals("null")){
                                    viewASDPBencmark.setText(
                                            Integer.toString(
                                                    Integer.parseInt(response.getJSONObject("data").getString("SALES"))/30));
                                }
                                if (!response.getJSONObject("data").getString("WORSE").equals("null")){
                                    viewASDPWorse.setText(response.getJSONObject("data").getString("WORSE"));
                                }
                                if (!response.getJSONObject("data").getString("AVERAGE").equals("null")){
                                    viewASDPAverage.setText(response.getJSONObject("data").getString("AVERAGE"));
                                }
                                if (!response.getJSONObject("data").getString("BEST").equals("null")){
                                    viewASDPBest.setText(response.getJSONObject("data").getString("BEST"));
                                }

                                if (!response.getJSONObject("data").getString("SALES").equals("null")){
                                    viewSalesBencmark.setText(response.getJSONObject("data").getString("SALES"));
                                }
                                if (!response.getJSONObject("data").getString("WORSE").equals("null")){
                                    viewSalesWorse.setText(Integer.toString(Integer.parseInt(response.getJSONObject("data").getString("WORSE")) * 30));
                                }
                                if (!response.getJSONObject("data").getString("AVERAGE").equals("null")){
                                    viewSalesAverage.setText(Integer.toString(Integer.parseInt(response.getJSONObject("data").getString("AVERAGE")) * 30));
                                }
                                if (!response.getJSONObject("data").getString("BEST").equals("null")){
                                    viewSalesBest.setText(Integer.toString(Integer.parseInt(response.getJSONObject("data").getString("BEST")) * 30));
                                }
                                if (!response.getJSONObject("data").getString("SALES").equals("null") && !response.getJSONObject("data").getString("GP").equals("null")){
                                    viewGPIDRBencmark.setText(Integer.toString(
                                                Integer.parseInt(
                                                        response.getJSONObject("data").getString("SALES")) * (Integer.parseInt(response.getJSONObject("data").getString("GP")) / 100)));
                                }
                                if (!response.getJSONObject("data").getString("WORSE").equals("null") && !response.getJSONObject("data").getString("GP").equals("null")){
                                    viewGPIDRWorse.setText(Integer.toString(
                                                    (Integer.parseInt(response.getJSONObject("data").getString("WORSE")) * 30) * (Integer.parseInt(response.getJSONObject("data").getString("GP")) / 100)));
                                }

                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(DetailsFS.this,"Opps Error : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    void getUserRegion(String url){
        final ProgressDialog dialog = utils.showLoadDialog(DetailsFS.this, params.MValues.loading_msg);
        dialog.show();

        AndroidNetworking.post(api.SERVER_API+url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.USER_ID, utils.getDataSession(DetailsFS.this,"sessionUserId"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                if (!response.getJSONObject("data").getString("USER_ID").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrById",response.getJSONObject("data").getString("USER_ID"));
                                }
                                if (!response.getJSONObject("data").getString("USERNAME").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrNameById",response.getJSONObject("data").getString("USERNAME"));
                                }
                                if (!response.getJSONObject("data").getString("S_EMAIL").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrEmailById",response.getJSONObject("data").getString("S_EMAIL"));
                                }
                                if (!response.getJSONObject("data").getString("NO_TELP").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrNoTlpById",response.getJSONObject("data").getString("NO_TELP"));
                                }
                                if (!response.getJSONObject("data").getString("TITLE").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrTitleById",response.getJSONObject("data").getString("TITLE"));
                                }
                                if (!response.getJSONObject("data").getString("IMAGE").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrImgById",response.getJSONObject("data").getString("IMAGE"));
                                }
                                if (!response.getJSONObject("data").getString("REGIONAL").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrRegionById",response.getJSONObject("data").getString("REGIONAL"));
                                }
                                if (!response.getJSONObject("data").getString("PASSWORD").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrPassById",response.getJSONObject("data").getString("PASSWORD"));
                                }
                                if (!response.getJSONObject("data").getString("USER_STATUS").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrStatusById",response.getJSONObject("data").getString("USER_STATUS"));
                                }
                                if (!response.getJSONObject("data").getString("NO_STATUS").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrNoStatusById",response.getJSONObject("data").getString("NO_STATUS"));
                                }
                                if (!response.getJSONObject("data").getString("STATUS").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrvStatusById",response.getJSONObject("data").getString("STATUS"));
                                }
                                if (!response.getJSONObject("data").getString("remember_token").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrTokenById",response.getJSONObject("data").getString("remember_token"));
                                }
                                if (!response.getJSONObject("data").getString("created_at").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrCreateATById",response.getJSONObject("data").getString("created_at"));
                                }
                                if (!response.getJSONObject("data").getString("updated_at").equals("null")){
                                    utils.setDataSession(DetailsFS.this,"sessionValUsrUpdateATById",response.getJSONObject("data").getString("updated_at"));
                                }
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(),"Opps.. Error : "+anError.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
    }
}
