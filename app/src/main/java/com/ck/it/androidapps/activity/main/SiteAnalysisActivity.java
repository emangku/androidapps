package com.ck.it.androidapps.activity.main;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.fragment.areaanalysis.kompetitor.PhotoFragment;
import com.ck.it.androidapps.util.utils;

public class SiteAnalysisActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_analysis);
        Fragment fragment = new PhotoFragment();
        setTitle("Situasi Bangunan");
        utils.showFragmentTwo(this, fragment);
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            Intent intent = new Intent(SiteAnalysisActivity.this, HomeActivity.class);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }
}
