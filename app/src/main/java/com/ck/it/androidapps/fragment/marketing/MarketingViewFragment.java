package com.ck.it.androidapps.fragment.marketing;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 12/12/2017.
 */

@SuppressWarnings("deprecation")
public class MarketingViewFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View view;
    SwipeRefreshLayout viewRefreshMarketingView;
    RecyclerView viewRcyMarketingView;
    ProgressDialog dialog;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.marketing_view_fragment, container,false);
        init();
        return view;
    }

    void init(){
        viewRefreshMarketingView = (SwipeRefreshLayout)view.findViewById(R.id.viewRefreshMarketingView);
        viewRefreshMarketingView.setOnRefreshListener(this);
        viewRcyMarketingView = (RecyclerView) view.findViewById(R.id.viewRcyMarketingView);
    }


    void showDataMarketingView(String url){
        dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API+url)
                .addBodyParameter(params.SITE_CODE,utils.getDataSession(view.getContext(),""))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
    @Override
    public void onRefresh() {

    }
}
