package com.ck.it.androidapps.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.main.AreaAnalysisActivity;
import com.ck.it.androidapps.activity.main.SiteAnalysisActivity;
import com.ck.it.androidapps.activity.main.SiteInformamtionActivity;
import com.ck.it.androidapps.activity.main.SquenceVideoActivity;
import com.ck.it.androidapps.activity.maps.MappingAreaActivity;
import com.ck.it.androidapps.fragment.FS.fs_history.FSHistoryActivity;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import com.daimajia.slider.library.Tricks.ViewPagerEx;
import com.goka.blurredgridmenu.GridMenu;
import com.goka.blurredgridmenu.GridMenuFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class HomeFragment extends Fragment
        implements BaseSliderView.OnSliderClickListener,
        ViewPagerEx.OnPageChangeListener,
        View.OnClickListener {
    private View view;
    private SliderLayout mDemoSlider;
    private GridMenuFragment mGridMenuFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_main, container, false);
        initView();
        return view;
    }

    void initView() {
        mGridMenuFragment = GridMenuFragment.newInstance(R.drawable.splash);
        ((FragmentActivity) view.getContext())
                .getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.home_frm, mGridMenuFragment)
                .addToBackStack(null)
                .commit();
        setupGrdMenu();
        initSideIMG();

        mGridMenuFragment.setOnClickMenuListener(new GridMenuFragment.OnClickMenuListener() {
            @Override
            public void onClickMenu(GridMenu gridMenu, int position) {
                if (gridMenu.getTitle().equals("Site Information")) {
                    startActivityForResult(new Intent(view.getContext(), SiteInformamtionActivity.class), 0);
                } else if (gridMenu.getTitle().equals("Site Analysis")) {
                    startActivityForResult(new Intent(view.getContext(), SiteAnalysisActivity.class), 1);
                } else if (gridMenu.getTitle().equals("Area Analysis")) {
                    startActivityForResult(new Intent(view.getContext(), AreaAnalysisActivity.class), 2);
                } else if (gridMenu.getTitle().equals("Mapping Area")) {
                    startActivityForResult(new Intent(view.getContext(), MappingAreaActivity.class), 3);
                } else if (gridMenu.getTitle().equals("Trading Area")) {

                } else if (gridMenu.getTitle().equals("Video")) {
                    startActivityForResult(new Intent(view.getContext(), SquenceVideoActivity.class), 5);
                } else if (gridMenu.getTitle().equals("FS")) {
//                    startActivityForResult(new Intent(view.getContext(), FinalFSActivity.class), 6);
                    startActivityForResult(new Intent(view.getContext(), FSHistoryActivity.class), 6);
                } else if (gridMenu.getTitle().equals("Approval")) {

                }
            }
        });
    }

    private void setupGrdMenu() {
        List<GridMenu> menus = new ArrayList<>();
        menus.add(new GridMenu("Site Information", R.drawable.ico));
        menus.add(new GridMenu("Site Analysis", R.drawable.ico));
        menus.add(new GridMenu("Area Analysis", R.drawable.ico));
        menus.add(new GridMenu("Mapping Area", R.drawable.ico));
        menus.add(new GridMenu("Trading Area", R.drawable.ico));
        menus.add(new GridMenu("Video", R.drawable.ico));
        menus.add(new GridMenu("FS", R.drawable.ico));
        menus.add(new GridMenu("Approval", R.drawable.ico));
        mGridMenuFragment.setupMenu(menus);
    }

    void initSideIMG() {
        mDemoSlider = (SliderLayout) view.findViewById(R.id.slider);

        HashMap<String, String> url_maps = new HashMap<String, String>();
        url_maps.put("Semple 1", "https://image-store.slidesharecdn.com/79b3150c-3e5a-4c35-8de7-c8bfd6dc9637-original.jpeg");
        url_maps.put("Semple 2", "https://s3-ap-southeast-1.amazonaws.com/swa.co.id/wp-content/uploads/2013/03/circle-k.jpg");
        url_maps.put("Semple 3", "https://siva.jsstatic.com/id/8409/images/photo/8409_photo_0_92673.png");
        url_maps.put("Semple 4", "http://2.bp.blogspot.com/-z1v2czywx2A/ULBAo0iQH6I/AAAAAAAAIyo/VoLNl88Y18w/s1600/job-vacancy-circle-k.jpg");

        for (String name : url_maps.keySet()) {
            TextSliderView textSliderView = new TextSliderView(view.getContext());
            // initialize a SliderLayout
            textSliderView
                    .description(name)
                    .image(url_maps.get(name))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(this);

            //add your extra information
            textSliderView.bundle(new Bundle());
            textSliderView.getBundle()
                    .putString("extra", name);

            mDemoSlider.addSlider(textSliderView);

            mDemoSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
            mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            mDemoSlider.setCustomAnimation(new DescriptionAnimation());
            mDemoSlider.setDuration(4000);
        }
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onClick(View v) {
    }
}
