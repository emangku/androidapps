package com.ck.it.androidapps.fragment.legalanalisa;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.adapter.AdapterGalleryApprove;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.model.ModelGalleryApprove;
import com.ck.it.androidapps.util.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Creef on 12/12/2017.
 */

@SuppressWarnings("deprecation")
public class LegalAnalisaApprovalFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{
    private View view;
    private RecyclerView rvapp_gallery;
    private List<ModelGalleryApprove> list;
    private AdapterGalleryApprove adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.legal_analisa_approval_fragment, container, false);
        init();
        return view;
    }


    void init() {
        mSwipeRefreshLayout = (SwipeRefreshLayout)view.findViewById(R.id.swipe_container2);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        showDataGalery("galery");
    }

    void showDataGalery(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .addBodyParameter(params.DOC_NAME, "Approve")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                list = new ArrayList<ModelGalleryApprove>();
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    ModelGalleryApprove modelGalleryApprove = new ModelGalleryApprove();
                                    JSONObject object = array.getJSONObject(i);
                                    modelGalleryApprove.setUSER_ID(object.getString("USER_ID"));
                                    modelGalleryApprove.setDOC_NAME(object.getString("DOC_NAME"));
                                    modelGalleryApprove.setDOCUMENT(object.getString("DOCUMENT"));
                                    modelGalleryApprove.setCreated_at(object.getString("created_at"));
                                    modelGalleryApprove.setUpdated_at(object.getString("updated_at"));
                                    list.add(modelGalleryApprove);
                                }
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("Error : " + e.fillInStackTrace());
                            dialog.dismiss();
                        }

                        rvapp_gallery = (RecyclerView) view.findViewById(R.id.rvap_gallery);
                        rvapp_gallery.setLayoutManager(new GridLayoutManager(view.getContext(), 2));
                        adapter = new AdapterGalleryApprove(view.getContext(), list);
                        rvapp_gallery.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(), "opps error : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public void onRefresh() {
        showDataGalery("galery");
        mSwipeRefreshLayout.setRefreshing(false);
    }
}
