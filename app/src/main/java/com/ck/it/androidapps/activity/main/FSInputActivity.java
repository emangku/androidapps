package com.ck.it.androidapps.activity.main;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.fragment.FS.FSInvestasiFragments;

public class FSInputActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fsinput);
        getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.viewFS, new FSInvestasiFragments())
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            startActivity(new Intent(FSInputActivity.this, HomeActivity.class));
        } else {
            super.onBackPressed();
        }
    }
}
