package com.ck.it.androidapps.activity.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.androidnetworking.interfaces.UploadProgressListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.gpstracker.GpsTracker;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;

import io.github.lizhangqu.coreprogress.ProgressHelper;
import io.github.lizhangqu.coreprogress.ProgressListener;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@SuppressWarnings("deprecation")
public class SquenceVideoActivity extends AppCompatActivity {
    private static final int VIDEO_CAPTURE = 101;
    Uri videoUri;
    MediaRecorder recorder;
    CamcorderProfile cpHigh;
    DisplayMetrics metrics;
    GpsTracker gps;
    double latitude, longitude;
    File mediaFile;
    BottomNavigationView bottomNavigationView;
    String videoPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_squence_video);
        setTitle("Squence Video");
        init();
    }

    public void startRecordingVideo() {
        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FRONT)) {
            Intent intent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            mediaFile = new File(
                    Environment.getExternalStorageDirectory().getAbsolutePath() + "/myvideo.mp4");
            videoUri = Uri.fromFile(mediaFile);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, videoUri);
            startActivityForResult(intent, VIDEO_CAPTURE);
        } else {
            Toast.makeText(this, "No camera on device", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == VIDEO_CAPTURE) {
            if (resultCode == RESULT_OK) {
                videoPath = data.getData().getPath();
                playbackRecordedVideo(videoUri);
            } else if (resultCode == RESULT_CANCELED) {
                Toast.makeText(this, "Video recording cancelled.", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, "Failed to record video", Toast.LENGTH_LONG).show();
            }
        }
    }

    public void playbackRecordedVideo(Uri uri) {
        VideoView mVideoView = (VideoView) findViewById(R.id.video_view);
        mVideoView.setVideoURI(uri);
        mVideoView.setMediaController(new MediaController(this));
        mVideoView.requestFocus();
        mVideoView.start();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.viewUploads:
                    System.out.print(" MEDIA : " + mediaFile);
                    uploads(params.URL_METHOD.VUPLOADS, mediaFile);
//                    uploadsVideo(params.URL_METHOD.VUPLOADS,mediaFile);
                    return true;
            }
            return false;
        }
    };

    void init() {
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.vUploads);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        gps = new GpsTracker(this);
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        } else {
            Toast.makeText(this, "Opps...", Toast.LENGTH_LONG).show();
        }
    }

    void uploads(String url, File file) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        AndroidNetworking.upload(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
//                .addMultipartParameter(params.SITE_CODE,utils.getDataSession(SquenceVideoActivity.this, "sessionSiteCode"))
//                .addMultipartParameter(params.USER_ID, utils.getDataSession(SquenceVideoActivity.this, "sessionUserID"))
                .addMultipartParameter(params.SITE_CODE, "BDMS01180008")
                .addMultipartParameter(params.USER_ID, "JAK002")
                .addMultipartParameter(params.LATITUDE, String.valueOf(latitude))
                .addMultipartParameter(params.LONGITUDE, String.valueOf(longitude))
                .addMultipartFile(params.VIDEO, file)
                .build()
                .setUploadProgressListener(new UploadProgressListener() {
                    @Override
                    public void onProgress(long bytesUploaded, long totalBytes) {
                        dialog.setMax((int) totalBytes);
                        dialog.setProgress((int) (100 * totalBytes));
                        dialog.show();
                    }
                }).getAsJSONObject(new JSONObjectRequestListener() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    if (response.getString("status").equals("true")) {
                        dialog.dismiss();
                        startActivity(new Intent(SquenceVideoActivity.this, HomeActivity.class));
                        SquenceVideoActivity.this.finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    dialog.dismiss();
                }
            }

            @Override
            public void onError(ANError anError) {
                Toast.makeText(getApplicationContext(), "opps..Error : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                dialog.dismiss();
            }
        });
    }

    void uploadsVideo(String url, File file) {
        final ProgressDialog dialog = new ProgressDialog(this);
        dialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        dialog.show();
        OkHttpClient client = new OkHttpClient();
        final Request.Builder builder = new Request.Builder();
        builder.url(api.SERVER_API + url);
        final MultipartBody.Builder body = new MultipartBody.Builder();
        body.addFormDataPart(params.VIDEO, "BDMS01180008");
        body.addFormDataPart(params.VIDEO, "JAK002");
//        body.addFormDataPart(params.VIDEO,utils.getDataSession(this,"sessionSiteCode"));
//        body.addFormDataPart(params.VIDEO,utils.getDataSession(this,"sessionUserID"));
        body.addFormDataPart(params.VIDEO, String.valueOf(latitude));
        body.addFormDataPart(params.VIDEO, String.valueOf(longitude));
        body.addFormDataPart(params.VIDEO, utils.getDataSession(this, "sessionSiteCode"), RequestBody.create(MediaType.parse("VIDEO"), file));
        MultipartBody mBody = body.build();
        RequestBody requestBody = ProgressHelper.withProgress(mBody, new ProgressListener() {
            @Override
            public void onProgressStart(long totalBytes) {
                super.onProgressStart(totalBytes);
                dialog.setProgress((int) totalBytes);
            }

            @Override
            public void onProgressChanged(long numBytes, long totalBytes, float percent, float speed) {
                dialog.setProgress((int) (100 * percent));
            }

            @Override
            public void onProgressFinish() {
                super.onProgressFinish();
                dialog.dismiss();
                startActivity(new Intent(SquenceVideoActivity.this, HomeActivity.class));
            }
        });
        builder.post(requestBody);
        Call call = client.newCall(builder.build());
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.e("TAG", "=============onFailure===============");
                e.printStackTrace();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                Log.e("TAG", "=============onResponse===============");
                Log.e("TAG", "request headers:" + response.request().headers());
                Log.e("TAG", "response headers:" + response.headers());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.records, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.records_view) {
            startRecordingVideo();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
