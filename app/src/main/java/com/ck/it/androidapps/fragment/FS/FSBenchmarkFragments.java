package com.ck.it.androidapps.fragment.FS;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 1/30/2018.
 */

@SuppressWarnings("deprecation")
public class FSBenchmarkFragments extends Fragment implements View.OnClickListener {
    private View view;
    EditText viewStoreID,viewSalesBM,viewGP_BM,
            viewEmployment,viewUtility,viewRepairAndMaintenaceBM,
            viewStoreUserBM,viewOtherBM,viewRentBM,viewDepreciation,viewNSC;
    Button viewBtnBenchMark;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fs_benchmark_fragment, container, false);
        init();
        return view;
    }

    void init(){
        getActivity().setTitle("FS Benchmark");
        viewStoreID = (EditText) view.findViewById(R.id.viewStoreID);
        viewSalesBM = (EditText) view.findViewById(R.id.viewSalesBM);
        viewGP_BM = (EditText) view.findViewById(R.id.viewGP_BM);
        viewEmployment = (EditText) view.findViewById(R.id.viewEmployment);
        viewUtility = (EditText) view.findViewById(R.id.viewUtility);
        viewRepairAndMaintenaceBM = (EditText) view.findViewById(R.id.viewRepairAndMaintenaceBM);
        viewStoreUserBM = (EditText) view.findViewById(R.id.viewStoreUserBM);
        viewRentBM = (EditText) view.findViewById(R.id.viewRentBM);
        viewDepreciation = (EditText) view.findViewById(R.id.viewDepreciation);
        viewNSC = (EditText) view.findViewById(R.id.viewNSC);
        viewOtherBM = (EditText) view.findViewById(R.id.viewOtherBM);
        viewBtnBenchMark = (Button) view.findViewById(R.id.viewBtnBenchMark);
        viewBtnBenchMark.setOnClickListener(this);
    }


    void saveBanchMark(String url){
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(),"Tunggu...");
        dialog.show();
        AndroidNetworking.post(api.SERVER_API+url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(),"sessionValKeySiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.STORE_ID, viewStoreID.getText().toString())
                .addBodyParameter(params.SALES, viewSalesBM.getText().toString())
                .addBodyParameter(params.GP, viewGP_BM.getText().toString())
                .addBodyParameter(params.EMPLOYMENT, viewEmployment.getText().toString())
                .addBodyParameter(params.UTILITY, viewUtility.getText().toString())
                .addBodyParameter(params.REPAIR_MAINTENANCE, viewRepairAndMaintenaceBM.getText().toString())
                .addBodyParameter(params.STORE_USE, viewStoreUserBM.getText().toString())
                .addBodyParameter(params.OTHERS, viewOtherBM.getText().toString())
                .addBodyParameter(params.RENT, viewRentBM.getText().toString())
                .addBodyParameter(params.DEPRECIATION, viewDepreciation.getText().toString())
                .addBodyParameter(params.NSC, viewNSC.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
                                ((FragmentActivity)view.getContext())
                                        .getSupportFragmentManager()
                                        .beginTransaction()
                                        .addToBackStack(null)
                                        .replace(R.id.viewFS,new FSStoreCostFragments())
                                        .commit();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(),"Opps error : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewBtnBenchMark:
                if (TextUtils.isEmpty(viewStoreID.getText().toString())){
                    viewStoreID.setError(params.MValues.field_error_msg);
                    viewStoreID.requestFocus();
                }else if (TextUtils.isEmpty(viewSalesBM.getText().toString())){
                    viewSalesBM.setError(params.MValues.field_error_msg);
                    viewSalesBM.requestFocus();
                }else if (TextUtils.isEmpty(viewGP_BM.getText().toString())){
                    viewGP_BM.setError(params.MValues.field_error_msg);
                    viewGP_BM.requestFocus();
                }else if (TextUtils.isEmpty(viewEmployment.getText().toString())){
                    viewEmployment.setError(params.MValues.field_error_msg);
                    viewEmployment.requestFocus();
                }else if (TextUtils.isEmpty(viewUtility.getText().toString())){
                    viewUtility.setError(params.MValues.field_error_msg);
                    viewUtility.requestFocus();
                }else if (TextUtils.isEmpty(viewRepairAndMaintenaceBM.getText().toString())){
                    viewRepairAndMaintenaceBM.setError(params.MValues.field_error_msg);
                    viewRepairAndMaintenaceBM.requestFocus();
                }else if (TextUtils.isEmpty(viewStoreUserBM.getText().toString())){
                    viewStoreUserBM.setError(params.MValues.field_error_msg);
                    viewStoreUserBM.requestFocus();
                }else if (TextUtils.isEmpty(viewOtherBM.getText().toString())){
                    viewOtherBM.setError(params.MValues.field_error_msg);
                    viewOtherBM.requestFocus();
                }else if (TextUtils.isEmpty(viewRentBM.getText().toString())){
                    viewRentBM.setError(params.MValues.field_error_msg);
                    viewRentBM.requestFocus();
                }else if (TextUtils.isEmpty(viewDepreciation.getText().toString())){
                    viewDepreciation.setError(params.MValues.field_error_msg);
                    viewDepreciation.requestFocus();
                }else if (TextUtils.isEmpty(viewNSC.getText().toString())){
                    viewNSC.setError(params.MValues.field_error_msg);
                    viewNSC.requestFocus();
                }else{
                    saveBanchMark(params.URL_METHOD.SAVE_BANCHMARK);
                }
                break;
        }
    }
}
