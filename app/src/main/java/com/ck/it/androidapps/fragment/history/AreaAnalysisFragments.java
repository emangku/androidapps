package com.ck.it.androidapps.fragment.history;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.ui.RecyclerUIItemDecoration;
import com.ck.it.androidapps.adapter.AreaAnalysisAdapter;
import com.ck.it.androidapps.adapter.AreaAnalysisIMGAdapter;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.model.areaanlysis.AreaAnalysisIMGModel;
import com.ck.it.androidapps.model.areaanlysis.AreaAnalysisModel;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Creef on 1/16/2018.
 */

@SuppressWarnings("deprecation")
public class AreaAnalysisFragments extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View view;
    RecyclerView view_rcl_area, view_rcl_area_img;
    List<AreaAnalysisModel> aList;
    List<AreaAnalysisIMGModel> imgList;
    AreaAnalysisAdapter adapter;
    AreaAnalysisIMGAdapter mAdapter;
    private SwipeRefreshLayout viewRefreshAreaA;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.area_analysis_fragment, container, false);
        init();
        return view;
    }

    void init() {
        view_rcl_area = (RecyclerView) view.findViewById(R.id.view_rcl_area);
        view_rcl_area_img = (RecyclerView) view.findViewById(R.id.view_rcl_area_img);
        viewRefreshAreaA = (SwipeRefreshLayout) view.findViewById(R.id.viewRefreshAreaA);
        viewRefreshAreaA.setOnRefreshListener(this);
        showArea(params.URL_METHOD.GET_AREA);
        showAreaIMG(params.URL_METHOD.GET_AREA_IMG);
    }

    void showArea(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionValKeySiteCode"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                aList = new ArrayList<AreaAnalysisModel>();
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    AreaAnalysisModel mModel = new AreaAnalysisModel();
                                    JSONObject mObj = array.getJSONObject(i);
                                    mModel.setID(mObj.getString("id"));
                                    mModel.setSITE_CODE(mObj.getString("SITE_CODE"));
                                    mModel.setUSER_ID(mObj.getString("USER_ID"));
                                    mModel.setAA_ID(mObj.getString("AA_ID"));
                                    mModel.setTYPE(mObj.getString("TYPE"));
                                    mModel.setVALUE_OPTION(mObj.getString("VALUE_OPTION"));
                                    mModel.setVALUE_RANGE(mObj.getString("VALUE_RANGE"));
                                    aList.add(mModel);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        view_rcl_area.setLayoutManager(new LinearLayoutManager(view.getContext()));
                        view_rcl_area.addItemDecoration(new RecyclerUIItemDecoration(view.getContext(),LinearLayoutManager.VERTICAL,0));
                        adapter = new AreaAnalysisAdapter(view.getContext(), aList);
                        view_rcl_area.setAdapter(adapter);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(), "opps error.. AA : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    void showAreaIMG(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionValKeySiteCode"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                imgList = new ArrayList<AreaAnalysisIMGModel>();
                                JSONArray array = response.getJSONArray("data");
                                for (int a = 0; a < array.length(); a++) {
                                    AreaAnalysisIMGModel model = new AreaAnalysisIMGModel();
                                    JSONObject obj = array.getJSONObject(a);
                                    model.setSITE_CODE(obj.getString("SITE_CODE"));
                                    model.setUSER_ID(obj.getString("USER_ID"));
                                    model.setAA_ID(obj.getString("AA_ID"));
                                    model.setIMAGE(obj.getString("IMAGE"));
                                    model.setTYPE(obj.getString("TYPE"));
                                    model.setVALUE_OPTION(obj.getString("VALUE_OPTION"));
                                    model.setVALUE_RANGE(obj.getString("VALUE_RANGE"));
                                    imgList.add(model);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        view_rcl_area_img.setLayoutManager(new LinearLayoutManager(view.getContext()));
                        view_rcl_area_img.addItemDecoration(new RecyclerUIItemDecoration(view.getContext(),LinearLayoutManager.VERTICAL,0));
                        mAdapter = new AreaAnalysisIMGAdapter(view.getContext(), imgList);
                        view_rcl_area_img.setAdapter(mAdapter);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext().getApplicationContext(), "opps error.. A A : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onRefresh() {
        showArea(params.URL_METHOD.GET_AREA);
        showAreaIMG(params.URL_METHOD.GET_AREA_IMG);
        viewRefreshAreaA.setRefreshing(false);
    }
    public static AreaAnalysisFragments newInstance() {
        Bundle args = new Bundle();
        AreaAnalysisFragments fragment = new AreaAnalysisFragments();
        fragment.setArguments(args);
        return fragment;
    }
}
