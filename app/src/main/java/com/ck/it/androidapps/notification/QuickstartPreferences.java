package com.ck.it.androidapps.notification;

/**
 * Created by Creef on 2/22/2018.
 */

public class QuickstartPreferences {
    public static final String SENT_TOKEN_TO_SERVER = "sentTokenToServer";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
}
