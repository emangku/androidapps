package com.ck.it.androidapps.fragment.areaanalysis.trafficflow;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eko on 04/01/18.
 */

@SuppressWarnings("deprecation")
public class JenisKendaraanDominantFragment extends Fragment implements View.OnClickListener {
    private View view;
    String[] jeniskendaraandominsai = {
            "-Jenis Kendaraan Dominant-",
            "Mobil", "Motor", "Angkutan umum"};
    String opt, type_opt = "";
    Spinner viewJenisKendaraanDominan;
    RadioGroup viewGroupJenisKendaraanDominan;
    Button viewNextJenisKendaraanDominan;
    ArrayAdapter<String> adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.jenis_kendaraan_domonan_fragment, container, false);
        initView();
        return view;
    }

    void initView() {
        viewJenisKendaraanDominan = (Spinner) view.findViewById(R.id.viewJenisKendaraanDominan);
        adapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, jeniskendaraandominsai);
        viewJenisKendaraanDominan.setAdapter(adapter);
        viewJenisKendaraanDominan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type_opt = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        viewGroupJenisKendaraanDominan = (RadioGroup) view.findViewById(R.id.viewGroupJenisKendaraanDominan);
        viewGroupJenisKendaraanDominan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.jkd_1:
                        opt = "1";
                        break;
                    case R.id.jkd_2:
                        opt = "2";
                        break;
                    case R.id.jkd_3:
                        opt = "3";
                        break;
                    case R.id.jkd_4:
                        opt = "4";
                        break;
                    case R.id.jkd_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewNextJenisKendaraanDominan = (Button) view.findViewById(R.id.viewNextJenisKendaraanDominan);
        viewNextJenisKendaraanDominan.setOnClickListener(this);
    }

    void saveData(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.AA_ID, "AA")
                .addBodyParameter(params.TYPE, "Jenis Kendaraan Dominant")
                .addBodyParameter(params.VALUE_OPTION, type_opt)
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new ModaTransportasiPribadiFragment());
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(), "Oppss Error : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextJenisKendaraanDominan:
                if (type_opt.equals("-Jenis Kendaraan Dominant-")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (opt.equals("")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else {
                    saveData(params.URL_METHOD.SAVE_AREA_ANALYSIS);
                }
                break;
        }
    }
}
