package com.ck.it.androidapps.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.fragment.siteanalysis.HistoryFragment;
import com.ck.it.androidapps.helpers.UserSessionManager;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.util.utils;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

@SuppressWarnings("deprecation")
public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    Fragment fragment = null;
    FragmentManager fragmentManager = getSupportFragmentManager();
    UserSessionManager session;
    View header;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Home");
        fragment = new HistoryFragment();
        fragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.frame, fragment)
                .commit();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        header = LayoutInflater.from(this).inflate(R.layout.nav_header_home, navigationView, false);
        navigationView.addHeaderView(header);
        final ImageView img = (ImageView) header.findViewById(R.id.profile_images);
        final TextView user_name = (TextView) header.findViewById(R.id.user_name);
        System.out.println(api.SERVER_API_IMG + utils.getDataSession(this, "sessionIMAGE"));
        Picasso.with(this)
                .load(api.SERVER_API_IMG + utils.getDataSession(this, "sessionIMAGE"))
                .resize(513, 800)
                .centerInside()
                .into(img);
        user_name.setText(utils.getDataSession(this, "sessionUserLogin"));
        session = new UserSessionManager(getApplicationContext());
        if (session.checkLogin()) {
            HashMap<String, String> user = session.getUserDetails();
            String username = user.get(UserSessionManager.KEY_USERNAME);
            String user_id = user.get(UserSessionManager.KEY_USERID);
            user_name.setText(username);
            utils.setDataSession(HomeActivity.this, "sessionUserIds", user_id);
        }
        user_name.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(HomeActivity.this, ProfileActivity.class), 0);
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            startActivity(new Intent(this, LoginActivity.class));
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.home, menu);
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivityForResult(new Intent(this, HistoryActivity.class), 1);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.history) {
            setTitle("History");
            fragment = new HistoryFragment();
            fragmentManager
                    .beginTransaction()
                    .addToBackStack(null)
                    .replace(R.id.frame, fragment)
                    .commit();
//            startActivityForResult(new Intent(HomeActivity.this,ProfileActivity.class),0);
        }
//        else if (id == R.id.projectplaning) {
//            setTitle("Project Planning");
//            fragment = new ProjectPlaningFragment();
//            fragmentManager
//                    .beginTransaction()
//                    .addToBackStack(null)
//                    .replace(R.id.frame, fragment)
//                    .commit();
//        } else if (id == R.id.project) {
//            setTitle("Project");
//            fragment = new ProjectFragment();
//            fragmentManager
//                    .beginTransaction()
//                    .addToBackStack(null)
//                    .replace(R.id.frame, fragment)
//                    .commit();
//
//        } else if (id == R.id.monitoring) {
//            setTitle("Monitoring");
//            fragment = new MonitoringFragment();
//            fragmentManager
//                    .beginTransaction()
//                    .addToBackStack(null)
//                    .replace(R.id.frame, fragment)
//                    .commit();
//        }
        else if (id == R.id.help) {
            setTitle("Help");
        } else if (id == R.id.logout) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            View view = LayoutInflater.from(this).inflate(R.layout.logout_notification, null, false);
            builder.setView(view);
            LinearLayout viewTidak = (LinearLayout) view.findViewById(R.id.viewTidak);
            LinearLayout viewYa = (LinearLayout) view.findViewById(R.id.viewYa);
            final AlertDialog dialog = builder.create();
            viewTidak.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.cancel();
                }
            });
            viewYa.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    session.logoutUser();
                    dialog.cancel();
                }
            });
            builder.show();

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

}
