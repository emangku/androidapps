package com.ck.it.androidapps.fragment.spk;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ck.it.androidapps.R;

/**
 * Created by eko on 22/03/18.
 */

@SuppressWarnings("deprecation")
public class SpkCommentsFragments extends Fragment {
    private View view;
    @Override
    public void onAttach(Activity context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.spk_comment_fragmants,container, false);
        return view;
    }
}
