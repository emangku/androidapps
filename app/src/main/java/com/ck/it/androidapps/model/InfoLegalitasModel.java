package com.ck.it.androidapps.model;

/**
 * Created by Creef on 12/17/2017.
 */

public class InfoLegalitasModel {
    String SITE_CODE, USER_ID,
            RENT, RENOVATION,
            EQUIPMENT, TOTAL_INVESTMENT,
            created_at, updated_at,
            BEST, AVERAGE,
            WORSE,GP_STANDARD,
            STORE_ID, SALES,
            GP, EMPLOYMENT,
            UTILITY, REPAIR_MAINTENANCE,
            STORE_USE, OTHERS,
            DESCRIPTION, NSC,
            NO_OF_STAFF, SL,
            CSR, UTILITES;
}
