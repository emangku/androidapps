package com.ck.it.androidapps.fragment.siteanalysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.util.utils;

/**
 * Created by Creef on 12/6/2017.
 */

public class FasilitasBangunanFragment extends Fragment implements View.OnClickListener {
    private View view;
    String[] kapasitaslistrik = {"-Kapasitas Listrik-","Tidak Ada","< 2200 watt","2200-4400 watt","4400-10000 watt","10000-20000 watt","> 20000 watt"};
    String[] jaringanair = {"-Jaringan Air-","Tidak ada","Sumur air","PDAM"};
    String[] jaringantelp = {"-Jaringan Telepon-","Ada","Tidak"};
    String[] toilet = {"-Toilet-","Ada","Tidak"};
    String[] tipetoilet = {"-Type Toilet-","Ada","Tidak"};

    Spinner viewKapasitasListrik,viewJaringanAir,viewJaringanTelp,viewToilet,viewTipeToilet;
    Button viewNextFasilitasBangunan;
    ArrayAdapter<String> klAdapter, jaAdapter, jtAdapter,tAdapter,ttAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fasilitas_bangunan,container,false);
        initView();
        return view;
    }

    void initView(){
        viewNextFasilitasBangunan = (Button)view.findViewById(R.id.viewNextFasilitasBangunan);
        viewNextFasilitasBangunan.setOnClickListener(this);

        viewKapasitasListrik = (Spinner) view.findViewById(R.id.viewKapasitasListrik);
        klAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner,kapasitaslistrik);
        viewKapasitasListrik.setAdapter(klAdapter);

        viewJaringanAir = (Spinner) view.findViewById(R.id.viewJaringanAir);
        jaAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner,jaringanair);
        viewJaringanAir.setAdapter(jaAdapter);


        viewJaringanTelp = (Spinner) view.findViewById(R.id.viewJaringanTelp);
        jtAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner,jaringantelp);
        viewJaringanTelp.setAdapter(jtAdapter);


        viewToilet = (Spinner) view.findViewById(R.id.viewToilet);
        tAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner,toilet);
        viewToilet.setAdapter(klAdapter);


        viewTipeToilet = (Spinner) view.findViewById(R.id.viewTipeToilet);
        ttAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner,tipetoilet);
        viewTipeToilet.setAdapter(ttAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextFasilitasBangunan:
                Fragment fragment = new ParkirFragment();
                getActivity().setTitle("Parkir");
                utils.showFragmentTwo(view.getContext(),fragment);
                break;
        }
    }
}
