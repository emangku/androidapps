package com.ck.it.androidapps.model;

/**
 * Created by Creef on 2/6/2018.
 */

public class Historymodel {
    private String SITE_CODE, USER_ID,
            LATITUDE, LONGITUDE,
            ALAMAT_A, DESA, KECAMATAN,
            KABUPATEN, PROVINSI,
            KODE_POS, created_at,
            updated_at, NAMA_KONTAK,
            NOMOR_TELEPON, ALAMAT_B,
            HUBUNGAN_DENGAN_PEMILIK, NAMA_PEMILIK,
            BADAN_HUKUM, ALAMAT_C,
            NOTLP_OR_FAX_OR_NOHP, EMAIL,
            RENCANA_KERJASAMA, HARGA_PENAWARAN_SEWA,
            PERIODE_SEWA, GRACE_PERIOD,
            STATUS_HARGA, SERVICE_CHARGE,
            BIAYA_PAJAK, BIAYA_NOTARIS,
            SERTIFIKASI, ATAS_NAMA,
            LUAS_TANAH, IJIN_MENDIRIKAN_BANGUNAN,
            TIPE_BANGUNAN, POSISI_BANGUNAN,
            TAHUN_PEMBUATAN, UKURAN_LOKASI,
            UKURAN_BANGUNAN, LEBAR_MUKA_BANGUNAN,
            PANJANG_BANGUNAN, SA_ID,
            TYPE, VALUE_OPTION,
            VALUE_RANGE, AA_ID,
            IMAGE, VIDEO;

    public Historymodel() {
    }

    public Historymodel(String SITE_CODE, String USER_ID,
                        String LATITUDE, String LONGITUDE,
                        String ALAMAT_A, String DESA,
                        String KECAMATAN, String KABUPATEN,
                        String PROVINSI, String KODE_POS,
                        String created_at, String updated_at,
                        String NAMA_KONTAK, String NOMOR_TELEPON,
                        String ALAMAT_B, String HUBUNGAN_DENGAN_PEMILIK,
                        String NAMA_PEMILIK, String BADAN_HUKUM,
                        String ALAMAT_C, String NOTLP_OR_FAX_OR_NOHP,
                        String EMAIL, String RENCANA_KERJASAMA,
                        String HARGA_PENAWARAN_SEWA, String PERIODE_SEWA,
                        String GRACE_PERIOD, String STATUS_HARGA,
                        String SERVICE_CHARGE, String BIAYA_PAJAK,
                        String BIAYA_NOTARIS, String SERTIFIKASI,
                        String ATAS_NAMA, String LUAS_TANAH,
                        String IJIN_MENDIRIKAN_BANGUNAN, String TIPE_BANGUNAN,
                        String POSISI_BANGUNAN, String TAHUN_PEMBUATAN,
                        String UKURAN_LOKASI, String UKURAN_BANGUNAN,
                        String LEBAR_MUKA_BANGUNAN, String PANJANG_BANGUNAN,
                        String SA_ID, String TYPE, String VALUE_OPTION,
                        String VALUE_RANGE, String AA_ID,
                        String IMAGE, String VIDEO) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.LATITUDE = LATITUDE;
        this.LONGITUDE = LONGITUDE;
        this.ALAMAT_A = ALAMAT_A;
        this.DESA = DESA;
        this.KECAMATAN = KECAMATAN;
        this.KABUPATEN = KABUPATEN;
        this.PROVINSI = PROVINSI;
        this.KODE_POS = KODE_POS;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.NAMA_KONTAK = NAMA_KONTAK;
        this.NOMOR_TELEPON = NOMOR_TELEPON;
        this.ALAMAT_B = ALAMAT_B;
        this.HUBUNGAN_DENGAN_PEMILIK = HUBUNGAN_DENGAN_PEMILIK;
        this.NAMA_PEMILIK = NAMA_PEMILIK;
        this.BADAN_HUKUM = BADAN_HUKUM;
        this.ALAMAT_C = ALAMAT_C;
        this.NOTLP_OR_FAX_OR_NOHP = NOTLP_OR_FAX_OR_NOHP;
        this.EMAIL = EMAIL;
        this.RENCANA_KERJASAMA = RENCANA_KERJASAMA;
        this.HARGA_PENAWARAN_SEWA = HARGA_PENAWARAN_SEWA;
        this.PERIODE_SEWA = PERIODE_SEWA;
        this.GRACE_PERIOD = GRACE_PERIOD;
        this.STATUS_HARGA = STATUS_HARGA;
        this.SERVICE_CHARGE = SERVICE_CHARGE;
        this.BIAYA_PAJAK = BIAYA_PAJAK;
        this.BIAYA_NOTARIS = BIAYA_NOTARIS;
        this.SERTIFIKASI = SERTIFIKASI;
        this.ATAS_NAMA = ATAS_NAMA;
        this.LUAS_TANAH = LUAS_TANAH;
        this.IJIN_MENDIRIKAN_BANGUNAN = IJIN_MENDIRIKAN_BANGUNAN;
        this.TIPE_BANGUNAN = TIPE_BANGUNAN;
        this.POSISI_BANGUNAN = POSISI_BANGUNAN;
        this.TAHUN_PEMBUATAN = TAHUN_PEMBUATAN;
        this.UKURAN_LOKASI = UKURAN_LOKASI;
        this.UKURAN_BANGUNAN = UKURAN_BANGUNAN;
        this.LEBAR_MUKA_BANGUNAN = LEBAR_MUKA_BANGUNAN;
        this.PANJANG_BANGUNAN = PANJANG_BANGUNAN;
        this.SA_ID = SA_ID;
        this.TYPE = TYPE;
        this.VALUE_OPTION = VALUE_OPTION;
        this.VALUE_RANGE = VALUE_RANGE;
        this.AA_ID = AA_ID;
        this.IMAGE = IMAGE;
        this.VIDEO = VIDEO;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getLATITUDE() {
        return LATITUDE;
    }

    public void setLATITUDE(String LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public String getLONGITUDE() {
        return LONGITUDE;
    }

    public void setLONGITUDE(String LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
    }

    public String getALAMAT_A() {
        return ALAMAT_A;
    }

    public void setALAMAT_A(String ALAMAT_A) {
        this.ALAMAT_A = ALAMAT_A;
    }

    public String getDESA() {
        return DESA;
    }

    public void setDESA(String DESA) {
        this.DESA = DESA;
    }

    public String getKECAMATAN() {
        return KECAMATAN;
    }

    public void setKECAMATAN(String KECAMATAN) {
        this.KECAMATAN = KECAMATAN;
    }

    public String getKABUPATEN() {
        return KABUPATEN;
    }

    public void setKABUPATEN(String KABUPATEN) {
        this.KABUPATEN = KABUPATEN;
    }

    public String getPROVINSI() {
        return PROVINSI;
    }

    public void setPROVINSI(String PROVINSI) {
        this.PROVINSI = PROVINSI;
    }

    public String getKODE_POS() {
        return KODE_POS;
    }

    public void setKODE_POS(String KODE_POS) {
        this.KODE_POS = KODE_POS;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getNAMA_KONTAK() {
        return NAMA_KONTAK;
    }

    public void setNAMA_KONTAK(String NAMA_KONTAK) {
        this.NAMA_KONTAK = NAMA_KONTAK;
    }

    public String getNOMOR_TELEPON() {
        return NOMOR_TELEPON;
    }

    public void setNOMOR_TELEPON(String NOMOR_TELEPON) {
        this.NOMOR_TELEPON = NOMOR_TELEPON;
    }

    public String getALAMAT_B() {
        return ALAMAT_B;
    }

    public void setALAMAT_B(String ALAMAT_B) {
        this.ALAMAT_B = ALAMAT_B;
    }

    public String getHUBUNGAN_DENGAN_PEMILIK() {
        return HUBUNGAN_DENGAN_PEMILIK;
    }

    public void setHUBUNGAN_DENGAN_PEMILIK(String HUBUNGAN_DENGAN_PEMILIK) {
        this.HUBUNGAN_DENGAN_PEMILIK = HUBUNGAN_DENGAN_PEMILIK;
    }

    public String getNAMA_PEMILIK() {
        return NAMA_PEMILIK;
    }

    public void setNAMA_PEMILIK(String NAMA_PEMILIK) {
        this.NAMA_PEMILIK = NAMA_PEMILIK;
    }

    public String getBADAN_HUKUM() {
        return BADAN_HUKUM;
    }

    public void setBADAN_HUKUM(String BADAN_HUKUM) {
        this.BADAN_HUKUM = BADAN_HUKUM;
    }

    public String getALAMAT_C() {
        return ALAMAT_C;
    }

    public void setALAMAT_C(String ALAMAT_C) {
        this.ALAMAT_C = ALAMAT_C;
    }

    public String getNOTLP_OR_FAX_OR_NOHP() {
        return NOTLP_OR_FAX_OR_NOHP;
    }

    public void setNOTLP_OR_FAX_OR_NOHP(String NOTLP_OR_FAX_OR_NOHP) {
        this.NOTLP_OR_FAX_OR_NOHP = NOTLP_OR_FAX_OR_NOHP;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getRENCANA_KERJASAMA() {
        return RENCANA_KERJASAMA;
    }

    public void setRENCANA_KERJASAMA(String RENCANA_KERJASAMA) {
        this.RENCANA_KERJASAMA = RENCANA_KERJASAMA;
    }

    public String getHARGA_PENAWARAN_SEWA() {
        return HARGA_PENAWARAN_SEWA;
    }

    public void setHARGA_PENAWARAN_SEWA(String HARGA_PENAWARAN_SEWA) {
        this.HARGA_PENAWARAN_SEWA = HARGA_PENAWARAN_SEWA;
    }

    public String getPERIODE_SEWA() {
        return PERIODE_SEWA;
    }

    public void setPERIODE_SEWA(String PERIODE_SEWA) {
        this.PERIODE_SEWA = PERIODE_SEWA;
    }

    public String getGRACE_PERIOD() {
        return GRACE_PERIOD;
    }

    public void setGRACE_PERIOD(String GRACE_PERIOD) {
        this.GRACE_PERIOD = GRACE_PERIOD;
    }

    public String getSTATUS_HARGA() {
        return STATUS_HARGA;
    }

    public void setSTATUS_HARGA(String STATUS_HARGA) {
        this.STATUS_HARGA = STATUS_HARGA;
    }

    public String getSERVICE_CHARGE() {
        return SERVICE_CHARGE;
    }

    public void setSERVICE_CHARGE(String SERVICE_CHARGE) {
        this.SERVICE_CHARGE = SERVICE_CHARGE;
    }

    public String getBIAYA_PAJAK() {
        return BIAYA_PAJAK;
    }

    public void setBIAYA_PAJAK(String BIAYA_PAJAK) {
        this.BIAYA_PAJAK = BIAYA_PAJAK;
    }

    public String getBIAYA_NOTARIS() {
        return BIAYA_NOTARIS;
    }

    public void setBIAYA_NOTARIS(String BIAYA_NOTARIS) {
        this.BIAYA_NOTARIS = BIAYA_NOTARIS;
    }

    public String getSERTIFIKASI() {
        return SERTIFIKASI;
    }

    public void setSERTIFIKASI(String SERTIFIKASI) {
        this.SERTIFIKASI = SERTIFIKASI;
    }

    public String getATAS_NAMA() {
        return ATAS_NAMA;
    }

    public void setATAS_NAMA(String ATAS_NAMA) {
        this.ATAS_NAMA = ATAS_NAMA;
    }

    public String getLUAS_TANAH() {
        return LUAS_TANAH;
    }

    public void setLUAS_TANAH(String LUAS_TANAH) {
        this.LUAS_TANAH = LUAS_TANAH;
    }

    public String getIJIN_MENDIRIKAN_BANGUNAN() {
        return IJIN_MENDIRIKAN_BANGUNAN;
    }

    public void setIJIN_MENDIRIKAN_BANGUNAN(String IJIN_MENDIRIKAN_BANGUNAN) {
        this.IJIN_MENDIRIKAN_BANGUNAN = IJIN_MENDIRIKAN_BANGUNAN;
    }

    public String getTIPE_BANGUNAN() {
        return TIPE_BANGUNAN;
    }

    public void setTIPE_BANGUNAN(String TIPE_BANGUNAN) {
        this.TIPE_BANGUNAN = TIPE_BANGUNAN;
    }

    public String getPOSISI_BANGUNAN() {
        return POSISI_BANGUNAN;
    }

    public void setPOSISI_BANGUNAN(String POSISI_BANGUNAN) {
        this.POSISI_BANGUNAN = POSISI_BANGUNAN;
    }

    public String getTAHUN_PEMBUATAN() {
        return TAHUN_PEMBUATAN;
    }

    public void setTAHUN_PEMBUATAN(String TAHUN_PEMBUATAN) {
        this.TAHUN_PEMBUATAN = TAHUN_PEMBUATAN;
    }

    public String getUKURAN_LOKASI() {
        return UKURAN_LOKASI;
    }

    public void setUKURAN_LOKASI(String UKURAN_LOKASI) {
        this.UKURAN_LOKASI = UKURAN_LOKASI;
    }

    public String getUKURAN_BANGUNAN() {
        return UKURAN_BANGUNAN;
    }

    public void setUKURAN_BANGUNAN(String UKURAN_BANGUNAN) {
        this.UKURAN_BANGUNAN = UKURAN_BANGUNAN;
    }

    public String getLEBAR_MUKA_BANGUNAN() {
        return LEBAR_MUKA_BANGUNAN;
    }

    public void setLEBAR_MUKA_BANGUNAN(String LEBAR_MUKA_BANGUNAN) {
        this.LEBAR_MUKA_BANGUNAN = LEBAR_MUKA_BANGUNAN;
    }

    public String getPANJANG_BANGUNAN() {
        return PANJANG_BANGUNAN;
    }

    public void setPANJANG_BANGUNAN(String PANJANG_BANGUNAN) {
        this.PANJANG_BANGUNAN = PANJANG_BANGUNAN;
    }

    public String getSA_ID() {
        return SA_ID;
    }

    public void setSA_ID(String SA_ID) {
        this.SA_ID = SA_ID;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getVALUE_OPTION() {
        return VALUE_OPTION;
    }

    public void setVALUE_OPTION(String VALUE_OPTION) {
        this.VALUE_OPTION = VALUE_OPTION;
    }

    public String getVALUE_RANGE() {
        return VALUE_RANGE;
    }

    public void setVALUE_RANGE(String VALUE_RANGE) {
        this.VALUE_RANGE = VALUE_RANGE;
    }

    public String getAA_ID() {
        return AA_ID;
    }

    public void setAA_ID(String AA_ID) {
        this.AA_ID = AA_ID;
    }

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public String getVIDEO() {
        return VIDEO;
    }

    public void setVIDEO(String VIDEO) {
        this.VIDEO = VIDEO;
    }
}
