package com.ck.it.androidapps.adapter;

import android.content.Context;
import android.support.annotation.IdRes;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.model.areaanlysis.AreaAnalysisModel;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;
import com.rengwuxian.materialedittext.MaterialEditText;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Creef on 1/29/2018.
 */

public class AreaAnalysisAdapter extends RecyclerView.Adapter<AreaAnalysisAdapter.ViewHolder> {
    private View view;
    private Context mContext;
    private List<AreaAnalysisModel> mlist = new ArrayList<>();
    String opt = "";

    public AreaAnalysisAdapter(Context context, List<AreaAnalysisModel> list) {
        this.mContext = context;
        this.mlist = list;
    }

    @Override
    public AreaAnalysisAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mContext).inflate(R.layout.list_area, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AreaAnalysisAdapter.ViewHolder holder, final int position) {
        holder.viewAreaType.setText(mlist.get(position).getTYPE());
        holder.viewAreaOptions.setText(mlist.get(position).getVALUE_OPTION());
        holder.viewAreaKondisi.setText(mlist.get(position).getVALUE_RANGE());
        holder.viewClickEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View vi = LayoutInflater.from(view.getContext()).inflate(R.layout.dialog_input_edit_areaanalysis, null, false);
                final AppCompatTextView viewID, viewSITECODE, viewUSERID, viewAAID, viewTYPE;
                final MaterialEditText viewOPTIONVALUE;
                final LinearLayout viewCancle, viewUpdate, viewFrameDialog;
                final ProgressBar viewDialog;
                viewDialog = (ProgressBar) vi.findViewById(R.id.viewDialog);
                viewCancle = (LinearLayout) vi.findViewById(R.id.viewCancle);
                viewUpdate = (LinearLayout) vi.findViewById(R.id.viewUpdate);
                viewID = (AppCompatTextView) vi.findViewById(R.id.viewID);
                viewID.setText(mlist.get(position).getID());
                viewSITECODE = (AppCompatTextView) vi.findViewById(R.id.viewSITECODE);
                viewSITECODE.setText(mlist.get(position).getSITE_CODE());
                viewUSERID = (AppCompatTextView) vi.findViewById(R.id.viewUSERID);
                viewUSERID.setText(mlist.get(position).getUSER_ID());
                viewAAID = (AppCompatTextView) vi.findViewById(R.id.viewAAID);
                viewAAID.setText(mlist.get(position).getAA_ID());
                viewTYPE = (AppCompatTextView) vi.findViewById(R.id.viewTYPE);
                viewTYPE.setText(mlist.get(position).getTYPE());
                viewOPTIONVALUE = (MaterialEditText) vi.findViewById(R.id.viewOPTIONVALUE);
                viewOPTIONVALUE.setText(mlist.get(position).getVALUE_OPTION());
                RadioGroup viewGroupEdit;
                viewFrameDialog = (LinearLayout) vi.findViewById(R.id.viewFrameDialog);
                viewGroupEdit = (RadioGroup) vi.findViewById(R.id.viewGroupEdit);
                viewGroupEdit.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                        switch (checkedId) {
                            case R.id.edit_aa_1:
                                opt = "1";
                                break;
                            case R.id.edit_aa_2:
                                opt = "2";
                                break;
                            case R.id.edit_aa_3:
                                opt = "3";
                                break;
                            case R.id.edit_aa_4:
                                opt = "4";
                                break;
                            case R.id.edit_aa_5:
                                opt = "5";
                                break;
                        }
                    }
                });
                final AlertDialog dialog = utils.customeAlertInputDialog(view.getContext(), vi);
                viewCancle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                    }
                });
                viewUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        viewDialog.setVisibility(View.VISIBLE);
                        viewFrameDialog.setVisibility(View.GONE);
                        AndroidNetworking.post(api.SERVER_API + "update_area_analysisID")
                                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                                .addBodyParameter("id", viewID.getText().toString())
                                .addBodyParameter(params.SITE_CODE, viewSITECODE.getText().toString())
                                .addBodyParameter(params.USER_ID, viewUSERID.getText().toString())
                                .addBodyParameter(params.AA_ID, viewAAID.getText().toString())
                                .addBodyParameter(params.VALUE_OPTION, viewOPTIONVALUE.getText().toString())
                                .addBodyParameter(params.VALUE_RANGE, opt)
                                .build()
                                .getAsJSONObject(new JSONObjectRequestListener() {
                                    @Override
                                    public void onResponse(JSONObject response) {
                                        try {
                                            if (response.getString("status").equals("true")) {
                                                viewDialog.setVisibility(View.GONE);
                                                viewFrameDialog.setVisibility(View.VISIBLE);
                                                dialog.dismiss();
                                            }
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                            viewDialog.setVisibility(View.GONE);
                                            viewFrameDialog.setVisibility(View.VISIBLE);
                                            dialog.dismiss();
                                        }
                                    }

                                    @Override
                                    public void onError(ANError anError) {
                                        Toast.makeText(view.getContext(), "Opps Error", Toast.LENGTH_LONG).show();
                                        viewDialog.setVisibility(View.GONE);
                                        viewFrameDialog.setVisibility(View.VISIBLE);
                                    }
                                });
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (mlist != null) {
            return mlist.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView viewAreaType, viewAreaOptions, viewAreaKondisi;
        LinearLayout viewClickEdit;

        public ViewHolder(View itemView) {
            super(itemView);
            viewAreaType = (TextView) itemView.findViewById(R.id.viewAreaType);
            viewAreaOptions = (TextView) itemView.findViewById(R.id.viewAreaOptions);
            viewAreaKondisi = (TextView) itemView.findViewById(R.id.viewAreaKondisi);
            viewClickEdit = (LinearLayout) itemView.findViewById(R.id.viewClickEdit);
        }
    }
}
