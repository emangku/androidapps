package com.ck.it.androidapps.fragment.photodocument;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ck.it.androidapps.R;

/**
 * Created by Creef on 1/4/2018.
 */

public class KtpUploadScanner extends Fragment implements View.OnClickListener {
    private View view;
    int REQUEST_CODE = 99;
    ImageView img;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ktp_scaner_upload, container,false);
        initView();
        return view;
    }

    void initView(){
        img = (ImageView)view.findViewById(R.id.ktp_scanner);
        img.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.ktp_scanner:

                break;
        }
    }

}
