package com.ck.it.androidapps.model;

/**
 * Created by eko on 06/03/18.
 */

public class ModelFSDetails {
    private String SITE_CODE, USER_ID,
            RENT, RENOVATION,
            EQUIPMENT, TOTAL_INVESTMENT,
            created_at, updated_at,
            BEST, AVERAGE,
            WORSE, GP_STANDARD,
            STORE_ID, SALES,
            GP, EMPLOYMENT,
            UTILITY, REPAIR_MAINTENANCE,
            STORE_USE, OTHERS,
            DESCRIPTION, NSC,
            NO_OF_STAFF, SL,
            CSR, UTILITES, OTHER;

    public ModelFSDetails() {
    }

    public ModelFSDetails(String SITE_CODE, String USER_ID,
                          String RENT, String RENOVATION,
                          String EQUIPMENT, String TOTAL_INVESTMENT,
                          String created_at, String updated_at,
                          String BEST, String AVERAGE,
                          String WORSE, String GP_STANDARD,
                          String STORE_ID, String SALES,
                          String GP, String EMPLOYMENT,
                          String UTILITY, String REPAIR_MAINTENANCE,
                          String STORE_USE, String OTHERS,
                          String DESCRIPTION, String NSC,
                          String NO_OF_STAFF, String SL,
                          String CSR, String UTILITES, String OTHER) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.RENT = RENT;
        this.RENOVATION = RENOVATION;
        this.EQUIPMENT = EQUIPMENT;
        this.TOTAL_INVESTMENT = TOTAL_INVESTMENT;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.BEST = BEST;
        this.AVERAGE = AVERAGE;
        this.WORSE = WORSE;
        this.GP_STANDARD = GP_STANDARD;
        this.STORE_ID = STORE_ID;
        this.SALES = SALES;
        this.GP = GP;
        this.EMPLOYMENT = EMPLOYMENT;
        this.UTILITY = UTILITY;
        this.REPAIR_MAINTENANCE = REPAIR_MAINTENANCE;
        this.STORE_USE = STORE_USE;
        this.OTHERS = OTHERS;
        this.DESCRIPTION = DESCRIPTION;
        this.NSC = NSC;
        this.NO_OF_STAFF = NO_OF_STAFF;
        this.SL = SL;
        this.CSR = CSR;
        this.UTILITES = UTILITES;
        this.OTHER = OTHER;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getRENT() {
        return RENT;
    }

    public void setRENT(String RENT) {
        this.RENT = RENT;
    }

    public String getRENOVATION() {
        return RENOVATION;
    }

    public void setRENOVATION(String RENOVATION) {
        this.RENOVATION = RENOVATION;
    }

    public String getEQUIPMENT() {
        return EQUIPMENT;
    }

    public void setEQUIPMENT(String EQUIPMENT) {
        this.EQUIPMENT = EQUIPMENT;
    }

    public String getTOTAL_INVESTMENT() {
        return TOTAL_INVESTMENT;
    }

    public void setTOTAL_INVESTMENT(String TOTAL_INVESTMENT) {
        this.TOTAL_INVESTMENT = TOTAL_INVESTMENT;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getBEST() {
        return BEST;
    }

    public void setBEST(String BEST) {
        this.BEST = BEST;
    }

    public String getAVERAGE() {
        return AVERAGE;
    }

    public void setAVERAGE(String AVERAGE) {
        this.AVERAGE = AVERAGE;
    }

    public String getWORSE() {
        return WORSE;
    }

    public void setWORSE(String WORSE) {
        this.WORSE = WORSE;
    }

    public String getGP_STANDARD() {
        return GP_STANDARD;
    }

    public void setGP_STANDARD(String GP_STANDARD) {
        this.GP_STANDARD = GP_STANDARD;
    }

    public String getSTORE_ID() {
        return STORE_ID;
    }

    public void setSTORE_ID(String STORE_ID) {
        this.STORE_ID = STORE_ID;
    }

    public String getSALES() {
        return SALES;
    }

    public void setSALES(String SALES) {
        this.SALES = SALES;
    }

    public String getGP() {
        return GP;
    }

    public void setGP(String GP) {
        this.GP = GP;
    }

    public String getEMPLOYMENT() {
        return EMPLOYMENT;
    }

    public void setEMPLOYMENT(String EMPLOYMENT) {
        this.EMPLOYMENT = EMPLOYMENT;
    }

    public String getUTILITY() {
        return UTILITY;
    }

    public void setUTILITY(String UTILITY) {
        this.UTILITY = UTILITY;
    }

    public String getREPAIR_MAINTENANCE() {
        return REPAIR_MAINTENANCE;
    }

    public void setREPAIR_MAINTENANCE(String REPAIR_MAINTENANCE) {
        this.REPAIR_MAINTENANCE = REPAIR_MAINTENANCE;
    }

    public String getSTORE_USE() {
        return STORE_USE;
    }

    public void setSTORE_USE(String STORE_USE) {
        this.STORE_USE = STORE_USE;
    }

    public String getOTHERS() {
        return OTHERS;
    }

    public void setOTHERS(String OTHERS) {
        this.OTHERS = OTHERS;
    }

    public String getDESCRIPTION() {
        return DESCRIPTION;
    }

    public void setDESCRIPTION(String DESCRIPTION) {
        this.DESCRIPTION = DESCRIPTION;
    }

    public String getNSC() {
        return NSC;
    }

    public void setNSC(String NSC) {
        this.NSC = NSC;
    }

    public String getNO_OF_STAFF() {
        return NO_OF_STAFF;
    }

    public void setNO_OF_STAFF(String NO_OF_STAFF) {
        this.NO_OF_STAFF = NO_OF_STAFF;
    }

    public String getSL() {
        return SL;
    }

    public void setSL(String SL) {
        this.SL = SL;
    }

    public String getCSR() {
        return CSR;
    }

    public void setCSR(String CSR) {
        this.CSR = CSR;
    }

    public String getUTILITES() {
        return UTILITES;
    }

    public void setUTILITES(String UTILITES) {
        this.UTILITES = UTILITES;
    }

    public String getOTHER() {
        return OTHER;
    }

    public void setOTHER(String OTHER) {
        this.OTHER = OTHER;
    }
}
