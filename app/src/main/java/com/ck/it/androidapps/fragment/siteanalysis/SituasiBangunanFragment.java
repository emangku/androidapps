package com.ck.it.androidapps.fragment.siteanalysis;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Creef on 12/6/2017.
 */

@SuppressWarnings("deprecation")
public class SituasiBangunanFragment extends Fragment implements View.OnClickListener {
    private View view;
    Calendar calendar;
    DatePicker datePicker;
    DatePickerDialog datePickerDialog;
    int year, month, day;
    Button viewNextStatusBangunan;
    EditText viewTahunPembuatan, viewUkuranLokasi, viewUkuranBangunan, viewLebarMukaJalan, viewPanjangBangunan;
    RadioGroup viewPosisiBangunan, viewTipeBangunan;
    String tipeBangunan, posisiBangunan = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.status_bangunan, container, false);
        datePicker = new DatePicker(view.getContext());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        }
        initView();
        return view;
    }

    void initView() {
        viewNextStatusBangunan = (Button) view.findViewById(R.id.viewNextStatusBangunan);
        viewNextStatusBangunan.setOnClickListener(this);
        viewTipeBangunan = (RadioGroup) view.findViewById(R.id.viewTipeBangunan);
        viewTipeBangunan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.vRuko:
                        tipeBangunan = "Ruko";
                        break;
                    case R.id.vRumahTinggal:
                        tipeBangunan = "Rumah Tinggal";
                        break;
                }
            }
        });
        viewPosisiBangunan = (RadioGroup) view.findViewById(R.id.viewPosisiBangunan);
        viewPosisiBangunan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.vCorner:
                        posisiBangunan = "Corner";
                        break;
                    case R.id.vMidBlock:
                        posisiBangunan = "Mid Block";
                        break;
                }
            }
        });
        viewTahunPembuatan = (EditText) view.findViewById(R.id.viewTahunPembuatan);
        viewTahunPembuatan.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    datePickerDialog = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int years, int months, int dayOfMonth) {
                            try {
                                SimpleDateFormat date = new SimpleDateFormat("dd/mm/yyyy");
                                Date date1 = date.parse(dayOfMonth + "/" + (months + 1) + "/" + years);
                                SimpleDateFormat dateFormat = new SimpleDateFormat("dd/mm/yyyy");
                                String finalDate = dateFormat.format(date1);
                                viewTahunPembuatan.setText(finalDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, year, month, day);
                    datePicker.init(year, month, day, null);
                    datePickerDialog.show();
                } else {
//                    Toasty.error(view.getContext(),"Oops..", Toast.LENGTH_LONG).show();
                }
            }
        });

        viewUkuranLokasi = (EditText) view.findViewById(R.id.viewUkuranLokasi);
        viewUkuranLokasi.requestFocus();
        viewUkuranBangunan = (EditText) view.findViewById(R.id.viewUkuranBangunan);
        viewLebarMukaJalan = (EditText) view.findViewById(R.id.viewLebarMukaJalan);
        viewPanjangBangunan = (EditText) view.findViewById(R.id.viewPanjangBangunan);
    }

    void saveData(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), "Tunngu...");
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.TIPE_BANGUNAN, tipeBangunan)
                .addBodyParameter(params.POSISI_BANGUNAN, posisiBangunan)
                .addBodyParameter(params.TAHUN_PEMBUATAN, viewTahunPembuatan.getText().toString())
                .addBodyParameter(params.UKURAN_LOKASI, viewUkuranLokasi.getText().toString())
                .addBodyParameter(params.UKURAN_BANGUNAN, viewUkuranBangunan.getText().toString())
                .addBodyParameter(params.LEBAR_MUKA_BANGUNAN, viewLebarMukaJalan.getText().toString())
                .addBodyParameter(params.PANJANG_BANGUNAN, viewPanjangBangunan.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new KBBahanAtapFragment());
                                getActivity().setTitle(R.string.kondisi_bangunan);
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(), "Opss, Error..", Toast.LENGTH_LONG).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextStatusBangunan:
                if (TextUtils.isEmpty(tipeBangunan)) {
                    Toast.makeText(view.getContext(), "Silahkan Tipe Bangunan !", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(posisiBangunan)) {
                    Toast.makeText(view.getContext(), "Silahkan Posisi Bangunan !", Toast.LENGTH_LONG).show();
                } else if (TextUtils.isEmpty(viewTahunPembuatan.getText().toString())) {
                    viewTahunPembuatan.setError("Tidak Boleh Kosong");
                } else if (TextUtils.isEmpty(viewUkuranLokasi.getText().toString())) {
                    viewUkuranLokasi.setError("Tidak Boleh Kosong");
                } else if (TextUtils.isEmpty(viewUkuranBangunan.getText().toString())) {
                    viewUkuranBangunan.setError("Tidak Boleh Kosong");
                } else if (TextUtils.isEmpty(viewLebarMukaJalan.getText().toString())) {
                    viewLebarMukaJalan.setError("Tidak Boleh Kosong");
                } else if (TextUtils.isEmpty(viewPanjangBangunan.getText().toString())) {
                    viewPanjangBangunan.setError("Tidak Boleh Kosong");
                } else {
                    saveData(params.URL_METHOD.SAVE_SITUASI_BANGUNAN);
                }
                break;
        }
    }
}
