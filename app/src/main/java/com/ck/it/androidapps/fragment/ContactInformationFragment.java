package com.ck.it.androidapps.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 12/5/2017.
 */

@SuppressWarnings("deprecation")
public class ContactInformationFragment extends Fragment implements View.OnClickListener {
    private View view;
    Button viewNextContact;
    EditText viewNamaKontak, viewNomorTelepon, viewKAlamat, viewHubunganDenganPemilik;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact, container, false);
        initView();
        return view;
    }

    void initView() {
        viewNextContact = (Button) view.findViewById(R.id.viewNextContact);
        viewNextContact.setOnClickListener(this);


        viewNamaKontak = (EditText) view.findViewById(R.id.viewNamaKontak);
        viewNomorTelepon = (EditText) view.findViewById(R.id.viewNomorTelepon);
        viewKAlamat = (EditText) view.findViewById(R.id.viewKAlamat);
        viewHubunganDenganPemilik = (EditText) view.findViewById(R.id.viewHubunganDenganPemilik);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextContact:
                if (TextUtils.isEmpty(viewNamaKontak.getText().toString())) {
                    viewNamaKontak.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewNomorTelepon.getText().toString())) {
                    viewNomorTelepon.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewKAlamat.getText().toString())) {
                    viewKAlamat.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewHubunganDenganPemilik.getText().toString())) {
                    viewHubunganDenganPemilik.setError(params.MValues.field_error_msg);
                } else {
                    saveInfoKontak(params.URL_METHOD.SAVE_INFO_KONTAK);
                }
                break;
        }
    }

    void saveInfoKontak(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.NAMA_KONTAK, viewNamaKontak.getText().toString())
                .addBodyParameter(params.NOMOR_TELEPON, viewNomorTelepon.getText().toString())
                .addBodyParameter(params.K_ALAMAT, viewKAlamat.getText().toString())
                .addBodyParameter(params.HUBUNGAN_DENGAN_PEMILIK, viewHubunganDenganPemilik.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new OwnershipInformationFragment());
                                getActivity().setTitle(R.string.informasi_kepemilikan);
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(), "Opps, Error..", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
