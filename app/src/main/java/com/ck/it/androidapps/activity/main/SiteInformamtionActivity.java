package com.ck.it.androidapps.activity.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.fragment.LocationInfoFragment;

import net.steamcrafted.loadtoast.LoadToast;

public class SiteInformamtionActivity extends AppCompatActivity {
    Fragment fragment = null;
    FragmentManager fragmentManager = getSupportFragmentManager();
    LoadToast loadToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_site_informamtion);
        setTitle(R.string.informasi_lokasi);
        fragment = new LocationInfoFragment();
        fragmentManager
                .beginTransaction()
                .addToBackStack(null)
                .replace(R.id.frame, fragment)
                .commit();


    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            Intent intent = new Intent(SiteInformamtionActivity.this, HomeActivity.class);
            startActivity(intent);
        } else {
            super.onBackPressed();
        }
    }
}
