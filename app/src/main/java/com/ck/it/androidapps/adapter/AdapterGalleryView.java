package com.ck.it.androidapps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.main.FullScreenGaleryActivity;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.model.ModelGalleryView;
import com.ck.it.androidapps.widget.SquareImageView;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Creef on 3/19/2018.
 */

public class AdapterGalleryView extends RecyclerView.Adapter<AdapterGalleryView.ViewHolder> {
    private List<ModelGalleryView> galleryList;
    private Context context;
    private AlertDialog.Builder builder;
    private View views;
    private LinearLayout viewFullScreen, viewCommetLegal;
    public AdapterGalleryView(Context context, List<ModelGalleryView> galleryList) {
        this.context = context;
        this.galleryList = galleryList;
    }

    @Override
    public AdapterGalleryView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.legal_history_list, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final AdapterGalleryView.ViewHolder viewHolder, final int position) {
        views = LayoutInflater.from(context).inflate(R.layout.legal_opt, null, false);

        viewHolder.title.setText(galleryList.get(position).getDOC_NAME());
        Picasso.with(context)
                .load(api.SERVER_API_IMG+galleryList.get(position).getDOCUMENT())
                .resize(100, 100)
                .centerCrop()
                .into(viewHolder.img);
        viewHolder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder = new AlertDialog.Builder(context);
                builder.setView(views);
                viewCommetLegal = (LinearLayout) views.findViewById(R.id.viewCommetLegal);
                viewFullScreen = (LinearLayout) views.findViewById(R.id.viewFullScreen);
                final AlertDialog dialog = builder.create();
                viewCommetLegal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.cancel();
                        rmView();
                    }
                });
                viewFullScreen.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intn = new Intent(context, FullScreenGaleryActivity.class);
                        intn.putExtra("url", galleryList.get(position).getDOCUMENT());
                        context.startActivity(intn);
                        dialog.cancel();
                        rmView();
                    }
                });
                dialog.show();

            }
        });
        viewHolder.frm_clik.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    public int getItemCount() {
        if (galleryList != null){
            return galleryList.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        SquareImageView img;
        TextView title;
        FrameLayout frm_clik;
        public ViewHolder(View itemView) {
            super(itemView);
            img = (SquareImageView)itemView.findViewById(R.id.thumbnail);
            title = (TextView)itemView.findViewById(R.id.title);
            frm_clik = (FrameLayout) itemView.findViewById(R.id.viewFrameClick);
        }
    }
    public void rmView(){
        if (views !=null){
            ViewGroup vg = (ViewGroup) views.getParent();
            vg.removeView(views);
        }
    }
}