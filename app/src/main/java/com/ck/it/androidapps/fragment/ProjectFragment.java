package com.ck.it.androidapps.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.main.EquipmentActivity;
import com.ck.it.androidapps.activity.main.KonstruksiActivity;
import com.ck.it.androidapps.activity.main.OpeningActivity;
import com.goka.blurredgridmenu.GridMenu;
import com.goka.blurredgridmenu.GridMenuFragment;

import java.util.ArrayList;
import java.util.List;

public class ProjectFragment extends Fragment {
    private View view;
    private GridMenuFragment mGridMenuFragment;
    private ProjectFragment projectFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.project_fragment, container, false);
        initview();
        return view;
    }

    void initview() {
        mGridMenuFragment = GridMenuFragment.newInstance(R.drawable.splash);
        ((FragmentActivity) view.getContext()).getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame, mGridMenuFragment)
                .addToBackStack(null)
                .commit();
        setupGrdMenu();
        mGridMenuFragment.setOnClickMenuListener(new GridMenuFragment.OnClickMenuListener() {
            @Override
            public void onClickMenu(GridMenu gridMenu, int position) {
                if (gridMenu.getTitle().equals("Konstruksi")) {
                    startActivityForResult(new Intent(view.getContext(), KonstruksiActivity.class), 0);
                } else if (gridMenu.getTitle().equals("Equipment")) {
                    startActivityForResult(new Intent(view.getContext(), EquipmentActivity.class), 1);
                } else if (gridMenu.getTitle().equals("Opening")) {
                    startActivityForResult(new Intent(view.getContext(), OpeningActivity.class), 2);
                }
            }
        });
    }

    private void setupGrdMenu() {
        List<GridMenu> menus = new ArrayList<>();
        menus.add(new GridMenu("Konstruksi", R.drawable.ico));
        menus.add(new GridMenu("Equipment", R.drawable.ico));
        menus.add(new GridMenu("Opening", R.drawable.ico));
        mGridMenuFragment.setupMenu(menus);
    }

    public ProjectFragment newInstace() {
        return projectFragment;
    }
}
