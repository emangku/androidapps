package com.ck.it.androidapps.fragment.siteanalysis;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 12/21/2017.
 */

@SuppressWarnings("deprecation")
public class SebelahKananFragment extends Fragment implements View.OnClickListener {

    private View view;
    String[] kanan = {"-Sebelah Kanan-", "Rumah", "Toko", "Kantor", "Bangunan", "Commercial", "Sekolah", "Tempat Ibadah", "Lahan Kosong"};
    ArrayAdapter<String> kananAdapter;
    Spinner viewPenyandingKanan;
    Button viewNextPenyandingKanan;
    RadioGroup viewGroupPenyandingKanan;
    String type_opt = "";
    String opt = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.sebelah_kanan_fragment, container, false);
        initView();
        return view;
    }

    void initView() {
        viewPenyandingKanan = (Spinner) view.findViewById(R.id.viewPenyandingKanan);
        kananAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, kanan);
        viewPenyandingKanan.setAdapter(kananAdapter);
        viewPenyandingKanan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type_opt = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        viewGroupPenyandingKanan = (RadioGroup) view.findViewById(R.id.viewGroupPenyandingKanan);
        viewGroupPenyandingKanan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.pkanan_1:
                        opt = "1";
                        break;
                    case R.id.pkanan_2:
                        opt = "2";
                        break;
                    case R.id.pkanan_3:
                        opt = "3";
                        break;
                    case R.id.pkanan_4:
                        opt = "4";
                        break;
                    case R.id.pkanan_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewNextPenyandingKanan = (Button) view.findViewById(R.id.viewNextPenyandingKanan);
        viewNextPenyandingKanan.setOnClickListener(this);
    }

    void saveData(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.SA_ID, "SA")
                .addBodyParameter(params.TYPE, "Sebelah Kanan")
                .addBodyParameter(params.VALUE_OPTION, type_opt)
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new VisibilityBangunanDariSisiKiri());
                                getActivity().setTitle("Visibility (50-100 m)");
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext().getApplicationContext(), "Oppss, Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextPenyandingKanan:
                if (type_opt.equals("") || type_opt.equals("-Sebelah Kanan-")) {
                    Toast.makeText(view.getContext().getApplicationContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (opt.equals("")) {
                    Toast.makeText(view.getContext().getApplicationContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else {
                    saveData(params.URL_METHOD.SAVE_SITE_ANALYSIS);
                }
                break;
        }
    }
}
