package com.ck.it.androidapps.helpers;

import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Creef on 3/19/2018.
 */

public class Test {
    public static void main(String[] args) {
        Map<String, Object> lis = new HashMap<String, Object>();
        lis.put("idFlowData",0);
        lis.put("alamat","AA");
        lis.put("alasan","AA");
        lis.put("kdCutiLokasi","1");
        lis.put("kdCutiJenis","AA");
        lis.put("tglAwal","AA");
        lis.put("tglAkhir","AA");
        lis.put("lamaCuti","AA");
        lis.put("kdCutiPengajuan","AA");
        Gson gson = new Gson();
        String data = gson.toJson(lis);
        System.out.println(data);
    }
}
