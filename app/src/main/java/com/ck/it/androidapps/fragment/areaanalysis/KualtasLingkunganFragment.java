package com.ck.it.androidapps.fragment.areaanalysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.util.utils;

/**
 * Created by Creef on 12/7/2017.
 */

public class KualtasLingkunganFragment extends Fragment implements View.OnClickListener{
    private View view;
    String[] arsitektur = {
            "-Arsitektur Bangunan Sekitar-",
            "Sangat Bagus",
            "Bagus",
            "Sedang",
            "Jelek",
            "Sangat Jelek"};
    String[] kjalan = {
            "-Arsitektur Bangunan Sekitar-",
            "Sangat Bagus",
            "Bagus",
            "Sedang",
            "Jelek",
            "Sangat Jelek"};
    String[] ktrotoar = {
            "-Arsitektur Bangunan Sekitar-",
            "Sangat Bagus",
            "Bagus",
            "Sedang",
            "Jelek",
            "Sangat Jelek"};
    String[] kdrainase = {
            "-Arsitektur Bangunan Sekitar-",
            "Sangat Bagus",
            "Bagus",
            "Sedang",
            "Jelek",
            "Sangat Jelek"};
    Spinner viewArsitekturBangunanSekitar,viewKualitasJalan,viewKualitasTrotoar,viewKualitasDrainase;
    Button viewNextKualitasLingkungan;
    ArrayAdapter<String> abAdapter,kjAdapter, ktAdapter,kdAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.kualitaslingkungan_fragment,container,false);
        initView();
        return view;
    }

    void  initView(){
        viewNextKualitasLingkungan = (Button)view.findViewById(R.id.viewNextKualitasLingkungan);
        viewNextKualitasLingkungan.setOnClickListener(this);

        viewArsitekturBangunanSekitar = (Spinner)view.findViewById(R.id.viewArsitekturBangunanSekitar);
        abAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner,arsitektur);
        viewArsitekturBangunanSekitar.setAdapter(abAdapter);

        viewKualitasJalan = (Spinner)view.findViewById(R.id.viewKualitasJalan);
        kjAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner,kjalan);
        viewKualitasJalan.setAdapter(kjAdapter);

        viewKualitasTrotoar = (Spinner)view.findViewById(R.id.viewKualitasTrotoar);
        ktAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner,ktrotoar);
        viewKualitasTrotoar.setAdapter(ktAdapter);

        viewKualitasDrainase = (Spinner)view.findViewById(R.id.viewKualitasDrainase);
        kdAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner,kdrainase);
        viewKualitasDrainase.setAdapter(kdAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextKualitasLingkungan:
                utils.showFragmentThree(view.getContext(), new TrafficFlowFragment());
                getActivity().setTitle("Traffic Flow");
                break;
        }
    }
}
