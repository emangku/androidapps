package com.ck.it.androidapps.model;

/**
 * Created by Creef on 3/5/2018.
 */

public class MappingModels {
    private String SITE_CODE;
    private String USER_ID;
    private String IMAGE;
    private String LATITUDE;
    private String LONGITUDE;
    private String created_at;
    private String updated_at;

    public MappingModels() {
    }

    public MappingModels(String SITE_CODE, String USER_ID, String IMAGE, String LATITUDE, String LONGITUDE, String created_at, String updated_at) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.IMAGE = IMAGE;
        this.LATITUDE = LATITUDE;
        this.LONGITUDE = LONGITUDE;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public String getLATITUDE() {
        return LATITUDE;
    }

    public void setLATITUDE(String LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public String getLONGITUDE() {
        return LONGITUDE;
    }

    public void setLONGITUDE(String LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
