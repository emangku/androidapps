package com.ck.it.androidapps.adapter;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.ApprovalStatus;
import com.ck.it.androidapps.helpers.Orientation;
import com.ck.it.androidapps.model.TimeLineModel;
import com.ck.it.androidapps.util.VectorDrawableUtils;
import com.ck.it.androidapps.util.utils;
import com.github.vipulasri.timelineview.TimelineView;

import java.util.List;

/**
 * Created by Creef on 12/19/2017.
 */

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ViewHolder> {
    private List<TimeLineModel> mFeedList;
    private Context context;
    private Orientation orientation;
    private boolean mWithLinePadding;
    private LayoutInflater mLayoutInflater;

    public HistoryAdapter(List<TimeLineModel> mFeedList, Orientation orientation, boolean mWithLinePadding) {
        this.mFeedList = mFeedList;
        this.orientation = orientation;
        this.mWithLinePadding = mWithLinePadding;
    }

    @Override
    public HistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        mLayoutInflater = LayoutInflater.from(context);
        View view;
        if (orientation == Orientation.HORIZONTAL) {
            view = mLayoutInflater.inflate(mWithLinePadding ? R.layout.list_horizontal_history_padding : R.layout.list_horizontal_history, parent, false);
        } else {
            view = mLayoutInflater.inflate(mWithLinePadding ? R.layout.list_time_line_padding : R.layout.list_time_line_history, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.ViewHolder holder, int position) {
        TimeLineModel timeLineModel = mFeedList.get(position);
        if (timeLineModel.getApprovalStatus() == ApprovalStatus.INACTIVE) {
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(context, R.drawable.ic_maker_inactive, R.color.colorPrimary));
        } else if (timeLineModel.getApprovalStatus() == ApprovalStatus.ACTIVE) {
            holder.mTimelineView.setMarker(VectorDrawableUtils.getDrawable(context, R.drawable.ic_marker_active, R.color.colorPrimary));
        } else {
            holder.mTimelineView.setMarker(ContextCompat.getDrawable(context, R.drawable.ic_marker), ContextCompat.getColor(context, R.color.colorPrimary));
        }

        if (!timeLineModel.getTANGGAL().isEmpty()) {
            holder.text_timeline_date.setVisibility(View.VISIBLE);
            holder.text_timeline_date.setText(utils.setParseDateTime(timeLineModel.getTANGGAL(), "yyyy-MM-dd HH:mm", "hh:mm a, dd-MMM-yyyy"));
        } else
            holder.text_timeline_date.setVisibility(View.GONE);
        holder.text_timeline_title.setText(timeLineModel.getLOCATION());
    }

    @Override
    public int getItemCount() {
        return (mFeedList != null ? mFeedList.size() : 0);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TimelineView mTimelineView;
        AppCompatTextView text_timeline_date, text_timeline_title;

        public ViewHolder(View itemView) {
            super(itemView);
            mTimelineView = (TimelineView) itemView.findViewById(R.id.time_marker);
            text_timeline_date = (AppCompatTextView) itemView.findViewById(R.id.text_timeline_date);
            text_timeline_title = (AppCompatTextView) itemView.findViewById(R.id.text_timeline_title);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return TimelineView.getTimeLineViewType(position, getItemCount());
    }
}
