package com.ck.it.androidapps.fragment.siteanalysis;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 12/21/2017.
 */

@SuppressWarnings("deprecation")
public class KapasitasListrikFragment extends Fragment implements View.OnClickListener {

    private View view;
    String[] kapasitaslistrik = {"-Kapasitas Listrik-", "Tidak Ada", "< 2200 watt", "2200-4400 watt", "4400-10000 watt", "10000-20000 watt", "> 20000 watt"};
    Spinner viewKapasitasListrik;
    Button viewNextKapasitasListrik;
    ArrayAdapter<String> klAdapter;
    RadioGroup optKapasitasListrik;
    String opt = "";
    String type_opt = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.kapasitas_listrik, container, false);
        initView();
        return view;
    }

    void initView() {
        viewKapasitasListrik = (Spinner) view.findViewById(R.id.viewKapasitasListrik);
        klAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, kapasitaslistrik);
        viewKapasitasListrik.setAdapter(klAdapter);

        viewKapasitasListrik.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type_opt = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        optKapasitasListrik = (RadioGroup) view.findViewById(R.id.optKapasitasListrik);
        optKapasitasListrik.setOnClickListener(this);

        optKapasitasListrik = (RadioGroup) view.findViewById(R.id.optKapasitasListrik);
        optKapasitasListrik.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.kls_1:
                        opt = "1";
                        break;
                    case R.id.kls_2:
                        opt = "2";
                        break;
                    case R.id.kls_3:
                        opt = "3";
                        break;
                    case R.id.kls_4:
                        opt = "4";
                        break;
                    case R.id.kls_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewNextKapasitasListrik = (Button) view.findViewById(R.id.viewNextKapasitasListrik);
        viewNextKapasitasListrik.setOnClickListener(this);
    }

    void saveData(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.SA_ID, "SA-KB")
                .addBodyParameter(params.TYPE, "Kapasitas Listrik")
                .addBodyParameter(params.VALUE_OPTION, type_opt)
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new JaringanAirFragment());
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext().getApplicationContext(), "Oppss, Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextKapasitasListrik:
                if (type_opt.equals("") || type_opt.equals("-Kapasitas Listrik-")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_LONG).show();
                } else if (opt.equals("")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_LONG).show();
                } else {
                    saveData(params.URL_METHOD.SAVE_SITE_ANALYSIS);
                }
                break;
        }
    }
}
