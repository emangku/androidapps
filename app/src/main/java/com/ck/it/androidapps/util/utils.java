package com.ck.it.androidapps.util;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.icu.util.Calendar;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import android.widget.VideoView;

import com.androidnetworking.common.ANRequest;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.params;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import net.steamcrafted.loadtoast.LoadToast;

import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Creef on 12/5/2017.
 */

@SuppressWarnings("deprecation")
public class utils {
    private static SharedPreferences shared;
    public static LoadToast showDialog(Context context, String message){
        LoadToast load = new LoadToast(context);
        load.setText(message);
        load.setTextColor(Color.WHITE);
        load.setBackgroundColor(Color.RED);
        load.setTranslationY(700);
        load.show();
        return load;
    }
    public static void setDataSession(Context context, String key, String values){
         shared = context.getSharedPreferences(params.SESSION_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(key,values);
        editor.commit();
    }

    public static String getDataSession(Context context, String values){
        if (shared !=null){
            return shared.getString(values, null);
        }
        return  "";
    }
    public static void showFragment(Context context,Fragment fragment){
        ((FragmentActivity)context)
                .getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .replace(R.id.frame,fragment)
                .commit();
    }

    public static void showFragmentTwo(Context context,Fragment fragment){
        ((FragmentActivity)context)
                .getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .replace(R.id.framesa,fragment)
                .commit();
    }

    public static void showFragmentThree(Context context,Fragment fragment){
        ((FragmentActivity)context)
                .getSupportFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .addToBackStack(null)
                .replace(R.id.frm,fragment)
                .commit();
    }

    public static void setDataSessionExtraIntent(Context context, String session, String value){
        Bundle bundle = new Bundle();
        bundle.putString(session,value);
    }
    public static String getDataSessionExtraIntent(Context context , String value){
        Bundle bundle = new Bundle();
        String session = bundle.getString(value);

        return session;
    }
    public static Bitmap addWaterMark(Context context, Bitmap src , String watermark, int x, int y, int color, int alpha, int size, boolean underline){
        TextView txtWaterMark = new TextView(context);
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());

        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(src, 0, 0, null);

        Paint paint = new Paint();
        paint.setColor(color);
        paint.setAlpha(alpha);
        paint.setTextSize(size);
        paint.setAntiAlias(true);
        paint.setUnderlineText(underline);
        canvas.drawText(watermark, x, y, paint);

        return result;
    }

    public static VideoView getVideoViewValues(Context context,VideoView video, int obj){
        MediaController controller = new MediaController(context);
        controller.setAnchorView(video);
        video.setMediaController(controller);
        video.setKeepScreenOn(false);
        video.setVideoPath("android.resource://"+context.getPackageName()+"/"+obj);
        video.canSeekForward();
        video.canSeekBackward();
        video.requestFocus();
        return video;
    }

    public static void showDatePickerDialog(Activity context, final Bundle data){
        Calendar calendar = Calendar.getInstance();
        View view;
        final int year, month, day;
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
                String value = String.valueOf(year+month+day);
                data.putString("tanggal",value);
            }
        },year,month,day);
        datePickerDialog.setThemeDark(false);
        datePickerDialog.showYearPickerFirst(false);
        datePickerDialog.setAccentColor(Color.parseColor("#009688"));
        datePickerDialog.setTitle("Calendar");
        datePickerDialog.show(context.getFragmentManager(),"Calendar Show");
    }

    public static String setParseDateTime(String dateString, String originalFormat, String outputFormat){
        SimpleDateFormat formatter = new SimpleDateFormat(originalFormat, Locale.US);
        Date date = null;
        try{
            date = formatter.parse(dateString);
            SimpleDateFormat dateFormat = new SimpleDateFormat(outputFormat, new Locale("US"));
            return  dateFormat.format(date);
        }catch (ParseException e){
            e.printStackTrace();
            return null;
        }
    }
    public static String getRelativeTimeSpan(String dateString, String original){
        SimpleDateFormat formatter = new SimpleDateFormat(original, Locale.US);
        Date date = null;
        try {
           date = formatter.parse(dateString);
            return DateUtils.getRelativeTimeSpanString(date.getTime()).toString();
        }catch (ParseException e){
            e.printStackTrace();
            return null;
        }
    }

    public static ProgressDialog showLoadDialog(Context context, String load){
            ProgressDialog dialog = new ProgressDialog(context);
        dialog.setTitle(null);
        dialog.setMessage(load);
        dialog.setCancelable(true);
        return dialog;
    }

    public static ProgressBar showProgrssBar(Activity context,int id){
        ProgressBar progressBar = (ProgressBar)context.findViewById(id);
        progressBar = new ProgressBar(context,null,android.R.attr.progressBarStyleHorizontal);
        return  progressBar;
    }

    public static int checkConnectivity(Context context){
        int internet = 0;
        boolean enabled = true;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info = connectivityManager.getActiveNetworkInfo();
        if (info == null || !info.isConnected() || !info.isAvailable()){
            internet = 0;
            Toast.makeText(context.getApplicationContext(),"No Network Connection...", Toast.LENGTH_LONG).show();
            enabled = false;
        }else{
            internet = 1;
        }
        return internet;
    }

    public static Bitmap getScreenShot(View view){
        View viewScreen = view.getRootView();
        viewScreen.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(viewScreen.getDrawingCache());
        viewScreen.setDrawingCacheEnabled(false);
        return bitmap;
    }
    public static File getMainDirectoryName(Context context){
        File mainDir = new File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES),"Demo");

        if (!mainDir.exists()){
            if (mainDir.mkdir()){
                Log.e("Create Directory", "Main Directory Created : " + mainDir);
            }
        }
        return mainDir;
    }

    public static File store(Bitmap bm, String fileName, File savingPath){
        File dir = new File(savingPath.getAbsolutePath());
        if (!dir.exists())
            dir.mkdir();
        File file = new File(savingPath.getAbsolutePath(),fileName);
        try {
            FileOutputStream fOut = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();
        }catch (Exception e){
            e.printStackTrace();
        }
        return file;
    }

    public static Toast getMessageToast(Context context,String msg){
        Toast toast = Toast.makeText(context,msg,Toast.LENGTH_SHORT);
        toast.show();
        return toast;

    }

    public static AlertDialog.Builder CustomeDialogProgress(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.load_custome_dialog, null, false);
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        return builder;
    }

    public static DatePicker getDatePicker(Context context){
        View view = LayoutInflater.from(context).inflate(R.layout.custome_date_dialog,null, false);
        final AlertDialog dialog;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(view);
        dialog = builder.create();
        final DatePicker datePicker = (DatePicker)view.findViewById(R.id.date_custome);
        LinearLayout viewDone = (LinearLayout) view.findViewById(R.id.viewDone);
        viewDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Bundle data = new Bundle();
                data.putString("dates",datePicker.getTag().toString());
            }
        });
        builder.show();
        return datePicker;
    }

    public static TimePicker getTimePicker(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        View view = LayoutInflater.from(context).inflate(R.layout.custome_time, null, false);
        TimePicker timePicker = (TimePicker)view.findViewById(R.id.time_custome);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        return timePicker;
    }
    public static String RPFormat(String value){
        value=value.replace(",","");
        char lastDigit=value.charAt(value.length()-1);
        String result = "";
        int len = value.length()-1;
        int nDigits = 0;

        for (int i = len - 1; i >= 0; i--)
        {
            result = value.charAt(i) + result;
            nDigits++;
            if (((nDigits % 2) == 0) && (i > 0))
            {
                result = "," + result;
            }
        }
        return (result+lastDigit);
    }

    public static AlertDialog customeAlertInputDialog(Context context, View view){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(null);
        builder.setView(view);
        AlertDialog dialog = builder.create();
        return dialog;
    }

    public static void saveArrayData(String url){
        String[] datas = {"A","B",""};
        ANRequest.PostRequestBuilder builder = new ANRequest.PatchRequestBuilder(url);
        for (int i=0;i<datas.length;i++){
            builder.addBodyParameter("params_a",datas[i]);
            builder.addBodyParameter("params_b",datas[i]);
            builder.addBodyParameter("params_b",datas[i]);
            builder.build().getAsJSONObject(new JSONObjectRequestListener() {
                @Override
                public void onResponse(JSONObject response) {

                }

                @Override
                public void onError(ANError anError) {

                }
            });
        }

    }
}
