package com.ck.it.androidapps.gpstracker;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.widget.Toast;



public class GpsTracker extends Service implements LocationListener {
    private Context context;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;
    boolean canGetLocation = false;
    Location location;
    double latitude;
    double longitude;

    private static final long MiN_DISTANCE_CHAGE_FOR_UPDATE = 10;
    private static long MIN_TIME_BW_UPDATE = 1000 * 60 * 1;
    protected LocationManager locationManager;

    public GpsTracker(Context mContext) {
        this.context = mContext;
        getLocation();
    }

    @SuppressWarnings("MissingPermission")
    public Location getLocation() {
        try {
            locationManager = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);
            isGPSEnabled = locationManager
                    .isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled) {
                Toast.makeText(context.getApplicationContext(), "GPS Configuration ON....", Toast.LENGTH_LONG).show();
            } else if (!isNetworkEnabled) {
                Toast.makeText(context.getApplicationContext(), "NOt Connected network", Toast.LENGTH_LONG).show();
            } else {
                this.canGetLocation = true;
                if (isNetworkEnabled) {
                    locationManager
                            .requestLocationUpdates(
                                    LocationManager.NETWORK_PROVIDER,
                                    MIN_TIME_BW_UPDATE,
                                    MiN_DISTANCE_CHAGE_FOR_UPDATE, this);
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }
                if (isGPSEnabled) {
                    if (location == null) {
                        Criteria criteria = new Criteria();
                        criteria.setAccuracy(Criteria.ACCURACY_COARSE);
                        String provider = locationManager.getBestProvider(criteria, true);
                        location = locationManager.getLastKnownLocation(provider);
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATE,
                                MiN_DISTANCE_CHAGE_FOR_UPDATE, this);
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);
                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.fillInStackTrace();
        }
        return location;
    }

    @Override
    public void onLocationChanged(final Location location) {


    }


    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }
        return latitude;
    }

    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }
        return longitude;
    }

    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    public void showSettingAlerts() {
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle("GPS Setting");
        alert.setMessage("GPS Is Not Enabled...");
        alert.setPositiveButton("Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_LOCALE_SETTINGS);
                context.startActivity(intent);
            }
        });
        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.cancel();
            }
        });
        alert.show();
    }

    public String formatLatLongGPSTRACKING(double latitude, double longitude) {
        try {
            int latSecond = (int) Math.round(latitude * 3600);
            int latDegrees = latSecond / 3600;
            latSecond = Math.abs(latSecond % 3600);
            int latMinutes = latSecond / 60;
            latSecond %= 60;

            int longSecond = (int) Math.round(longitude * 3600);
            int longDegrees = longSecond / 3600;
            latSecond = Math.abs(longSecond % 3600);
            int longMinutes = longSecond / 60;
            longSecond %= 60;

            String latDegree = latDegrees >= 0 ? "N" : "S";
            String longDegree = longDegrees >= 0 ? "E" : "W";


            return Math.abs(latDegrees)
                    + "°"
                    + latMinutes
                    + "'"
                    + latSecond
                    + "\""
                    + latDegree
                    + " "
                    + Math.abs(longDegrees)
                    + "°"
                    + longMinutes
                    + "'"
                    + longSecond
                    + "\""
                    + longDegree;
        } catch (Exception e) {
            return "" + String.format("%8.5f", latitude)
                    + " "
                    + String.format("%8.5f", longitude);
        }
    }
}