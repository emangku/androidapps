package com.ck.it.androidapps.fragment.equipment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eko on 22/03/18.
 */

@SuppressWarnings("deprecation")
public class EquipmentCommentFragments extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View view;
    private SwipeRefreshLayout viewRefreshEquipmentComment;
    private RecyclerView viewRclEquipmentComment;
    ProgressDialog dialog;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.equipment_comment_fragment, container, false);
        init();
        return view;
    }

    void init(){
        viewRefreshEquipmentComment = (SwipeRefreshLayout) view.findViewById(R.id.viewRefreshEquipmentComment);
        viewRefreshEquipmentComment.setOnRefreshListener(this);
        viewRclEquipmentComment = (RecyclerView) view.findViewById(R.id.viewRclEquipmentComment);
    }
    void showDataEquipmentComment(String url){
        dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API+url)
                .addBodyParameter(params.SITE_CODE,utils.getDataSession(view.getContext(),""))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });
    }
    @Override
    public void onRefresh() {

    }
}
