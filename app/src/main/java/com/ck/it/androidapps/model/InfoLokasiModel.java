package com.ck.it.androidapps.model;

/**
 * Created by Creef on 12/17/2017.
 */

public class InfoLokasiModel {
    private String SITE_CODE;
    private String USER_ID;
    private String USERNAME;
    private String S_EMAIl;
    private String IMAGE;
    private String REGIONAL;
    private String PASSWORD;
    private String USER_STATUS;
    private String NO_STATUS;
    private String remember_token;
    private String LATITUDE;
    private String LONGITUDE;
    private String ALAMAT;
    private String DESA;
    private String KECAMATAN;
    private String KABUPATEN;
    private String PROVINSI;
    private String KODE_POS;
    private String created_at;
    private String updated_at;
    private String NAMA_KONTAK;
    private String NOMOR_TELEPON;
    private String HUBUNGAN_DENGAN_PEMILIK;
    private String NAMA_PEMILIK;
    private String BADAN_HUKUM;
    private String NOTLP_OR_FAX_OR_NOHP;
    private String EMAIL;
    private String RENCANA_KERJASAMA;
    private String HARGA_PENAWARAN_SEWA;
    private String PERIODE_SEWA;
    private String GRACE_PERIOD;
    private String STATUS_HARGA;
    private String SERVICE_CHARGE;
    private String BIAYA_PAJAK;
    private String BIAYA_NOTARIS;
    private String SERTIFIKASI;
    private String ATAS_NAMA;
    private String LUAS_TANAH;
    private String IJIN_MENDIRIKAN_BANGUNAN;


    public InfoLokasiModel() {
    }

    public InfoLokasiModel(
            String SITE_CODE, String USER_ID,
            String USERNAME, String s_EMAIl,
            String REGIONAL, String PASSWORD, String IMAGE,
            String USER_STATUS, String NO_STATUS,
            String remember_token, String LATITUDE,
            String LONGITUDE, String ALAMAT,
            String DESA, String KECAMATAN,
            String KABUPATEN, String PROVINSI,
            String KODE_POS, String created_at,
            String updated_at, String NAMA_KONTAK,
            String NOMOR_TELEPON, String HUBUNGAN_DENGAN_PEMILIK,
            String NAMA_PEMILIK, String BADAN_HUKUM,
            String NOTLP_OR_FAX_OR_NOHP, String EMAIL,
            String RENCANA_KERJASAMA, String HARGA_PENAWARAN_SEWA,
            String PERIODE_SEWA, String GRACE_PERIOD,
            String STATUS_HARGA, String SERVICE_CHARGE,
            String BIAYA_PAJAK, String BIAYA_NOTARIS,
            String SERTIFIKASI, String ATAS_NAMA,
            String LUAS_TANAH, String IJIN_MENDIRIKAN_BANGUNAN) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.USERNAME = USERNAME;
        this.S_EMAIl = s_EMAIl;
        this.REGIONAL = REGIONAL;
        this.PASSWORD = PASSWORD;
        this.USER_STATUS = USER_STATUS;
        this.NO_STATUS = NO_STATUS;
        this.remember_token = remember_token;
        this.LATITUDE = LATITUDE;
        this.LONGITUDE = LONGITUDE;
        this.ALAMAT = ALAMAT;
        this.DESA = DESA;
        this.KECAMATAN = KECAMATAN;
        this.KABUPATEN = KABUPATEN;
        this.PROVINSI = PROVINSI;
        this.KODE_POS = KODE_POS;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.NAMA_KONTAK = NAMA_KONTAK;
        this.NOMOR_TELEPON = NOMOR_TELEPON;
        this.HUBUNGAN_DENGAN_PEMILIK = HUBUNGAN_DENGAN_PEMILIK;
        this.NAMA_PEMILIK = NAMA_PEMILIK;
        this.BADAN_HUKUM = BADAN_HUKUM;
        this.NOTLP_OR_FAX_OR_NOHP = NOTLP_OR_FAX_OR_NOHP;
        this.EMAIL = EMAIL;
        this.RENCANA_KERJASAMA = RENCANA_KERJASAMA;
        this.HARGA_PENAWARAN_SEWA = HARGA_PENAWARAN_SEWA;
        this.PERIODE_SEWA = PERIODE_SEWA;
        this.GRACE_PERIOD = GRACE_PERIOD;
        this.STATUS_HARGA = STATUS_HARGA;
        this.SERVICE_CHARGE = SERVICE_CHARGE;
        this.BIAYA_PAJAK = BIAYA_PAJAK;
        this.BIAYA_NOTARIS = BIAYA_NOTARIS;
        this.SERTIFIKASI = SERTIFIKASI;
        this.ATAS_NAMA = ATAS_NAMA;
        this.LUAS_TANAH = LUAS_TANAH;
        this.IJIN_MENDIRIKAN_BANGUNAN = IJIN_MENDIRIKAN_BANGUNAN;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getUSERNAME() {
        return USERNAME;
    }

    public void setUSERNAME(String USERNAME) {
        this.USERNAME = USERNAME;
    }

    public String getS_EMAIl() {
        return S_EMAIl;
    }

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public void setS_EMAIl(String s_EMAIl) {
        S_EMAIl = s_EMAIl;
    }

    public String getREGIONAL() {
        return REGIONAL;
    }

    public void setREGIONAL(String REGIONAL) {
        this.REGIONAL = REGIONAL;
    }

    public String getPASSWORD() {
        return PASSWORD;
    }

    public void setPASSWORD(String PASSWORD) {
        this.PASSWORD = PASSWORD;
    }

    public String getUSER_STATUS() {
        return USER_STATUS;
    }

    public void setUSER_STATUS(String USER_STATUS) {
        this.USER_STATUS = USER_STATUS;
    }

    public String getNO_STATUS() {
        return NO_STATUS;
    }

    public void setNO_STATUS(String NO_STATUS) {
        this.NO_STATUS = NO_STATUS;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getLATITUDE() {
        return LATITUDE;
    }

    public void setLATITUDE(String LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public String getLONGITUDE() {
        return LONGITUDE;
    }

    public void setLONGITUDE(String LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
    }

    public String getALAMAT() {
        return ALAMAT;
    }

    public void setALAMAT(String ALAMAT) {
        this.ALAMAT = ALAMAT;
    }

    public String getDESA() {
        return DESA;
    }

    public void setDESA(String DESA) {
        this.DESA = DESA;
    }

    public String getKECAMATAN() {
        return KECAMATAN;
    }

    public void setKECAMATAN(String KECAMATAN) {
        this.KECAMATAN = KECAMATAN;
    }

    public String getKABUPATEN() {
        return KABUPATEN;
    }

    public void setKABUPATEN(String KABUPATEN) {
        this.KABUPATEN = KABUPATEN;
    }

    public String getPROVINSI() {
        return PROVINSI;
    }

    public void setPROVINSI(String PROVINSI) {
        this.PROVINSI = PROVINSI;
    }

    public String getKODE_POS() {
        return KODE_POS;
    }

    public void setKODE_POS(String KODE_POS) {
        this.KODE_POS = KODE_POS;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getNAMA_KONTAK() {
        return NAMA_KONTAK;
    }

    public void setNAMA_KONTAK(String NAMA_KONTAK) {
        this.NAMA_KONTAK = NAMA_KONTAK;
    }

    public String getNOMOR_TELEPON() {
        return NOMOR_TELEPON;
    }

    public void setNOMOR_TELEPON(String NOMOR_TELEPON) {
        this.NOMOR_TELEPON = NOMOR_TELEPON;
    }

    public String getHUBUNGAN_DENGAN_PEMILIK() {
        return HUBUNGAN_DENGAN_PEMILIK;
    }

    public void setHUBUNGAN_DENGAN_PEMILIK(String HUBUNGAN_DENGAN_PEMILIK) {
        this.HUBUNGAN_DENGAN_PEMILIK = HUBUNGAN_DENGAN_PEMILIK;
    }

    public String getNAMA_PEMILIK() {
        return NAMA_PEMILIK;
    }

    public void setNAMA_PEMILIK(String NAMA_PEMILIK) {
        this.NAMA_PEMILIK = NAMA_PEMILIK;
    }

    public String getBADAN_HUKUM() {
        return BADAN_HUKUM;
    }

    public void setBADAN_HUKUM(String BADAN_HUKUM) {
        this.BADAN_HUKUM = BADAN_HUKUM;
    }

    public String getNOTLP_OR_FAX_OR_NOHP() {
        return NOTLP_OR_FAX_OR_NOHP;
    }

    public void setNOTLP_OR_FAX_OR_NOHP(String NOTLP_OR_FAX_OR_NOHP) {
        this.NOTLP_OR_FAX_OR_NOHP = NOTLP_OR_FAX_OR_NOHP;
    }

    public String getEMAIL() {
        return EMAIL;
    }

    public void setEMAIL(String EMAIL) {
        this.EMAIL = EMAIL;
    }

    public String getRENCANA_KERJASAMA() {
        return RENCANA_KERJASAMA;
    }

    public void setRENCANA_KERJASAMA(String RENCANA_KERJASAMA) {
        this.RENCANA_KERJASAMA = RENCANA_KERJASAMA;
    }

    public String getHARGA_PENAWARAN_SEWA() {
        return HARGA_PENAWARAN_SEWA;
    }

    public void setHARGA_PENAWARAN_SEWA(String HARGA_PENAWARAN_SEWA) {
        this.HARGA_PENAWARAN_SEWA = HARGA_PENAWARAN_SEWA;
    }

    public String getPERIODE_SEWA() {
        return PERIODE_SEWA;
    }

    public void setPERIODE_SEWA(String PERIODE_SEWA) {
        this.PERIODE_SEWA = PERIODE_SEWA;
    }

    public String getGRACE_PERIOD() {
        return GRACE_PERIOD;
    }

    public void setGRACE_PERIOD(String GRACE_PERIOD) {
        this.GRACE_PERIOD = GRACE_PERIOD;
    }

    public String getSTATUS_HARGA() {
        return STATUS_HARGA;
    }

    public void setSTATUS_HARGA(String STATUS_HARGA) {
        this.STATUS_HARGA = STATUS_HARGA;
    }

    public String getSERVICE_CHARGE() {
        return SERVICE_CHARGE;
    }

    public void setSERVICE_CHARGE(String SERVICE_CHARGE) {
        this.SERVICE_CHARGE = SERVICE_CHARGE;
    }

    public String getBIAYA_PAJAK() {
        return BIAYA_PAJAK;
    }

    public void setBIAYA_PAJAK(String BIAYA_PAJAK) {
        this.BIAYA_PAJAK = BIAYA_PAJAK;
    }

    public String getBIAYA_NOTARIS() {
        return BIAYA_NOTARIS;
    }

    public void setBIAYA_NOTARIS(String BIAYA_NOTARIS) {
        this.BIAYA_NOTARIS = BIAYA_NOTARIS;
    }

    public String getSERTIFIKASI() {
        return SERTIFIKASI;
    }

    public void setSERTIFIKASI(String SERTIFIKASI) {
        this.SERTIFIKASI = SERTIFIKASI;
    }

    public String getATAS_NAMA() {
        return ATAS_NAMA;
    }

    public void setATAS_NAMA(String ATAS_NAMA) {
        this.ATAS_NAMA = ATAS_NAMA;
    }

    public String getLUAS_TANAH() {
        return LUAS_TANAH;
    }

    public void setLUAS_TANAH(String LUAS_TANAH) {
        this.LUAS_TANAH = LUAS_TANAH;
    }

    public String getIJIN_MENDIRIKAN_BANGUNAN() {
        return IJIN_MENDIRIKAN_BANGUNAN;
    }

    public void setIJIN_MENDIRIKAN_BANGUNAN(String IJIN_MENDIRIKAN_BANGUNAN) {
        this.IJIN_MENDIRIKAN_BANGUNAN = IJIN_MENDIRIKAN_BANGUNAN;
    }
}
