package com.ck.it.androidapps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.main.FullScreenGaleryActivity;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.model.areaanlysis.AreaAnalysisIMGModel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class AreaAnalysisIMGAdapter extends RecyclerView.Adapter<AreaAnalysisIMGAdapter.ViewHolder> {
    private View view;
    private Context mContext;
    private List<AreaAnalysisIMGModel> mlist = new ArrayList<>();

    public AreaAnalysisIMGAdapter(Context mContext, List<AreaAnalysisIMGModel> list) {
        this.mContext = mContext;
        this.mlist = list;
    }

    @Override
    public AreaAnalysisIMGAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(mContext).inflate(R.layout.list_area_img, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AreaAnalysisIMGAdapter.ViewHolder holder, final int position) {
        Picasso.with(view.getContext())
                .load(api.SERVER_API_IMG + mlist.get(position).getIMAGE())
                .centerInside()
                .resize(800, 1000)
                .placeholder(R.mipmap.imgcapture)
                .into(holder.viewImage);
        holder.viewKondisiIMG.setText(mlist.get(position).getVALUE_RANGE());
        holder.viewImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent(mContext, FullScreenGaleryActivity.class);
                intn.putExtra("url", mlist.get(position).getIMAGE());
                mContext.startActivity(intn);
            }
        });
        System.out.println(api.SERVER_API_IMG + mlist.get(position).getIMAGE());
    }

    @Override
    public int getItemCount() {
        if (mlist != null) {
            return mlist.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView viewImage;
        TextView viewKondisiIMG;
        public ViewHolder(View itemView) {
            super(itemView);
            viewImage = (ImageView) itemView.findViewById(R.id.viewImages);
            viewKondisiIMG = (TextView) itemView.findViewById(R.id.viewKondisiIMG);
        }
    }
}
