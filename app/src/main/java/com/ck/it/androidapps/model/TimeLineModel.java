package com.ck.it.androidapps.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.ck.it.androidapps.helpers.ApprovalStatus;

/**
 * Created by Creef on 12/19/2017.
 */

public class TimeLineModel implements Parcelable {
    private String TANGGAL;
    private String SITE_CODE;
    private String LOCATION;
    private ApprovalStatus approvalStatus;

    public TimeLineModel() {
    }

    public TimeLineModel(String TANGGAL, String SITE_CODE, String LOCATION, ApprovalStatus approvalStatus) {
        this.TANGGAL = TANGGAL;
        this.SITE_CODE = SITE_CODE;
        this.LOCATION = LOCATION;
        this.approvalStatus = approvalStatus;
    }

    public String getTANGGAL() {
        return TANGGAL;
    }

    public void setTANGGAL(String TANGGAL) {
        this.TANGGAL = TANGGAL;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getLOCATION() {
        return LOCATION;
    }

    public void setLOCATION(String LOCATION) {
        this.LOCATION = LOCATION;
    }

    public ApprovalStatus getApprovalStatus() {
        return approvalStatus;
    }

    public void setApprovalStatus(ApprovalStatus approvalStatus) {
        this.approvalStatus = approvalStatus;
    }

    public static Creator<TimeLineModel> getCREATOR() {
        return CREATOR;
    }

    protected TimeLineModel(Parcel in) {
        TANGGAL = in.readString();
        SITE_CODE = in.readString();
        LOCATION = in.readString();
        int tmpStatus = in.readInt();
        this.approvalStatus = tmpStatus == -1 ? null : ApprovalStatus.values()[tmpStatus];
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(TANGGAL);
        dest.writeString(SITE_CODE);
        dest.writeString(LOCATION);
        dest.writeInt(this.approvalStatus == null ? -1 : this.approvalStatus.ordinal());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<TimeLineModel> CREATOR = new Creator<TimeLineModel>() {
        @Override
        public TimeLineModel createFromParcel(Parcel in) {
            return new TimeLineModel(in);
        }

        @Override
        public TimeLineModel[] newArray(int size) {
            return new TimeLineModel[size];
        }
    };
}
