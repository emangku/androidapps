package com.ck.it.androidapps.fragment.docuploads;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;

/**
 * Created by Creef on 2/23/2018.
 */

@SuppressWarnings("deprecation")
public class DocUploadsFragments extends Fragment {
    private View view;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.uploads_doc_fragement, container, false);
        return view;
    }

    void init(){

    }

    void uploadDOC(String url, File file){
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.uploading_msg);
        dialog.show();

        AndroidNetworking.upload(api.SERVER_API+url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addMultipartParameter(params.USER_ID,utils.getDataSession(view.getContext(),"sessionUserID"))
                .addMultipartFile(params.DOCUMENT,file)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(),"Oppss Error : ", Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
    }


}
