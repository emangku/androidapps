package com.ck.it.androidapps.fragment.siteanalysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.util.utils;

/**
 * Created by Creef on 12/6/2017.
 */

public class KondisiBangunanFragment extends Fragment implements View.OnClickListener {
    private View view;
    String[] bahanatap = {"-Bahan Atap-","Genteng","Seng","Asbes"};
    String[] bahandinding = {"-Bahan Dinding-","Bata","Kayu","Seng"};
    String[] bahanlantai = {"-Bahan Lantai-","Granit","Keramik","Semen"};
    String[] halaman = {"-Halaman-","Aspal","Paving","Semen"};
    Button viewNextKondisiBangunan;
    Spinner viewHalaman, viewBahanAtap, viewBahanDinding,viewBahanLantai;
    ArrayAdapter<String> hAdapter, baAdapter, bdAdapter,blAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.kondisi_bangunan,container,false);
        initView();
        return view;
    }
    void initView(){
        viewNextKondisiBangunan = (Button)view.findViewById(R.id.viewNextKondisiBangunan);
        viewNextKondisiBangunan.setOnClickListener(this);

        viewHalaman = (Spinner)view.findViewById(R.id.viewHalaman);
        hAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,halaman);
        viewHalaman.setAdapter(hAdapter);


        viewBahanAtap = (Spinner)view.findViewById(R.id.viewBahanAtap);
        baAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,bahanatap);
        viewBahanAtap.setAdapter(baAdapter);


        viewBahanDinding = (Spinner)view.findViewById(R.id.viewBahanDinding);
        bdAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,bahandinding);
        viewBahanDinding.setAdapter(bdAdapter);


        viewBahanLantai = (Spinner)view.findViewById(R.id.viewBahanLantai);
        blAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,bahanlantai);
        viewBahanLantai.setAdapter(bdAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextKondisiBangunan:
                Fragment fragment = new FasilitasBangunanFragment();
                getActivity().setTitle("Fasilitas Bangunan");
                utils.showFragmentTwo(view.getContext(),fragment);
                break;
        }
    }
}
