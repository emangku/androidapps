package com.ck.it.androidapps.fragment.FS;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.fragment.FS.fs_history.FSHistoryActivity;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 1/30/2018.
 */

@SuppressWarnings("deprecation")
public class FSStoreCostFragments extends Fragment implements View.OnClickListener {
    private View view;
    EditText viewNoOfStaff,viewSL,viewCSR,viewUtilities,viewRepairAndMaintenace,viewStoreUserSC,viewOtherSC;
    Button viewBtnStoreCost;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fs_storecode_fragment, container, false);
        init();
        return view;
    }

    void init(){
        getActivity().setTitle("FS Store Cost");
        viewNoOfStaff = (EditText) view.findViewById(R.id.viewNoOfStaff);
        viewSL = (EditText) view.findViewById(R.id.viewSL);
        viewCSR = (EditText) view.findViewById(R.id.viewCSR);
        viewUtilities = (EditText) view.findViewById(R.id.viewUtilities);
        viewRepairAndMaintenace = (EditText) view.findViewById(R.id.viewRepairAndMaintenace);
        viewStoreUserSC = (EditText) view.findViewById(R.id.viewStoreUserSC);
        viewOtherSC = (EditText) view.findViewById(R.id.viewOtherSC);
        viewBtnStoreCost = (Button) view.findViewById(R.id.viewBtnStoreCost);
        viewBtnStoreCost.setOnClickListener(this);
    }
    void saveStoreCost(String url){
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(),"Tunggu...");
        dialog.show();
        AndroidNetworking.post(api.SERVER_API+url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(),"sessionValKeySiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(),"sessionUserID"))
                .addBodyParameter(params.NO_OF_STAFF, viewNoOfStaff.getText().toString())
                .addBodyParameter(params.SL, viewSL.getText().toString())
                .addBodyParameter(params.CSR,viewCSR.getText().toString())
                .addBodyParameter(params.UTILITIES, viewUtilities.getText().toString())
                .addBodyParameter(params.REPAIR_MAINTENANCE, viewRepairAndMaintenace.getText().toString())
                .addBodyParameter(params.STORE_USE,viewStoreUserSC.getText().toString())
                .addBodyParameter(params.OTHERS, viewOtherSC.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
//                                ((FragmentActivity)view.getContext())
//                                        .getSupportFragmentManager()
//                                        .beginTransaction()
//                                        .addToBackStack(null)
//                                        .replace(R.id.viewFS,new FSHistoryActivity())
//                                        .commit();
                                startActivity(new Intent(view.getContext(), FSHistoryActivity.class));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(),"Opps Error "+anError.getMessage(), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewBtnStoreCost:
                if (TextUtils.isEmpty(viewNoOfStaff.getText().toString())){
                    viewNoOfStaff.setError(params.MValues.field_error_msg);
                    viewNoOfStaff.requestFocus();
                }else if (TextUtils.isEmpty(viewSL.getText().toString())){
                    viewSL.setError(params.MValues.field_error_msg);
                    viewSL.requestFocus();
                }else if (TextUtils.isEmpty(viewCSR.getText().toString())){
                    viewCSR.setError(params.MValues.field_error_msg);
                    viewCSR.requestFocus();
                }else if (TextUtils.isEmpty(viewUtilities.getText().toString())){
                    viewUtilities.setError(params.MValues.field_error_msg);
                    viewUtilities.requestFocus();
                }else if (TextUtils.isEmpty(viewRepairAndMaintenace.getText().toString())){
                    viewRepairAndMaintenace.setError(params.MValues.field_error_msg);
                    viewRepairAndMaintenace.requestFocus();
                }else if (TextUtils.isEmpty(viewStoreUserSC.getText().toString())){
                    viewStoreUserSC.setError(params.MValues.field_error_msg);
                    viewStoreUserSC.requestFocus();
                }else if (TextUtils.isEmpty(viewOtherSC.getText().toString())){
                    viewOtherSC.setError(params.MValues.field_error_msg);
                    viewOtherSC.requestFocus();
                }else {
                    saveStoreCost(params.URL_METHOD.SAVE_STORE_COST);
                }
                break;
        }
    }
}
