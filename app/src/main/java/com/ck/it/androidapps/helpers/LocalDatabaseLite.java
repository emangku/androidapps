package com.ck.it.androidapps.helpers;

/**
 * Created by Creef on 12/18/2017.
 */

public interface LocalDatabaseLite {
    String TABLE_INFO_LOKASI                    = "info_lokasi";
    String TABLE_INFO_KONTAK                    = "info_kontak";
    String ACTIVITY_GENERATOR                   = "activity_generator";
    String AKSES                                = "akses";
    String DEMOGRAFI                            = "demografi";
    String FASILITAS_BANGUNAN                   = "fasilitas_bangunan";
    String INFO_KEPEMILIKAN                     = "info_kepemilikan";
    String INFO_LEGALITAS_BANGUNAN              = "info_legalitas_bangunan";
    String INFO_SEWA                            = "info_sewa";
    String KOMPETITOR                           = "kompetitor";
    String KONDISI_BANGUNAN                     = "kondisi_bangunan";
    String KUALITAS_LINGKUNAGAN_SEKITAR         = "kualitas_lingkungan_sekitar";
    String MAPPING_AREA                         = "mapping_area";
    String PARKIR                               = "parkir";
    String PENYANDING                           = "penyanding";
    String SITUASI_BANGUNAN                     = "situasi_bangunan";
    String TRAFFIC_FLOW                         = "traffic_flow";
    String USER_ACCESS                          = "user_access";
    String VISIBILITY                           = "visibility";

    /// Tipe
    String TEXT_TYPE                            = "TEXT";
    String TYPE_LATITUDE                        = "DOUBLE";
    String TYPE_LONGITUDE                       = "DOUBLE";
    String _COMA                                = ",";


    String SQL_CREATE_TABLE_INFO_LOKASI = "CREATE TABLE "
                                                + TABLE_INFO_LOKASI
                                                + " ("+ FieldTable.FieldString._ID + " INTEGER PRIMARY KEY,"
                                                + FieldTable.FieldString.SITE_CODE + TEXT_TYPE + _COMA;

}
