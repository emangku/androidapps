package com.ck.it.androidapps.fragment.siteanalysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.util.utils;

/**
 * Created by Creef on 12/6/2017.
 */

public class PenyandingFragment extends Fragment implements View.OnClickListener{
    private View view;
    String[] kiri = {"-Sebelah Kiri-","Rumah","Toko","Kantor","Bangunan","Commercial","Sekolah","Tempat Ibadah","Lahan Kosong"};
    String[] kanan = {"-Sebelah Kanan-","Rumah","Toko","Kantor","Bangunan","Commercial","Sekolah","Tempat Ibadah","Lahan Kosong"};
    ArrayAdapter<String> kiriAdapter, kananAdapter;
    Spinner viewPenyandingKanan,viewPenyandingKiri;
    Button viewNextPenyanding;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.penyanding_fragment,container,false);
        initView();
        return view;
    }
    void initView(){
        viewNextPenyanding = (Button) view.findViewById(R.id.viewNextPenyanding);
        viewNextPenyanding.setOnClickListener(this);

        viewPenyandingKanan = (Spinner) view.findViewById(R.id.viewPenyandingKanan);
        kananAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,kanan);
        viewPenyandingKanan.setAdapter(kananAdapter);


        viewPenyandingKiri = (Spinner) view.findViewById(R.id.viewPenyandingKiri);
        kiriAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,kiri);
        viewPenyandingKiri.setAdapter(kiriAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextPenyanding:
                Fragment fragment = new VisibilityFragment();
                getActivity().setTitle("Visibility (50-100 m)");
                utils.showFragmentTwo(view.getContext(),fragment);
                break;
        }
    }
}
