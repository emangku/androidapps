package com.ck.it.androidapps.activity.main;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.squareup.picasso.Picasso;

public class FullscreenMappingAreaActivity extends AppCompatActivity {
    String urlgambar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_fullscreen_mapping_area);
        getSupportActionBar().hide();
        ImageView fullGambar = (ImageView) findViewById(R.id.fullMappingArea);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            urlgambar = extras.getString("url");
            Picasso.with(this)
                    .load(api.SERVER_API_IMG + urlgambar)
                    .into(fullGambar);
        }

        String hasil = "";
        //        String[] array = {"CUTI A","CUTI B","CUTI C"};
        String[] array = getResources().getStringArray(R.array.jenis_cuti);

        String vals = array[0];
        if (vals == "CUTI A") {
            hasil = "1";
        } else if (vals == "CUTI B") {
            hasil = "2";
        }
    }

}
