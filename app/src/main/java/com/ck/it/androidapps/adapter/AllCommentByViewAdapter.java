package com.ck.it.androidapps.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.model.ModelAllComments;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Creef on 3/26/2018.
 */

public class AllCommentByViewAdapter extends RecyclerView.Adapter<AllCommentByViewAdapter.ViewHolder> {
    private Context context;
    private View view;
    private List<ModelAllComments> list = new ArrayList<>();
    @Override
    public AllCommentByViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_coments,null,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AllCommentByViewAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        if (list != null){
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
