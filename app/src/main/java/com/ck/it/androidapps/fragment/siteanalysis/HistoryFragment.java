package com.ck.it.androidapps.fragment.siteanalysis;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.ui.RecyclerUIItemDecoration;
import com.ck.it.androidapps.adapter.HistorisAdapters;
import com.ck.it.androidapps.fragment.LocationInfoFragment;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.model.ModelHistory;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Creef on 1/7/2018.
 */

@SuppressWarnings("deprecation")
public class HistoryFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View view;
    FloatingActionButton fab;
    HistorisAdapters adapters;
    List<ModelHistory> list;
    RecyclerView viewHistoryFront;
    private static final String TAG = "MainActivity";
    private SwipeRefreshLayout viewHisRefresh;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.activity_main2, container, false);
        initView();
        return view;
    }


    void initView() {
        fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utils.showFragment(view.getContext(), new LocationInfoFragment());
//               utils.showFragment(view.getContext(), new PhotoFragment());
                getActivity().setTitle("Survey");
            }
        });
        getData_history(params.URL_METHOD.GET_HISTORY);
        viewHistoryFront = (RecyclerView) view.findViewById(R.id.viewHistoryFront);
//        notificationApprove();
        viewHisRefresh = (SwipeRefreshLayout)view.findViewById(R.id.viewHisRefresh);
        viewHisRefresh.setOnRefreshListener(this);
    }

    void notificationApprove() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = getString(R.string.default_notification_channel_id);
            String channelName = getString(R.string.default_notification_channel_name);
            NotificationManager notificationManager =
                    view.getContext().getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }
        if (getActivity().getIntent().getExtras() != null) {
            for (String key : getActivity().getIntent().getExtras().keySet()) {
                Object value = getActivity().getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }
        //https://developers.google.com/cloud-messaging/downstream?authuser=1
        //https://demo.androidhive.info/firebase/notifications/
        //https://www.androidhive.info/2012/10/android-push-notifications-using-google-cloud-messaging-gcm-php-and-mysql/
    }

    void senMessage() {
        FirebaseMessaging.getInstance().subscribeToTopic(utils.getDataSession(view.getContext(), "sessionValKeySiteCode"));
    }

    void refreshToken() {
        String token = FirebaseInstanceId.getInstance().getToken();
        String msg = getString(R.string.msg_token_fmt, token);
    }

    void getData_history(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                list = new ArrayList<ModelHistory>();
                                JSONArray array = response.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    ModelHistory historymodel = new ModelHistory();
                                    JSONObject obj = array.getJSONObject(i);
                                    historymodel.setSITE_CODE(obj.getString("SITE_CODE"));
                                    historymodel.setUSER_ID(obj.getString("USER_ID"));
                                    historymodel.setLATITUDE(obj.getString("LATITUDE"));
                                    historymodel.setLONGITUDE(obj.getString("LONGITUDE"));
                                    historymodel.setALAMAT_A(obj.getString("ALAMAT_A"));
                                    historymodel.setDESA(obj.getString("DESA"));
                                    historymodel.setKECAMATAN(obj.getString("KECAMATAN"));
                                    historymodel.setKABUPATEN(obj.getString("KABUPATEN"));
                                    historymodel.setPROVINSI(obj.getString("PROVINSI"));
                                    historymodel.setKODE_POS(obj.getString("KODE_POS"));
                                    historymodel.setCreated_at(obj.getString("created_at"));
                                    historymodel.setUpdated_at(obj.getString("updated_at"));
                                    list.add(historymodel);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        adapters = new HistorisAdapters(view.getContext(), list);
                        viewHistoryFront.setHasFixedSize(false);
                        LinearLayoutManager llm = new LinearLayoutManager(view.getContext());
                        viewHistoryFront.setLayoutManager(llm);
                        viewHistoryFront.addItemDecoration(new RecyclerUIItemDecoration(view.getContext(),LinearLayoutManager.VERTICAL,0));
                        viewHistoryFront.setAdapter(adapters);
                        dialog.dismiss();
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(), "Opps.. Error : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public void onRefresh() {
        getData_history(params.URL_METHOD.GET_HISTORY);
        viewHisRefresh.setRefreshing(false);
    }
}
