package com.ck.it.androidapps.fragment;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.icu.util.Calendar;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;


@SuppressWarnings("deprecation")
public class RentalinformationFragment extends Fragment implements View.OnClickListener {
    private View view;
    EditText viewPeriode, viewHargaSewa, viewEndDate;
    Button viewNextrental;
    Calendar calendar, calendars;
    DatePicker datePicker, datePickerEnd;
    DatePickerDialog datePickerDialog, datePickerDialogEnd;
    int year, month, day;
    int myears, mmonths, mdays;
    int nYear, nMount, nDay;
    Spinner viewGracePeriod, viewStatusHarga, viewServiceChange, viewBiayaPajak, viewBiayaNotaris;
    TextView txtSpinner, viewCalculateYear;
    String[] graceperiod = {"-Grace Period-", "Ya", "Tidak"};
    String[] statusharga = {"-Status Harga-", "Final", "Negotiable"};
    String[] servicechange = {"-Service Change-", "Ya", "Tidak"};
    String[] biayapajak = {"-Biaya Pajak-", "Pemilik", "Penyewa"};
    String[] biayanotaris = {"-Biaya Notaris-", "Pemilik", "Penyewa"};
    String GracePeriod = "";
    String StatusHarga = "";
    String ServiceChange = "";
    String BiayaPajak = "";
    String BiayaNotaris = "";
    String RencanaKerjasama = "";
    ArrayAdapter<String> gAdapter, shAdapter, scAdapter, bpAdapter, bnAdapter;
    RadioGroup viewSelectedPlant;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.rentalinformation, container, false);
        datePicker = new DatePicker(view.getContext());
        datePickerEnd = new DatePicker(view.getContext());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);

            calendars = Calendar.getInstance();
            myears = calendars.get(Calendar.YEAR);
            mmonths = calendars.get(Calendar.MONTH);
            mdays = calendars.get(Calendar.DAY_OF_MONTH);
        }
        initView();
        return view;
    }

    void initView() {
        viewNextrental = (Button) view.findViewById(R.id.viewNextrental);
        viewNextrental.setOnClickListener(this);
        viewPeriode = (EditText) view.findViewById(R.id.viewPeriode);
        viewPeriode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    datePickerDialog = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int years, int months, int dayOfMonth) {
                            try {
                                SimpleDateFormat date = new SimpleDateFormat("yyyy/mm/dd");
                                Date date1 = date.parse(years + "/" + (months + 1) + "/" + dayOfMonth);
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd");
                                String finalDate = dateFormat.format(date1);
                                viewPeriode.setText(finalDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, year, month, day);
                    datePicker.init(year, month, day, null);
                    datePickerDialog.show();
                } else {
//                    Toasty.error(view.getContext(),"Oops..", Toast.LENGTH_LONG).show();
                }
            }
        });
        viewCalculateYear = (TextView) view.findViewById(R.id.viewCalculateYear);
        viewEndDate = (EditText) view.findViewById(R.id.viewEndDate);
        viewEndDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    datePickerDialogEnd = new DatePickerDialog(view.getContext(), new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int years, int months, int dayOfMonth) {
                            try {
                                SimpleDateFormat date = new SimpleDateFormat("yyyy/mm/dd");
                                Date date1 = date.parse(years + "/" + (months + 1) + "/" + dayOfMonth);
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/mm/dd");
                                String finalDate = dateFormat.format(date1);
                                viewEndDate.setText(finalDate);
                                calculateStringDateFotmate(viewPeriode.getText().toString(),viewEndDate.getText().toString());
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        }
                    }, myears, mmonths, mdays);
                    datePickerEnd.init(myears, mmonths, mdays, null);
                    datePickerDialogEnd.show();
                }
            }
        });
        txtSpinner = (TextView) view.findViewById(R.id.txtSpinner);
        viewGracePeriod = (Spinner) view.findViewById(R.id.viewGracePeriod);
        gAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, graceperiod);
        viewGracePeriod.setAdapter(gAdapter);

        viewGracePeriod.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                GracePeriod = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        viewStatusHarga = (Spinner) view.findViewById(R.id.viewStatusHarga);
        shAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, statusharga);
        viewStatusHarga.setAdapter(shAdapter);

        viewStatusHarga.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                StatusHarga = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        viewServiceChange = (Spinner) view.findViewById(R.id.viewServiceChange);
        scAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, servicechange);
        viewServiceChange.setAdapter(scAdapter);

        viewServiceChange.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ServiceChange = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        viewBiayaPajak = (Spinner) view.findViewById(R.id.viewBiayaPajak);
        bpAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, biayapajak);
        viewBiayaPajak.setAdapter(bpAdapter);
        viewBiayaPajak.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BiayaPajak = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        viewBiayaNotaris = (Spinner) view.findViewById(R.id.viewBiayaNotaris);
        bnAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, biayanotaris);
        viewBiayaNotaris.setAdapter(bnAdapter);

        viewBiayaNotaris.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                BiayaNotaris = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        viewHargaSewa = (EditText) view.findViewById(R.id.viewHargaSewa);

        viewSelectedPlant = (RadioGroup) view.findViewById(R.id.viewSelectedPlant);
        viewSelectedPlant.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.viewRadioSewa:
                        RencanaKerjasama = "Sewa";
                        break;
                    case R.id.viewRadioRevenueSharing:
                        RencanaKerjasama = "Revenue Sharing";
                        break;
                    case R.id.viewRadioFranchise:
                        RencanaKerjasama = "Franchise";
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextrental:
                if (RencanaKerjasama.equals("")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(viewPeriode.getText().toString())) {
                    viewPeriode.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewHargaSewa.getText().toString())) {
                    viewHargaSewa.setError(params.MValues.field_error_msg);
                } else if (GracePeriod.equals("") || GracePeriod.equals("-Grace Period-")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (StatusHarga.equals("") || StatusHarga.equals("-Status Harga-")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (ServiceChange.equals("") || ServiceChange.equals("-Service Change-")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (BiayaPajak.equals("") || BiayaPajak.equals("-Biaya Pajak-")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (BiayaNotaris.equals("") || BiayaNotaris.equals("-Biaya Notaris-")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else {
                    saveInfoSewa(params.URL_METHOD.SAVE_INFO_SEWA);
                }
                break;
        }
    }

    // saveinfosewa
    void saveInfoSewa(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.RENCANA_KERJASAMA, RencanaKerjasama)
                .addBodyParameter(params.HARGA_PENAWARAN_SEWA, viewHargaSewa.getText().toString())
                .addBodyParameter(params.PERIODE_SEWA_START, viewPeriode.getText().toString())
                .addBodyParameter(params.PERIODE_SEWA_END, viewEndDate.getText().toString())
                .addBodyParameter(params.GRACE_PERIOD, GracePeriod)
                .addBodyParameter(params.STATUS_HARGA, StatusHarga)
                .addBodyParameter(params.SERVICE_CHARGE, ServiceChange)
                .addBodyParameter(params.BIAYA_PAJAK, BiayaPajak)
                .addBodyParameter(params.BIAYA_NOTARIS, BiayaNotaris)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                        @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new BuildingLegalityInformationFragment());
                                getActivity().setTitle(R.string.informasi_legalitas_bangunan);
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(), "Opss, Error..", Toast.LENGTH_LONG).show();
                    }
                });
    }

    @SuppressLint("SimpleDateFormat")
    public String calculateStringDateFotmate(String starDate, String endDate){
        SimpleDateFormat start,end;
        String val = "";
        start = new SimpleDateFormat("yyyy/MM/dd");
        end = new SimpleDateFormat("yyyy/MM/dd");
        try {
            Date dateste = start.parse(starDate);
            Date enddate = end.parse(endDate);

            long deff = enddate.getTime() - dateste.getTime() ;
            long yrr = (enddate.getYear() - dateste.getYear());
            long mount = yrr  /12 ;
            long m = enddate.getMonth() - dateste.getMonth();
            long vMount = m - mount;
            val = yrr+" " +m+" " +String.valueOf(TimeUnit.DAYS.convert(deff,TimeUnit.MILLISECONDS));
            viewCalculateYear.setText(String.valueOf(yrr));
            System.out.println(val);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return val;
    }
}
