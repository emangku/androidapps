package com.ck.it.androidapps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.main.FullscreenMappingAreaActivity;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.model.MappingModels;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Creef on 3/5/2018.
 */

public class MappingAdapter extends RecyclerView.Adapter<MappingAdapter.ViewHolder> {
    private View view;
    private Context context;
    private List<MappingModels> list = new ArrayList<>();

    public MappingAdapter(Context context, List<MappingModels> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_mapping, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Picasso.with(view.getContext())
                .load(api.SERVER_API_IMG + list.get(position).getIMAGE())
                .resize(512, 800)
                .placeholder(R.mipmap.imgcapture)
                .centerInside()
                .into(holder.img);
        holder.img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intn = new Intent(context, FullscreenMappingAreaActivity.class);
                intn.putExtra("url", list.get(position).getIMAGE());
                context.startActivity(intn);
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.viewIMGMapping);
        }
    }
}
