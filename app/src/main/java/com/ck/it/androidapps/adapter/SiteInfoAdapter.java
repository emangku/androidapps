package com.ck.it.androidapps.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.model.InfoLokasiModel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Creef on 1/19/2018.
 */

public class SiteInfoAdapter extends RecyclerView.Adapter<SiteInfoAdapter.ViewHolder> {
    View view;
    List<InfoLokasiModel> list = new ArrayList<>();

    @Override
    public SiteInfoAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_site_info, null);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SiteInfoAdapter.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(View itemView) {
            super(itemView);
        }
    }
}
