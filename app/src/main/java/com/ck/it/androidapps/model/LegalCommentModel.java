package com.ck.it.androidapps.model;

/**
 * Created by eko on 22/03/18.
 */

public class LegalCommentModel {
    private String SITE_CODE, USER_ID,DOC_NAME, COMMENT;

    public LegalCommentModel() {
    }

    public LegalCommentModel(String SITE_CODE, String USER_ID, String DOC_NAME, String COMMENT) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.DOC_NAME = DOC_NAME;
        this.COMMENT = COMMENT;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getDOC_NAME() {
        return DOC_NAME;
    }

    public void setDOC_NAME(String DOC_NAME) {
        this.DOC_NAME = DOC_NAME;
    }

    public String getCOMMENT() {
        return COMMENT;
    }

    public void setCOMMENT(String COMMENT) {
        this.COMMENT = COMMENT;
    }
}
