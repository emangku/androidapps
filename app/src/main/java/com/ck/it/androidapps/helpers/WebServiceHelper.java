package com.ck.it.androidapps.helpers;

import com.ck.it.androidapps.ssl.SSLSocket;

import java.util.concurrent.TimeUnit;

import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;


@SuppressWarnings({"deprecation", "ConstantConditions"})
public class WebServiceHelper {
    public static OkHttpClient client;

    public static void initialize(){
        if (client == null) {
            client = new OkHttpClient();
            client.newBuilder()
                    .sslSocketFactory(SSLSocket.getSSLSocketFactory())
                    .connectTimeout(200, TimeUnit.SECONDS);
        }
    }

    public static String doGET(String url, String token) throws Exception {
        Request request = new Request.Builder()
                .addHeader("Autorization", "Bearer " + token)
                .url(url)
                .build();
        Response response = client.newCall(request).execute();

        return response.body().string();
    }

    public static String doPOST(String url, RequestBody body, String token) throws Exception {
        Headers headers = new Headers.Builder()
                .add("Authorization", "Bearer " + token)
                .build();

        Request request = new Request.Builder()
                .headers(headers)
                .url(url)
                .post(body)
                .build();

        Response response = client.newCall(request).execute();

        return response.body().string();
    }
}
