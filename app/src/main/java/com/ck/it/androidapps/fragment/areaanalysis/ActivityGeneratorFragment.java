package com.ck.it.androidapps.fragment.areaanalysis;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.util.utils;
import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.FileNotFoundException;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Creef on 12/7/2017.
 */

public class ActivityGeneratorFragment extends Fragment implements View.OnClickListener {
    private View view;
    Spinner viewKelasSosial,viewJalanOperasi,viewHariBeroperasi;
    ArrayAdapter<String> ksAdapter, joAdapter,hbAdapter;
    String[] kelassosial = {
            "-Kelas Sosial-",
            "Low","Middle","High"};
    String[] jamoperasi = {
            "-Jam Operasi ( Jam )-",
            "8","12","24"};
    String[] hariberoperasi = {
            "-Hari Beroperasi-",
            " S","S","R","K","J","S","M"};
    Button viewNextActivity;
    ImageView viewCaptureActivity;
    CameraPhoto cameraPhoto;
    int CAMERA_REQUEST = 1100;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.aktivitygenertator_fragment,container,false);
        initView();
        return view;
    }

    void initView(){
        new AlertDialog.Builder(view.getContext())
                .setTitle("Note")
                .setIcon(R.drawable.ic_info)
                .setView(R.layout.info)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();

        viewNextActivity = (Button) view.findViewById(R.id.viewNextActivity);
        viewNextActivity.setOnClickListener(this);
        viewCaptureActivity = (ImageView)view.findViewById(R.id.viewCaptureActivity);
        viewCaptureActivity.setOnClickListener(this);
        cameraPhoto = new CameraPhoto(view.getContext().getApplicationContext());
        viewKelasSosial = (Spinner) view.findViewById(R.id.viewKelasSosial);
        ksAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner,R.id.txtSpinner,kelassosial);
        viewKelasSosial.setAdapter(ksAdapter);

        viewJalanOperasi = (Spinner) view.findViewById(R.id.viewJalanOperasi);
        joAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner,R.id.txtSpinner,jamoperasi);
        viewJalanOperasi.setAdapter(joAdapter);

        viewHariBeroperasi = (Spinner) view.findViewById(R.id.viewHariBeroperasi);
        hbAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner,R.id.txtSpinner,hariberoperasi);
        viewHariBeroperasi.setAdapter(hbAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextActivity:
                utils.showFragmentThree(view.getContext(), new KompetitorFragment());
                getActivity().setTitle("Kompetitor");
                break;
            case R.id.viewCaptureActivity:
                try {
                    Intent intent = cameraPhoto.takePhotoIntent();
                    startActivityForResult(intent, CAMERA_REQUEST);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            if (requestCode == CAMERA_REQUEST){
                String photoPATH = cameraPhoto.getPhotoPath();
                try {
                    Bitmap bitmap = ImageLoader.init().from(photoPATH).requestSize(512,200).getBitmap();
                    viewCaptureActivity.setImageBitmap(bitmap);
                }catch (FileNotFoundException e){
                    e.printStackTrace();
                }
            }
        }
    }
}
