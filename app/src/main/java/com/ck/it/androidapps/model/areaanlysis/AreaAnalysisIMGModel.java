package com.ck.it.androidapps.model.areaanlysis;

/**
 * Created by Creef on 1/29/2018.
 */

public class AreaAnalysisIMGModel {
    private String SITE_CODE,
            USER_ID,
            AA_ID,
            IMAGE,
            TYPE,
            VALUE_OPTION,
            VALUE_RANGE;

    public AreaAnalysisIMGModel() {
    }

    public AreaAnalysisIMGModel(String SITE_CODE, String USER_ID,
                                String AA_ID, String IMAGE,
                                String TYPE, String VALUE_OPTION, String VALUE_RANGE) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.AA_ID = AA_ID;
        this.IMAGE = IMAGE;
        this.TYPE = TYPE;
        this.VALUE_OPTION = VALUE_OPTION;
        this.VALUE_RANGE = VALUE_RANGE;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getAA_ID() {
        return AA_ID;
    }

    public void setAA_ID(String AA_ID) {
        this.AA_ID = AA_ID;
    }

    public String getIMAGE() {
        return IMAGE;
    }

    public void setIMAGE(String IMAGE) {
        this.IMAGE = IMAGE;
    }

    public String getTYPE() {
        return TYPE;
    }

    public void setTYPE(String TYPE) {
        this.TYPE = TYPE;
    }

    public String getVALUE_OPTION() {
        return VALUE_OPTION;
    }

    public void setVALUE_OPTION(String VALUE_OPTION) {
        this.VALUE_OPTION = VALUE_OPTION;
    }

    public String getVALUE_RANGE() {
        return VALUE_RANGE;
    }

    public void setVALUE_RANGE(String VALUE_RANGE) {
        this.VALUE_RANGE = VALUE_RANGE;
    }
}
