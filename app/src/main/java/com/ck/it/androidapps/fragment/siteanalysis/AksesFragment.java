package com.ck.it.androidapps.fragment.siteanalysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.util.utils;

/**
 * Created by Creef on 12/6/2017.
 */

public class AksesFragment extends Fragment implements View.OnClickListener{
    private View view;
    String[] kualitasakses = {"-Kualitas Akses-","Sangat Leluasa","Leluasa","Terhalang","Sulit"};
    String[] kemiringanlokasi = {"-Kemiringan Lokasi-","Jalan menurun","Jalan menanjak","Jalan menikung","Jalan datar","Lampu merah"};
    ArrayAdapter<String> ksAdapter, klAdapter;
    Spinner viewKemiringanLokasi,viewKualitasAkses;
    Button viewNextAkses;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.akses_fragment,container,false);
        initView();
        return view;
    }
    void initView(){
        viewNextAkses = (Button)view.findViewById(R.id.viewNextAkses);
        viewNextAkses.setOnClickListener(this);
        viewKemiringanLokasi = (Spinner) view.findViewById(R.id.viewKemiringanLokasi);
        klAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner, R.id.txtSpinner,kemiringanlokasi);
        viewKemiringanLokasi.setAdapter(klAdapter);

        viewKualitasAkses = (Spinner) view.findViewById(R.id.viewKualitasAkses);
        ksAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner, R.id.txtSpinner,kualitasakses);
        viewKualitasAkses.setAdapter(ksAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextAkses:
                Fragment fragment = new PenyandingFragment();
                getActivity().setTitle("Penyanding");
                utils.showFragmentTwo(view.getContext(),fragment);
                break;
        }
    }
}
