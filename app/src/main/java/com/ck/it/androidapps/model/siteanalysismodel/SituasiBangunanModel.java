package com.ck.it.androidapps.model.siteanalysismodel;

/**
 * Created by Creef on 12/17/2017.
 */

public class SituasiBangunanModel {
    private String SITE_CODE;
    private String USER_ID;
    private String TIPE_BANGUNAN;
    private String POSISI_BANGUNAN;
    private String TAHUN_PEMBUATAN;
    private String UKURAN_LOKASI;
    private String UKURAN_BANGUNAN;
    private String LEBAR_MUKA_BANGUNAN;
    private String PANJANG_BANGUNAN;

    public SituasiBangunanModel() {
    }

    public SituasiBangunanModel(String SITE_CODE, String USER_ID, String TIPE_BANGUNAN, String POSISI_BANGUNAN, String TAHUN_PEMBUATAN, String UKURAN_LOKASI, String UKURAN_BANGUNAN, String LEBAR_MUKA_BANGUNAN, String PANJANG_BANGUNAN) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.TIPE_BANGUNAN = TIPE_BANGUNAN;
        this.POSISI_BANGUNAN = POSISI_BANGUNAN;
        this.TAHUN_PEMBUATAN = TAHUN_PEMBUATAN;
        this.UKURAN_LOKASI = UKURAN_LOKASI;
        this.UKURAN_BANGUNAN = UKURAN_BANGUNAN;
        this.LEBAR_MUKA_BANGUNAN = LEBAR_MUKA_BANGUNAN;
        this.PANJANG_BANGUNAN = PANJANG_BANGUNAN;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getTIPE_BANGUNAN() {
        return TIPE_BANGUNAN;
    }

    public void setTIPE_BANGUNAN(String TIPE_BANGUNAN) {
        this.TIPE_BANGUNAN = TIPE_BANGUNAN;
    }

    public String getPOSISI_BANGUNAN() {
        return POSISI_BANGUNAN;
    }

    public void setPOSISI_BANGUNAN(String POSISI_BANGUNAN) {
        this.POSISI_BANGUNAN = POSISI_BANGUNAN;
    }

    public String getTAHUN_PEMBUATAN() {
        return TAHUN_PEMBUATAN;
    }

    public void setTAHUN_PEMBUATAN(String TAHUN_PEMBUATAN) {
        this.TAHUN_PEMBUATAN = TAHUN_PEMBUATAN;
    }

    public String getUKURAN_LOKASI() {
        return UKURAN_LOKASI;
    }

    public void setUKURAN_LOKASI(String UKURAN_LOKASI) {
        this.UKURAN_LOKASI = UKURAN_LOKASI;
    }

    public String getUKURAN_BANGUNAN() {
        return UKURAN_BANGUNAN;
    }

    public void setUKURAN_BANGUNAN(String UKURAN_BANGUNAN) {
        this.UKURAN_BANGUNAN = UKURAN_BANGUNAN;
    }

    public String getLEBAR_MUKA_BANGUNAN() {
        return LEBAR_MUKA_BANGUNAN;
    }

    public void setLEBAR_MUKA_BANGUNAN(String LEBAR_MUKA_BANGUNAN) {
        this.LEBAR_MUKA_BANGUNAN = LEBAR_MUKA_BANGUNAN;
    }

    public String getPANJANG_BANGUNAN() {
        return PANJANG_BANGUNAN;
    }

    public void setPANJANG_BANGUNAN(String PANJANG_BANGUNAN) {
        this.PANJANG_BANGUNAN = PANJANG_BANGUNAN;
    }
}
