package com.ck.it.androidapps.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.gpstracker.GpsTracker;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 12/5/2017.
 */

@SuppressWarnings("deprecation")
public class LocationInfoFragment extends Fragment implements View.OnClickListener {
    private View view;
    Button viewNextLocationInfo;
    GpsTracker gps;
    double latitude, longitude;
    EditText viewAlamatLoc, viewDesa, viewKecamatan, viewKabupaten, viewProvinsi, viewKodePos;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.locationinfo_fragment, container, false);
        initView();
        return view;
    }

    void initView() {
        Toast.makeText(view.getContext(), utils.getDataSession(view.getContext(), "sessionUserID"), Toast.LENGTH_LONG).show();
        viewNextLocationInfo = (Button) view.findViewById(R.id.viewNextLocationInfo);
        viewNextLocationInfo.setOnClickListener(this);
        gps = new GpsTracker(view.getContext());
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();

            String dgress = gps.formatLatLongGPSTRACKING(latitude, longitude);
            Toast.makeText(view.getContext(), dgress, Toast.LENGTH_LONG).show();
            System.out.println(dgress);
            System.out.println(latitude + "," + longitude);
        } else {
            Toast.makeText(view.getContext(), "Opps...", Toast.LENGTH_LONG).show();
        }

        viewAlamatLoc = (EditText) view.findViewById(R.id.viewAlamatLoc);
        viewDesa = (EditText) view.findViewById(R.id.viewDesa);
        viewKecamatan = (EditText) view.findViewById(R.id.viewKecamatan);
        viewKabupaten = (EditText) view.findViewById(R.id.viewKabupaten);
        viewProvinsi = (EditText) view.findViewById(R.id.viewProvinsi);
        viewKodePos = (EditText) view.findViewById(R.id.viewKodePos);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextLocationInfo:
                if (TextUtils.isEmpty(viewAlamatLoc.getText().toString())) {
                    viewAlamatLoc.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewDesa.getText().toString())) {
                    viewDesa.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewKecamatan.getText().toString())) {
                    viewKecamatan.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewKabupaten.getText().toString())) {
                    viewKabupaten.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewProvinsi.getText().toString())) {
                    viewProvinsi.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewKodePos.getText().toString())) {
                    viewKodePos.setError(params.MValues.field_error_msg);
                } else {
                    saveIfoLocation(params.URL_METHOD.SAVE_INFO_LOKASI);
                }
                break;
        }
    }

    void saveIfoLocation(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, "")
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.LATITUDE, String.valueOf(latitude))
                .addBodyParameter(params.LONGITUDE, String.valueOf(longitude))
                .addBodyParameter(params.ALAMAT, viewAlamatLoc.getText().toString())
                .addBodyParameter(params.DESA, viewDesa.getText().toString())
                .addBodyParameter(params.KECAMATAN, viewKecamatan.getText().toString())
                .addBodyParameter(params.KABUPATEN, viewKabupaten.getText().toString())
                .addBodyParameter(params.PROVINSI, viewProvinsi.getText().toString())
                .addBodyParameter(params.KODE_POS, viewKodePos.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                if (!response.getJSONObject("data").getString("SITE_CODE").equals("null")) {
                                    utils.setDataSession(view.getContext(), "sessionSiteCode", response.getJSONObject("data").getString("SITE_CODE"));
                                }
                                utils.showFragment(view.getContext(), new ContactInformationFragment());
                                getActivity().setTitle(R.string.informasi_kontak);
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(), "Opps,..Error", Toast.LENGTH_LONG).show();
                    }
                });
    }

    void getUserData(String url) {
        AndroidNetworking.post(url + "user")
                .addBodyParameter("", "")
                .build();
    }
}
