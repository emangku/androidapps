package com.ck.it.androidapps.helpers;

import android.provider.BaseColumns;

/**
 * Created by Creef on 12/18/2017.
 */

public class FieldTable implements BaseColumns{
    private FieldTable() {}
    public static class FieldString implements BaseColumns{
        public static final String USERNAME             = "USERNAME";
        public static final String PASSWORD             = "PASSWORD";
        public static final String REGIONAL             = "REGIONAL";

        //Info Lokasi
        public static final String SITE_CODE            = "SITE_CODE";
        public static final String USER_ID              = "USER_ID";
        public static final String LATITUDE             = "LATITUDE";
        public static final String LONGITUDE            = "LONGITUDE";
        public static final String ALAMAT               = "ALAMAT";
        public static final String DESA                 = "DESA";
        public static final String KECAMATAN            = "KECAMATAN";
        public static final String KABUPATEN            = "KABUPATEN";
        public static final String PROVINSI             = "PROVINSI";
        public static final String KODE_POS             = "KODE_POS";


        //info Kontak

        public static final String NAMA_KONTAK              = "NAMA_KONTAK";
        public static final String NOMOR_TELEPON            = "NOMOR_TELEPON";
        public static final String K_ALAMAT                 = "ALAMAT";
        public static final String HUBUNGAN_DENGAN_PEMILIK  = "HUBUNGAN_DENGAN_PEMILIK";

        // Info Kepemilikan

        public static final String NAMA_PEMILIK             = "NAMA_PEMILIK";
        public static final String BADAN_HUKUM              = "BADAN_HUKUM";
        public static final String O_ALAMAT                 = "ALAMAT";
        public static final String NO_TELP_FAX_HO_HP        = "NO_TELP_FAX_HO_HP";
        public static final String EMAIL                    = "EMAIL";

        //info sewa

        public static final String RENCANA_KERJASAMA        = "RENCANA_KERJASAMA";
        public static final String HARGA_PENAWARAN_SEWA     = "HARGA_PENAWARAN_SEWA";
        public static final String PERIODE_SEWA             = "PERIODE_SEWA";
        public static final String GRACE_PERIOD             = "GRACE_PERIOD";
        public static final String STATUS_HARGA             = "STATUS_HARGA";
        public static final String SERVICE_CHARGE           = "SERVICE_CHARGE";
        public static final String BIAYA_PAJAK              = "BIAYA_PAJAK";
        public static final String BIAYA_NOTARIS            = "BIAYA_NOTARIS";

        //info logalitas

        public static final String SERTIFIKASI              = "SERTIFIKASI";
        public static final String ATAS_NAMA                = "ATAS_NAMA";
        public static final String LUAS_TANAH               = "LUAS_TANAH";
        public static final String IJIN_MENDIRIKAN_BANGUNAN = "IJIN_MENDIRIKAN_BANGUNAN";
    }

}
