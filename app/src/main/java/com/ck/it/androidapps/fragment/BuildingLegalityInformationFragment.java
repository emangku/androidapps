package com.ck.it.androidapps.fragment;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.fragment.siteanalysis.SituasiBangunanFragment;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 12/5/2017.
 */

@SuppressWarnings("deprecation")
public class BuildingLegalityInformationFragment extends Fragment implements View.OnClickListener {
    private View view;
    Button viewNextLegality;
    EditText viewAtasNama, viewLuasTanah;
    RadioGroup viewSertifikasi, viewIjinMendirikanBnagunan;
    String Sertifikasi = "";
    String IMB = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.buildinglegality, container, false);
        initView();
        return view;
    }


    void initView() {
        viewNextLegality = (Button) view.findViewById(R.id.viewNextLegality);
        viewNextLegality.setOnClickListener(this);
        viewAtasNama = (EditText) view.findViewById(R.id.viewAtasNama);
        viewLuasTanah = (EditText) view.findViewById(R.id.viewLuasTanah);

        viewSertifikasi = (RadioGroup) view.findViewById(R.id.viewSertifikasi);
        viewSertifikasi.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.viewSHM:
                        Sertifikasi = "SHM";
                        break;
                    case R.id.viewHGU:
                        Sertifikasi = "HGU";
                        break;
                    case R.id.viewHGB:
                        Sertifikasi = "HGB";
                        break;
                    case R.id.viewGIRIK:
                        Sertifikasi = "GIRIK";
                        break;
                }
            }
        });

        viewIjinMendirikanBnagunan = (RadioGroup) view.findViewById(R.id.viewIjinMendirikanBnagunan);
        viewIjinMendirikanBnagunan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.viewSementara:
                        IMB = "Sementara";
                        break;
                    case R.id.viewTetap:
                        IMB = "Tetap";
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextLegality:
                if (Sertifikasi.equals("") && IMB.equals("")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(viewAtasNama.getText().toString())) {
                    viewAtasNama.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewLuasTanah.getText().toString())) {
                    viewLuasTanah.setError(params.MValues.field_error_msg);
                } else {
                    saveLegality(params.URL_METHOD.SAVE_LEGALITAS);
                }
                break;
        }
    }

    void saveLegality(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.SERTIFIKASI, Sertifikasi)
                .addBodyParameter(params.ATAS_NAMA, viewAtasNama.getText().toString())
                .addBodyParameter(params.LUAS_TANAH, viewLuasTanah.getText().toString())
                .addBodyParameter(params.IJIN_MENDIRIKAN_BANGUNAN, IMB)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new SituasiBangunanFragment());
                                getActivity().setTitle(R.string.situasi_bangunan);
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(), "Opss Error...", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
