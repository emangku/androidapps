package com.ck.it.androidapps.fragment.siteanalysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.ck.it.androidapps.R;

/**
 * Created by Creef on 12/6/2017.
 */

public class VisibilityFragment extends Fragment implements View.OnClickListener{
    private View view;
    String[] vbkiri = {
            "-Visibility bangunan dari sisi kiri-",
            "Mudah dilihat",
            "Cukup terlihat",
            "Tidak terlihat"};

    String[] vbKanan = {
            "-Visibility bangunan dari sisi kanan-",
            "Mudah dilihat",
            "Cukup terlihat",
            "Tidak terlihat"};
    String[] kms = {
            "-Kemudahan menempatkan Sign-",
            "Mudah",
            "Cukup Mudah",
            "Sulit",
            "Sangat Sulit"};
    String[] pb = {
            "-Penghalang Bangunan-",
            "Pagar Pembatas",
            "Trotoar",
            "Parit",
            "Jalan",
            "Halte",
            "Jembatan Penyebrangan"};
    String[] pembatas = {
            "-Pembatas (Barrier)-",
            "Pedagang Liar",
            "Pohon",
            "Tiang Listrik",
            "Tembok Tetangga",
            "Papan Reklame"
    };

    Spinner viewVBKiri,viewVBKanan,viewKMSign,viewPenghalangBangunan,vewPembatas;
    Button viewNextVisibility;
    ArrayAdapter<String> VBKiriAdapter, VBKananAdapter,VNPBAdapter,VBPembatasAdapter,KMSAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.visibility_fragment,container,false);
        initView();
        return view;
    }
    void initView(){
        viewNextVisibility = (Button) view.findViewById(R.id.viewNextVisibility);
        viewNextVisibility.setOnClickListener(this);

        viewVBKiri = (Spinner) view.findViewById(R.id.viewVBKiri);
        VBKiriAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,vbkiri);
        viewVBKiri.setAdapter(VBKiriAdapter);

        viewVBKanan = (Spinner) view.findViewById(R.id.viewVBKanan);
        VBKananAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,vbKanan);
        viewVBKanan.setAdapter(VBKananAdapter);


        viewKMSign = (Spinner) view.findViewById(R.id.viewKMSign);
        KMSAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,kms);
        viewKMSign.setAdapter(KMSAdapter);


        viewPenghalangBangunan = (Spinner) view.findViewById(R.id.viewPenghalangBangunan);
        VNPBAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,pb);
        viewPenghalangBangunan.setAdapter(VNPBAdapter);


        vewPembatas = (Spinner) view.findViewById(R.id.vewPembatas);
        VBPembatasAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,pembatas);
        vewPembatas.setAdapter(VBPembatasAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextVisibility:
                break;
        }
    }
}
