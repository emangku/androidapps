package com.ck.it.androidapps.model;

/**
 * Created by Creef on 12/17/2017.
 */

public class InfoKontakModel {
    private String SITE_CODE;
    private String USER_ID;
    private String NAMA_KONTAK;
    private String NOMOR_TELEPON;
    private String ALAMAT;
    private String HUBUNGAN_DENGAN_PEMILIK;

    public InfoKontakModel() {
    }

    public InfoKontakModel(String SITE_CODE, String USER_ID, String NAMA_KONTAK, String NOMOR_TELEPON, String ALAMAT, String HUBUNGAN_DENGAN_PEMILIK) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.NAMA_KONTAK = NAMA_KONTAK;
        this.NOMOR_TELEPON = NOMOR_TELEPON;
        this.ALAMAT = ALAMAT;
        this.HUBUNGAN_DENGAN_PEMILIK = HUBUNGAN_DENGAN_PEMILIK;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getNAMA_KONTAK() {
        return NAMA_KONTAK;
    }

    public void setNAMA_KONTAK(String NAMA_KONTAK) {
        this.NAMA_KONTAK = NAMA_KONTAK;
    }

    public String getNOMOR_TELEPON() {
        return NOMOR_TELEPON;
    }

    public void setNOMOR_TELEPON(String NOMOR_TELEPON) {
        this.NOMOR_TELEPON = NOMOR_TELEPON;
    }

    public String getALAMAT() {
        return ALAMAT;
    }

    public void setALAMAT(String ALAMAT) {
        this.ALAMAT = ALAMAT;
    }

    public String getHUBUNGAN_DENGAN_PEMILIK() {
        return HUBUNGAN_DENGAN_PEMILIK;
    }

    public void setHUBUNGAN_DENGAN_PEMILIK(String HUBUNGAN_DENGAN_PEMILIK) {
        this.HUBUNGAN_DENGAN_PEMILIK = HUBUNGAN_DENGAN_PEMILIK;
    }
}
