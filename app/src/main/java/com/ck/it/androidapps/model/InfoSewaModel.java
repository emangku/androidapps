package com.ck.it.androidapps.model;


public class InfoSewaModel {
    private String SITE_CODE;
    private String USER_ID;
    private String RENACAN_KERJASAMA;
    private String HARGA_PENAWARAN_SEWA;
    private String PERIODE_SEWA;
    private String GRACE_PERIOD;
    private String STATUS_HARGA;
    private String SERVICE_CHARGE;
    private String BIAYA_PAJAK;
    private String BIAYA_NOTARIS;

    public InfoSewaModel() {
    }

    public InfoSewaModel(String SITE_CODE, String USER_ID, String RENACAN_KERJASAMA, String HARGA_PENAWARAN_SEWA, String PERIODE_SEWA, String GRACE_PERIOD, String STATUS_HARGA, String SERVICE_CHARGE, String BIAYA_PAJAK, String BIAYA_NOTARIS) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.RENACAN_KERJASAMA = RENACAN_KERJASAMA;
        this.HARGA_PENAWARAN_SEWA = HARGA_PENAWARAN_SEWA;
        this.PERIODE_SEWA = PERIODE_SEWA;
        this.GRACE_PERIOD = GRACE_PERIOD;
        this.STATUS_HARGA = STATUS_HARGA;
        this.SERVICE_CHARGE = SERVICE_CHARGE;
        this.BIAYA_PAJAK = BIAYA_PAJAK;
        this.BIAYA_NOTARIS = BIAYA_NOTARIS;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getRENACAN_KERJASAMA() {
        return RENACAN_KERJASAMA;
    }

    public void setRENACAN_KERJASAMA(String RENACAN_KERJASAMA) {
        this.RENACAN_KERJASAMA = RENACAN_KERJASAMA;
    }

    public String getHARGA_PENAWARAN_SEWA() {
        return HARGA_PENAWARAN_SEWA;
    }

    public void setHARGA_PENAWARAN_SEWA(String HARGA_PENAWARAN_SEWA) {
        this.HARGA_PENAWARAN_SEWA = HARGA_PENAWARAN_SEWA;
    }

    public String getPERIODE_SEWA() {
        return PERIODE_SEWA;
    }

    public void setPERIODE_SEWA(String PERIODE_SEWA) {
        this.PERIODE_SEWA = PERIODE_SEWA;
    }

    public String getGRACE_PERIOD() {
        return GRACE_PERIOD;
    }

    public void setGRACE_PERIOD(String GRACE_PERIOD) {
        this.GRACE_PERIOD = GRACE_PERIOD;
    }

    public String getSTATUS_HARGA() {
        return STATUS_HARGA;
    }

    public void setSTATUS_HARGA(String STATUS_HARGA) {
        this.STATUS_HARGA = STATUS_HARGA;
    }

    public String getSERVICE_CHARGE() {
        return SERVICE_CHARGE;
    }

    public void setSERVICE_CHARGE(String SERVICE_CHARGE) {
        this.SERVICE_CHARGE = SERVICE_CHARGE;
    }

    public String getBIAYA_PAJAK() {
        return BIAYA_PAJAK;
    }

    public void setBIAYA_PAJAK(String BIAYA_PAJAK) {
        this.BIAYA_PAJAK = BIAYA_PAJAK;
    }

    public String getBIAYA_NOTARIS() {
        return BIAYA_NOTARIS;
    }

    public void setBIAYA_NOTARIS(String BIAYA_NOTARIS) {
        this.BIAYA_NOTARIS = BIAYA_NOTARIS;
    }
}
