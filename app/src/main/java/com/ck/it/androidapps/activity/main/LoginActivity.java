package com.ck.it.androidapps.activity.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.session.SessionManager;
import com.ck.it.androidapps.helpers.UserSessionManager;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("deprecation")
public class LoginActivity extends AppCompatActivity implements View.OnClickListener {
    Button viewLogin;
    SessionManager sessionManager;
    EditText viewUserID, viewPassword;
    UserSessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_login);
        initView();
    }

    void initView() {
        session = new UserSessionManager(getApplicationContext());
        viewLogin = (Button) findViewById(R.id.viewLogin);
        viewLogin.setOnClickListener(this);
        viewUserID = (EditText) findViewById(R.id.viewUserID);
        viewPassword = (EditText) findViewById(R.id.viewPassword);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewLogin:
                if (TextUtils.isEmpty(viewUserID.getText().toString())) {
                    viewUserID.setError("Tidak boleh kosong");
                } else if (TextUtils.isEmpty(viewPassword.getText().toString())) {
                    viewPassword.setError("Tidak boleh kosong");
                } else {
                    onLoginStatement("login");
                }
//                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                break;
        }

//        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    void onLoginStatement(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(this, params.MValues.loading_msg);
        dialog.show();
//        final AlertDialog dialog = utils.CustomeDialogProgress(this).create();
//        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.USER_ID, viewUserID.getText().toString())
                .addBodyParameter(params.PASSWORD, viewPassword.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            System.out.println("Data : " + response.toString());
                            if (response.getString("status").equals("true")) {
//                                sessionManager.createUserLoginSession(response.getJSONObject("data").getString("USERNAME"), response.getJSONObject("data").getString("EMAIL"));
                                if (!response.getJSONObject("data").getString("USERNAME").equals("null")) {
                                    utils.setDataSession(LoginActivity.this, "sessionUserLogin", response.getJSONObject("data").getString("USERNAME"));
//                                    Toast.makeText(LoginActivity.this,response.getJSONObject("data").getString("USERNAME"),Toast.LENGTH_LONG).show();
                                }
                                if (!response.getJSONObject("data").getString("USER_ID").equals("null")) {
                                    utils.setDataSession(LoginActivity.this, "sessionUserID", response.getJSONObject("data").getString("USER_ID"));
//                                    Toast.makeText(LoginActivity.this,response.getJSONObject("data").getString("USER_ID"),Toast.LENGTH_LONG).show();
                                }
                                if (!response.getJSONObject("data").getString("S_EMAIL").equals("null")) {
                                    utils.setDataSession(LoginActivity.this, "sessionEmail", response.getJSONObject("data").getString("S_EMAIL"));
//                                    Toast.makeText(LoginActivity.this,response.getJSONObject("data").getString("EMAIl"),Toast.LENGTH_LONG).show();
                                }
                                if (!response.getJSONObject("data").getString("IMAGE").equals("null")) {
                                    utils.setDataSession(LoginActivity.this, "sessionIMAGE", response.getJSONObject("data").getString("IMAGE"));
                                }
                                if (!response.getJSONObject("data").getString("USER_STATUS").equals("null")) {
                                    utils.setDataSession(LoginActivity.this, "sessionUSER_STATUS", response.getJSONObject("data").getString("USER_STATUS"));
                                }
                                if (!response.getJSONObject("data").getString("NO_STATUS").equals("null")) {
                                    utils.setDataSession(LoginActivity.this, "sessionNO_STATUS", response.getJSONObject("data").getString("NO_STATUS"));
                                }
                                if (!response.getJSONObject("data").getString("STATUS").equals("null")) {
                                    utils.setDataSession(LoginActivity.this, "sessionSTATUS", response.getJSONObject("data").getString("STATUS"));
                                }
                                session.createUserLoginSession(response.getJSONObject("data").getString("USERNAME"), response.getJSONObject("data").getString("USER_ID"));
                                dialog.dismiss();
                                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                            }else if (response.getString("status").equals("false")){
                                dialog.dismiss();
                                Toast.makeText(getApplicationContext(),"Username / password Salah", Toast.LENGTH_SHORT).show();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(LoginActivity.this, "Opps, Error : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                        System.out.println(anError.getMessage());
                        dialog.dismiss();
                    }
                });
    }


}
