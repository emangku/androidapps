package com.ck.it.androidapps.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 12/5/2017.
 */

@SuppressWarnings("deprecation")
public class OwnershipInformationFragment extends Fragment implements View.OnClickListener {
    private View view;
    Button viewNextOwner;
    EditText viewNamaPemilik, viewBadanHukum, viewOAlamat, viewNoTelpFaxHoHp, viewEmail;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ownershipinformation, container, false);
        initView();
        return view;
    }

    void initView() {
        viewNextOwner = (Button) view.findViewById(R.id.viewNextOwner);
        viewNextOwner.setOnClickListener(this);
        viewNamaPemilik = (EditText) view.findViewById(R.id.viewNamaPemilik);
        viewBadanHukum = (EditText) view.findViewById(R.id.viewBadanHukum);
        viewOAlamat = (EditText) view.findViewById(R.id.viewOAlamat);
        viewNoTelpFaxHoHp = (EditText) view.findViewById(R.id.viewNoTelpFaxHoHp);
        viewEmail = (EditText) view.findViewById(R.id.viewEmail);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextOwner:
                if (TextUtils.isEmpty(viewNamaPemilik.getText().toString())) {
                    viewNamaPemilik.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewBadanHukum.getText().toString())) {
                    viewBadanHukum.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewOAlamat.getText().toString())) {
                    viewOAlamat.setError(params.MValues.field_error_msg);
                } else if (TextUtils.isEmpty(viewNoTelpFaxHoHp.getText().toString())) {
                    viewNoTelpFaxHoHp.setError(params.MValues.field_error_msg);
                } else {
                    saveInfoPemilik(params.URL_METHOD.SAVE_INFO_KEPEMILIKAN);
                }
                break;
        }
    }

    void saveInfoPemilik(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.NAMA_PEMILIK, viewNamaPemilik.getText().toString())
                .addBodyParameter(params.BADAN_HUKUM, viewBadanHukum.getText().toString())
                .addBodyParameter(params.O_ALAMAT, viewOAlamat.getText().toString())
                .addBodyParameter(params.NOTLP_OR_FAX_OR_NOHP, viewNoTelpFaxHoHp.getText().toString())
                .addBodyParameter(params.EMAIL, viewEmail.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new RentalinformationFragment());
                                getActivity().setTitle(R.string.informasi_sewa);
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(), "Opsss, Error..", Toast.LENGTH_LONG).show();
                    }
                });
    }
}
