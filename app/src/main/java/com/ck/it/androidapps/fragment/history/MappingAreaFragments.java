package com.ck.it.androidapps.fragment.history;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.adapter.MappingAdapter;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.model.MappingModels;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Creef on 1/16/2018.
 */

@SuppressWarnings("deprecation")
public class MappingAreaFragments extends Fragment implements SwipeRefreshLayout.OnRefreshListener {
    private View view;
    ImageView mapping;
    RecyclerView viewRCYL;
    List<MappingModels> list;
    MappingAdapter adapter;
    private SwipeRefreshLayout viewMappingRefresh;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.mapping_area_fragment, container, false);
        initView();
        return view;
    }

    void initView() {
        viewRCYL = (RecyclerView) view.findViewById(R.id.viewRCYL);
        viewMappingRefresh = (SwipeRefreshLayout) view.findViewById(R.id.viewMappingRefresh);
        viewMappingRefresh.setOnRefreshListener(this);
        getDataMppingbySiteCode(params.URL_METHOD.GET_MAPPING);
    }

    //https://github.com/firebase/quickstart-android/blob/master/messaging/app/src/main/java/com/google/firebase/quickstart/fcm/MainActivity.java
    void getDataMppingbySiteCode(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionValKeySiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionValKeyUserID"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                JSONArray array = response.getJSONArray("data");
                                list = new ArrayList<MappingModels>();
                                for (int i = 0; i < array.length(); i++) {
                                    MappingModels models = new MappingModels();
                                    JSONObject obj = array.getJSONObject(i);
                                    models.setSITE_CODE(obj.getString("SITE_CODE"));
                                    models.setUSER_ID(obj.getString("USER_ID"));
                                    models.setIMAGE(obj.getString("IMAGE"));
                                    models.setLATITUDE(obj.getString("LATITUDE"));
                                    models.setLONGITUDE(obj.getString("LONGITUDE"));
                                    models.setCreated_at(obj.getString("created_at"));
                                    models.setUpdated_at(obj.getString("updated_at"));
                                    list.add(models);
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                        viewRCYL.setLayoutManager(new LinearLayoutManager(view.getContext()));
                        adapter = new MappingAdapter(view.getContext(), list);
                        viewRCYL.setAdapter(adapter);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(),"Opps Error Mapp : "+anError.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public void onRefresh() {
        getDataMppingbySiteCode(params.URL_METHOD.GET_MAPPING);
        viewMappingRefresh.setRefreshing(false);
    }
}
