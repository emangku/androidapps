package com.ck.it.androidapps.fragment.FS;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 1/30/2018.
 */

@SuppressWarnings("deprecation")
public class FSIncomFragments extends Fragment implements View.OnClickListener {
    private View view;
    EditText viewBest, viewAverage,viewWorse,viewGPStandard;
    Button viewBtnIncome;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fs_income_fragment, container, false);
        init();
        return view;
    }

    void init(){
        getActivity().setTitle("FS Income");
        viewBest = (EditText) view.findViewById(R.id.viewBest);
        viewAverage = (EditText) view.findViewById(R.id.viewAverage);
        viewWorse = (EditText) view.findViewById(R.id.viewWorse);
        viewGPStandard = (EditText) view.findViewById(R.id.viewGPStandard);
        viewBtnIncome = (Button) view.findViewById(R.id.viewBtnIncome);
        viewBtnIncome.setOnClickListener(this);
    }

    void saveIncome(String url){
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(),"Tunggu...");
        dialog.show();
        AndroidNetworking.post(api.SERVER_API+url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(),"sessionValKeySiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(),"sessionUserID"))
                .addBodyParameter(params.BEST, viewBest.getText().toString())
                .addBodyParameter(params.AVERAGE, viewAverage.getText().toString())
                .addBodyParameter(params.WORSE, viewWorse.getText().toString())
                .addBodyParameter(params.GP_STANDARD, viewGPStandard.getText().toString())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
                                ((FragmentActivity)view.getContext())
                                        .getSupportFragmentManager()
                                        .beginTransaction()
                                        .addToBackStack(null)
                                        .replace(R.id.viewFS,new FSBenchmarkFragments())
                                        .commit();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(),"Opps..Error "+anError.getMessage(), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewBtnIncome:
                if (TextUtils.isEmpty(viewBest.getText().toString())){
                    viewBest.setError(params.MValues.field_error_msg);
                    viewBest.requestFocus();
                }else if(TextUtils.isEmpty(viewAverage.getText().toString())){
                    viewAverage.setError(params.MValues.field_error_msg);
                    viewAverage.requestFocus();
                } else if(TextUtils.isEmpty(viewWorse.getText().toString())){
                    viewWorse.setError(params.MValues.field_error_msg);
                    viewWorse.requestFocus();
                }else if(TextUtils.isEmpty(viewGPStandard.getText().toString())){
                    viewGPStandard.setError(params.MValues.field_error_msg);
                    viewGPStandard.requestFocus();
                }else{
                    saveIncome(params.URL_METHOD.SAVE_INCOME);
                }
                break;
        }
    }
}
