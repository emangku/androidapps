package com.ck.it.androidapps.fragment.siteanalysis;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 12/21/2017.
 */

@SuppressWarnings("deprecation")
public class KemiringanLokasiFragment extends Fragment implements View.OnClickListener {
    private View view;
    String[] kemiringanlokasi = {"-Kemiringan Lokasi-", "Jalan menurun", "Jalan menanjak", "Jalan menikung", "Jalan datar", "Lampu merah"};
    Spinner viewKemiringanLokasi;
    Button viewNextKemiringanLokasi;
    RadioGroup viewRadioKemiringanLokasi;
    ArrayAdapter<String> klAdapter;
    String opt = "";
    String type_opt = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.kemiringan_lokasi, container, false);
        initView();
        return view;
    }

    void initView() {
        viewKemiringanLokasi = (Spinner) view.findViewById(R.id.viewKemiringanLokasi);
        klAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, kemiringanlokasi);
        viewKemiringanLokasi.setAdapter(klAdapter);
        viewKemiringanLokasi.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type_opt = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        viewRadioKemiringanLokasi = (RadioGroup) view.findViewById(R.id.viewRadioKemiringanLokasi);
        viewRadioKemiringanLokasi.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.kl_1:
                        opt = "1";
                        break;
                    case R.id.kl_2:
                        opt = "2";
                        break;
                    case R.id.kl_3:
                        opt = "3";
                        break;
                    case R.id.kl_4:
                        opt = "4";
                        break;
                    case R.id.kl_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewNextKemiringanLokasi = (Button) view.findViewById(R.id.viewNextKemiringanLokasi);
        viewNextKemiringanLokasi.setOnClickListener(this);
    }

    void saveKemiringanLokasi(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.SA_ID, "SA-KB")
                .addBodyParameter(params.TYPE, "Kemiringan Lokasi")
                .addBodyParameter(params.VALUE_OPTION, type_opt)
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new SebelahKiriFragment());
                                getActivity().setTitle(R.string.penyanding);
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext().getApplicationContext(), "Oppss, Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextKemiringanLokasi:
                if (type_opt.equals("") || type_opt.equals("-Kemiringan Lokasi-")) {
                    Toast.makeText(view.getContext().getApplicationContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (opt.equals("")) {
                    Toast.makeText(view.getContext().getApplicationContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else {
                    saveKemiringanLokasi(params.URL_METHOD.SAVE_SITE_ANALYSIS);
                }
                break;
        }
    }


}
