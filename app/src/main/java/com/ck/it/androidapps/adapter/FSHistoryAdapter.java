package com.ck.it.androidapps.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.fragment.FS.fs_history.DetailsFS;
import com.ck.it.androidapps.model.ModelFSDetails;
import com.ck.it.androidapps.util.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by eko on 06/03/18.
 */

public class FSHistoryAdapter extends RecyclerView.Adapter<FSHistoryAdapter.ViewHolder> {
    private View view;
    private Context context;
    private List<ModelFSDetails> list = new ArrayList<>();

    public FSHistoryAdapter(Context context, List<ModelFSDetails> list) {
        this.context = context;
        this.list = list;
    }

    @Override
    public FSHistoryAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_fs, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FSHistoryAdapter.ViewHolder holder, final int position) {
        holder.viewIDSiteCode.setText(list.get(position).getSITE_CODE());
        holder.viewIDateCreate.setText(list.get(position).getCreated_at());
        holder.viewClickFSHistory.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onClick(View v) {
                utils.setDataSession(view.getContext(), "sessionUserId", list.get(position).getUSER_ID());
                view.getContext().startActivity(new Intent(view.getContext(), DetailsFS.class));
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView viewIDSiteCode, viewIDateCreate;
        LinearLayout viewClickFSHistory;

        public ViewHolder(View itemView) {
            super(itemView);
            viewIDateCreate = (TextView) itemView.findViewById(R.id.viewIDateCreate);
            viewIDSiteCode = (TextView) itemView.findViewById(R.id.viewIDSiteCode);
            viewClickFSHistory = (LinearLayout) itemView.findViewById(R.id.viewClickFSHistory);
        }
    }
}
