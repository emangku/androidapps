package com.ck.it.androidapps.fragment.siteanalysis;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eko on 03/01/18.
 */

@SuppressWarnings("deprecation")
public class VisibilityBangunanDariSisiKanan extends Fragment implements View.OnClickListener{
    private View view;
    String[] vbKanan = {
            "-Visibility bangunan dari sisi kanan-",
            "Mudah dilihat",
            "Cukup terlihat",
            "Tidak terlihat"};
    Spinner viewVBKanan;
    ArrayAdapter<String> VBKananAdapter;
    Button viewNextVisibilityKanan;
    RadioGroup viewGroupVisibilityKanan;
    String opt = "";
    String type_opt = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view  = inflater.inflate(R.layout.visibility_bangunan_sisi_kanan, container, false);
        initView();
        return view;
    }

    void initView(){
        viewVBKanan = (Spinner)view.findViewById(R.id.viewVBKanan);
        VBKananAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,vbKanan);
        viewVBKanan.setAdapter(VBKananAdapter);
        viewVBKanan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type_opt = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        viewGroupVisibilityKanan = (RadioGroup) view.findViewById(R.id.viewGroupVisibilityKanan);
        viewGroupVisibilityKanan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId){
                    case R.id.vka_1:
                        opt = "1";
                        break;
                    case R.id.vka_2:
                        opt = "2";
                        break;
                    case R.id.vka_3:
                        opt = "3";
                        break;
                    case R.id.vka_4:
                        opt = "4";
                        break;
                    case R.id.vka_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewNextVisibilityKanan = (Button)view.findViewById(R.id.viewNextVisibilityKanan);
        viewNextVisibilityKanan.setOnClickListener(this);
    }
    void saveData(String url){
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API+url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(),"sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.SA_ID, "SA")
                .addBodyParameter(params.TYPE, "Visibility bangunan dari sisi kanan")
                .addBodyParameter(params.VALUE_OPTION, type_opt)
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
                                utils.showFragment(view.getContext(),new KemudahanMenempatkanSign());
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext().getApplicationContext(), "Oppss, Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextVisibilityKanan:
                if (type_opt.equals("") && type_opt.equals("-Visibility bangunan dari sisi kanan-")){
                    Toast.makeText(view.getContext().getApplicationContext(),params.MValues.option_msg, Toast.LENGTH_LONG).show();
                }else if (opt.equals("")){
                    Toast.makeText(view.getContext().getApplicationContext(),params.MValues.option_msg, Toast.LENGTH_LONG).show();
                }else {
                    saveData(params.URL_METHOD.SAVE_SITE_ANALYSIS);
                }
                break;
        }
    }
}
