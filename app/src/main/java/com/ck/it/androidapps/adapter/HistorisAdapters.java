package com.ck.it.androidapps.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.main.DesignLayoutActivity;
import com.ck.it.androidapps.activity.main.FSInputActivity;
import com.ck.it.androidapps.activity.main.HistoryActivity;
import com.ck.it.androidapps.activity.main.LegalAnalisaActivity;
import com.ck.it.androidapps.activity.main.MOU_PT_RSActivity;
import com.ck.it.androidapps.fragment.FS.fs_history.FSHistoryActivity;
import com.ck.it.androidapps.model.ModelHistory;
import com.ck.it.androidapps.util.utils;

import java.util.ArrayList;
import java.util.List;

public class HistorisAdapters extends RecyclerView.Adapter<HistorisAdapters.ViewHolder> {
    private View view;
    private View mView;
    private Context context;
    List<ModelHistory> list = new ArrayList<>();
    private LinearLayout viewBTNFS, viewBTNHistory,
                    viewMouClick,viewLegalAnalysisClick,
                    viewDesignLayoutClick,viewCE_BOQClick,viewBDEquipmentClick,
                    viewMDEquipmentClick,viewMarketingClick,viewBudgetClick,
                    viewFinalFSClick,viewSOKClick,viewDocperjanjianClick,
                    viewkonstruksiClick,viewEqupmentClick,viewOpeningClick,viewSalesClick;
    private AlertDialog.Builder builder;

    public HistorisAdapters(Context context, List<ModelHistory> mList) {
        this.context = context;
        this.list = mList;
    }

    @Override
    public HistorisAdapters.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.history, null, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final HistorisAdapters.ViewHolder holder, final int position) {
        holder.viewTanggal.setText(list.get(position).getCreated_at());
        holder.viewDesa.setText(list.get(position).getDESA());
        holder.viewAlamat.setText(list.get(position).getALAMAT_A());
        holder.viewProvinsi.setText(list.get(position).getPROVINSI());
        holder.viewValueSiteCode.setText(list.get(position).getSITE_CODE());
        holder.viewCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                mView = LayoutInflater.from(view.getContext()).inflate(R.layout.option_history_click, null, false);
                builder = new AlertDialog.Builder(view.getContext());
                builder.setView(mView);
                builder.setTitle("Silahkan pilih .. !");
                builder.setIcon(R.drawable.ic_info);
                viewBTNFS = (LinearLayout) mView.findViewById(R.id.viewBTNFS);
                viewBTNHistory = (LinearLayout) mView.findViewById(R.id.viewBTNHistory);
                viewMouClick = (LinearLayout) mView.findViewById(R.id.viewMouClick);
                viewLegalAnalysisClick = (LinearLayout) mView.findViewById(R.id.viewLegalAnalysisClick);
                viewDesignLayoutClick = (LinearLayout) mView.findViewById(R.id.viewDesignLayoutClick);
                viewCE_BOQClick = (LinearLayout) mView.findViewById(R.id.viewCE_BOQClick);
                viewBDEquipmentClick = (LinearLayout) mView.findViewById(R.id.viewBDEquipmentClick);
                viewMDEquipmentClick = (LinearLayout) mView.findViewById(R.id.viewMDEquipmentClick);
                viewMarketingClick = (LinearLayout) mView.findViewById(R.id.viewMarketingClick);
                viewBudgetClick = (LinearLayout) mView.findViewById(R.id.viewBudgetClick);
                viewFinalFSClick = (LinearLayout) mView.findViewById(R.id.viewFinalFSClick);
                viewSOKClick = (LinearLayout) mView.findViewById(R.id.viewSOKClick);
                viewDocperjanjianClick = (LinearLayout) mView.findViewById(R.id.viewDocperjanjianClick);
                viewkonstruksiClick = (LinearLayout) mView.findViewById(R.id.viewkonstruksiClick);
                viewEqupmentClick = (LinearLayout) mView.findViewById(R.id.viewEqupmentClick);
                viewOpeningClick = (LinearLayout) mView.findViewById(R.id.viewOpeningClick);
                viewSalesClick = (LinearLayout) mView.findViewById(R.id.viewSalesClick);
                final AlertDialog dialog = builder.create();

                viewBTNFS.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), FSInputActivity.class));
                        dialog.cancel();
                    }
                });
                viewBTNHistory.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), HistoryActivity.class));
                        dialog.cancel();
                    }
                });
                viewMouClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), MOU_PT_RSActivity.class));
                    }
                });
                viewLegalAnalysisClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), LegalAnalisaActivity.class));
                    }
                });
                viewDesignLayoutClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewCE_BOQClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewBDEquipmentClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewMDEquipmentClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewMarketingClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewBudgetClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewFinalFSClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), FSHistoryActivity.class));
                    }
                });
                viewSOKClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewDocperjanjianClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewkonstruksiClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewEqupmentClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewOpeningClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                viewSalesClick.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        utils.setDataSession(view.getContext(), "sessionValKeySiteCode", list.get(position).getSITE_CODE());
                        utils.setDataSession(view.getContext(), "sessionValKeyUserID", list.get(position).getUSER_ID());
                        view.getContext().startActivity(new Intent(view.getContext(), DesignLayoutActivity.class));
                    }
                });
                dialog.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        if (list != null) {
            return list.size();
        }
        return 0;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView viewTanggal, viewDesa, viewAlamat, viewProvinsi, viewValueSiteCode;
        LinearLayout viewCard;

        public ViewHolder(View itemView) {
            super(itemView);
            viewValueSiteCode = (TextView) itemView.findViewById(R.id.viewValueSiteCode);
            viewProvinsi = (TextView) itemView.findViewById(R.id.viewProvinsi);
            viewAlamat = (TextView) itemView.findViewById(R.id.viewAlamat);
            viewDesa = (TextView) itemView.findViewById(R.id.viewDesa);
            viewTanggal = (TextView) itemView.findViewById(R.id.viewTanggal);
            viewCard = (LinearLayout) itemView.findViewById(R.id.viewCard);
        }
    }
}
