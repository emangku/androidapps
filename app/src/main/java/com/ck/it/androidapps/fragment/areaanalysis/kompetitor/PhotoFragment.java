package com.ck.it.androidapps.fragment.areaanalysis.kompetitor;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;
import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static android.app.Activity.RESULT_OK;

/**
 * Created by eko on 04/01/18.
 */

@SuppressWarnings("deprecation")
public class PhotoFragment extends Fragment implements View.OnClickListener {
    private View view;
    Button viewNextPhoto;
    ImageView viewPhoto;
    CameraPhoto cameraPhoto;
    RadioGroup viewGroupPhoto;
    int CAMERA_REQUEST = 1100;
    File file;
    String opt = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.photofragment, container, false);
        initView();
        return view;
    }

    void initView() {
        new AlertDialog.Builder(view.getContext())
                .setTitle("Note")
                .setIcon(R.drawable.ic_info)
                .setView(R.layout.info_kompetitor)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
        viewNextPhoto = (Button) view.findViewById(R.id.viewNextPhoto);
        viewNextPhoto.setOnClickListener(this);
        viewGroupPhoto = (RadioGroup) view.findViewById(R.id.viewGroupPhoto);
        viewGroupPhoto.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.pt_1:
                        opt = "1";
                        break;
                    case R.id.pt_2:
                        opt = "2";
                        break;
                    case R.id.pt_3:
                        opt = "3";
                        break;
                    case R.id.pt_4:
                        opt = "4";
                        break;
                    case R.id.pt_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewPhoto = (ImageView) view.findViewById(R.id.viewPhoto);
        viewPhoto.setOnClickListener(this);
        cameraPhoto = new CameraPhoto(view.getContext().getApplicationContext());
    }

    void saveData(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.upload(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addMultipartParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addMultipartParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addMultipartParameter(params.AA_ID, "AA")
                .addMultipartFile(params.IMAGE, file)
                .addMultipartParameter(params.TYPE, "Photo")
                .addMultipartParameter(params.VALUE_OPTION, "Photo")
                .addMultipartParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new JarakKelokasiFragment());
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(), "Opps..Error : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_REQUEST) {
                try {
                    String photoPATH = cameraPhoto.getPhotoPath();
                    file = new File(photoPATH);
                    Bitmap bitmap = ImageLoader.init().from(photoPATH).requestSize(512, 200).getBitmap();
                    viewPhoto.setImageBitmap(bitmap);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextPhoto:
                if (opt.equals("")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else {
                    saveData(params.URL_METHOD.SAVE_AREA_ANALYSIS_IMG);
                }
                break;
            case R.id.viewPhoto:
                try {
                    Intent intent = cameraPhoto.takePhotoIntent();
                    startActivityForResult(intent, CAMERA_REQUEST);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }
}
