package com.ck.it.androidapps.model;

/**
 * Created by Creef on 3/19/2018.
 */

public class ModelGalleryApprove {
    private String SITE_CODE, USER_ID, DOC_NAME, DOCUMENT, created_at, updated_at;

    public ModelGalleryApprove() {
    }

    public ModelGalleryApprove(String SITE_CODE, String USER_ID, String DOC_NAME,
                               String DOCUMENT, String created_at, String updated_at) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.DOC_NAME = DOC_NAME;
        this.DOCUMENT = DOCUMENT;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getDOC_NAME() {
        return DOC_NAME;
    }

    public void setDOC_NAME(String DOC_NAME) {
        this.DOC_NAME = DOC_NAME;
    }

    public String getDOCUMENT() {
        return DOCUMENT;
    }

    public void setDOCUMENT(String DOCUMENT) {
        this.DOCUMENT = DOCUMENT;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
