package com.ck.it.androidapps.fragment.siteanalysis;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 12/21/2017.
 */

@SuppressWarnings("deprecation")
public class UkuranLebarAreaParkirFragment extends Fragment implements View.OnClickListener {
    private View view;
    EditText viewLebarArea;
    RadioGroup viewGroupLebarArea;
    Button viewNextLebarArea;
    String opt = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.ukuran_lebar_parkir, container, false);
        initView();
        return view;
    }

    void initView() {
        viewLebarArea = (EditText) view.findViewById(R.id.viewLebarArea);
        viewGroupLebarArea = (RadioGroup) view.findViewById(R.id.viewGroupLebarArea);
        viewGroupLebarArea.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.la_1:
                        opt = "1";
                        break;
                    case R.id.la_2:
                        opt = "2";
                        break;
                    case R.id.la_3:
                        opt = "3";
                        break;
                    case R.id.la_4:
                        opt = "4";
                        break;
                    case R.id.la_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewNextLebarArea = (Button) view.findViewById(R.id.viewNextLebarArea);
        viewNextLebarArea.setOnClickListener(this);
    }

    void saveData(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.SA_ID, "SA")
                .addBodyParameter(params.TYPE, "Ukuran Lebar Area Parkir")
                .addBodyParameter(params.VALUE_OPTION, viewLebarArea.getText().toString())
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new UkuranPanjangAreaParkirFragment());
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext().getApplicationContext(), "Oppss, Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextLebarArea:
                if (opt.equals("")) {
                    Toast.makeText(view.getContext().getApplicationContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(viewLebarArea.getText().toString())) {
                    viewLebarArea.setError(params.MValues.field_error_msg);
                } else {
                    saveData(params.URL_METHOD.SAVE_SITE_ANALYSIS);
                }
                break;
        }
    }
}
