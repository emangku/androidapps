package com.ck.it.androidapps.fragment.areaanalysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.util.utils;

/**
 * Created by Creef on 12/7/2017.
 */

public class TrafficFlowFragment extends Fragment implements View.OnClickListener {
    private View view;
    Spinner viewKepadatanLaluliantas,viewKepadatanPejalanKaki,vewjenisKendaraanDominant,viewArahLalulintas,viewGoingHomeSide,viewJenisJalan;
    EditText viewModeaTransportPribadi,viewModaTransportUmum,viewTranstAngkutanBarang,viewLebarJalan;
    Button viewNextTrafficFlow;
    String[] kepdatanlalulintas = {
            "-Kepadatan Lalulintas-",
            "Sangat Padat","Padat","Sedang","Jarang","Tidak ada"};
    String[] kepdatanpejalankaki = {
            "-Kepadatan Pejalan Kaki-",
            "Sangat Padat","Padat","Sedang","Jarang","Tidak ada"};
    String[] arahlalulintas = {
            "-Arah Lalu Lintas-",
            "2 arah@1 Jalur","1 arah@ 2 Jalur","2 arah@ 2 jalur",
            "2 arah @ 2 jalur dgn Pembatas","1 Arah@ 2 Jalur dgn Pembatas","2 Arah@ banyak jalur"};
    String[] jeniskendaraandominsai = {
            "-Jenis Kendaraan Dominant-",
            "Mobil","Motor","Angkutan umum"};
    String[] goinghomeside = {
            "-Going Home Side-",
            "Ya","Tidak"};
    String[] jalan = {
            "-Jenis jalan-",
            "By Pass","Arteri","Primary","Secondary","Jalan Lingkungan"};

    ArrayAdapter<String> kllAdapter, kpkAdapter,allAdapter,jkdAdapter,gmsAdapter,jAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.trafficflow_fragment,container,false);
        initView();
        return view;
    }
    void initView(){
        viewNextTrafficFlow = (Button) view.findViewById(R.id.viewNextTrafficFlow);
        viewNextTrafficFlow.setOnClickListener(this);

        viewKepadatanLaluliantas = (Spinner) view.findViewById(R.id.viewKepadatanLaluliantas);
        kllAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner, R.id.txtSpinner,kepdatanlalulintas);
        viewKepadatanLaluliantas.setAdapter(kllAdapter);

        viewKepadatanPejalanKaki = (Spinner) view.findViewById(R.id.viewKepadatanPejalanKaki);
        kpkAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner, R.id.txtSpinner,kepdatanpejalankaki);
        viewKepadatanPejalanKaki.setAdapter(kpkAdapter);

        vewjenisKendaraanDominant = (Spinner) view.findViewById(R.id.vewjenisKendaraanDominant);
        jkdAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner, R.id.txtSpinner,jeniskendaraandominsai);
        vewjenisKendaraanDominant.setAdapter(jkdAdapter);

        viewArahLalulintas = (Spinner) view.findViewById(R.id.viewArahLalulintas);
        allAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner, R.id.txtSpinner,arahlalulintas);
        viewArahLalulintas.setAdapter(allAdapter);

        viewGoingHomeSide = (Spinner) view.findViewById(R.id.viewGoingHomeSide);
        gmsAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner, R.id.txtSpinner,goinghomeside);
        viewGoingHomeSide.setAdapter(gmsAdapter);

        viewJenisJalan = (Spinner) view.findViewById(R.id.viewJenisJalan);
        jAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner, R.id.txtSpinner,jalan);
        viewJenisJalan.setAdapter(jAdapter);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextTrafficFlow:
                utils.showFragmentThree(view.getContext(), new ActivityGeneratorFragment());
                getActivity().setTitle("Activity Generator");
                break;
        }
    }
}
