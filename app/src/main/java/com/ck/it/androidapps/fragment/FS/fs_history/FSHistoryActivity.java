package com.ck.it.androidapps.fragment.FS.fs_history;

import android.app.ProgressDialog;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.ui.RecyclerUIItemDecoration;
import com.ck.it.androidapps.adapter.FSHistoryAdapter;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.model.ModelFSDetails;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("deprecation")
public class FSHistoryActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    RecyclerView viewFSRCLHistory;
    List<ModelFSDetails> list;
    FSHistoryAdapter adapter;
    private SwipeRefreshLayout viewFsRefresh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fshistory);
        init();
    }

    void init(){
        viewFSRCLHistory = (RecyclerView) findViewById(R.id.viewFSRCLHistory);
        viewFSRCLHistory.setLayoutManager(new LinearLayoutManager(FSHistoryActivity.this));
        viewFsRefresh = (SwipeRefreshLayout) findViewById(R.id.viewFsRefresh);
        viewFsRefresh.setOnRefreshListener(this);
        showAllHistoryFs("get_fs");
//        new GetFS().execute((Void)null);
    }

    void showAllHistoryFs(String url){
        final ProgressDialog dialog = utils.showLoadDialog(this, params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.get(api.SERVER_API+url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                list = new ArrayList<ModelFSDetails>();
                                JSONArray array = response.getJSONArray("data");
                                System.out.println(array);
                                for (int i=0; i<array.length();i++){
                                    ModelFSDetails details = new ModelFSDetails();
                                    JSONObject obj = array.getJSONObject(i);
                                    details.setSITE_CODE(obj.getString("SITE_CODE"));
                                    details.setUSER_ID(obj.getString("USER_ID"));
                                    details.setRENT(obj.getString("RENT"));
                                    details.setRENOVATION(obj.getString("RENOVATION"));
                                    details.setEQUIPMENT(obj.getString("EQUIPMENT"));
                                    details.setTOTAL_INVESTMENT(obj.getString("TOTAL_INVESTMENT"));
                                    details.setCreated_at(obj.getString("created_at"));
                                    details.setUpdated_at(obj.getString("updated_at"));
                                    details.setBEST(obj.getString("BEST"));
                                    details.setAVERAGE(obj.getString("AVERAGE"));
                                    details.setWORSE(obj.getString("WORSE"));
                                    details.setGP_STANDARD(obj.getString("GP_STANDARD"));
                                    details.setSTORE_ID(obj.getString("STORE_ID"));
                                    details.setSALES(obj.getString("SALES"));
                                    details.setGP(obj.getString("GP"));
                                    details.setEMPLOYMENT(obj.getString("EMPLOYMENT"));
                                    details.setUTILITY(obj.getString("UTILITY"));
                                    details.setREPAIR_MAINTENANCE(obj.getString("REPAIR_MAINTENANCE"));
                                    details.setSTORE_USE(obj.getString("STORE_USE"));
                                    details.setOTHERS(obj.getString("OTHERS"));
                                    details.setDESCRIPTION(obj.getString("DESCRIPTION"));
                                    details.setNSC(obj.getString("NSC"));
                                    details.setNO_OF_STAFF(obj.getString("NO_OF_STAFF"));
                                    details.setSL(obj.getString("SL"));
                                    details.setCSR(obj.getString("CSR"));
                                    details.setUTILITES(obj.getString("UTILITES"));
                                    details.setOTHER(obj.getString("OTHER"));
                                    list.add(details);
                                }

                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                        adapter = new FSHistoryAdapter(FSHistoryActivity.this,list);
                        viewFSRCLHistory.addItemDecoration(new RecyclerUIItemDecoration(FSHistoryActivity.this,LinearLayoutManager.VERTICAL,0));
                        viewFSRCLHistory.setAdapter(adapter);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(FSHistoryActivity.this,"Opps Error : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });

    }

    @Override
    public void onRefresh() {
        showAllHistoryFs("get_fs");
        viewFsRefresh.setRefreshing(false);
    }
}
