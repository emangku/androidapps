package com.ck.it.androidapps.activity.ui;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by Creef on 1/8/2018.
 */

public class Test {

    public static void main(String[] args){
        String stardates = "01/03/2018";
        String enddates = "05/03/2018";
        getDayFormatCalculate(stardates, enddates);

    }

    public static String getDayFormatCalculate(String startdate, String endate){
        SimpleDateFormat start,end;
        String val = "";
        start = new SimpleDateFormat("dd/MM/yyyy");
        end = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date dateste = start.parse(startdate);
            Date enddate = end.parse(endate);

            long deff = enddate.getTime() - dateste.getTime() ;
            val = String.valueOf(TimeUnit.DAYS.convert(deff,TimeUnit.MILLISECONDS)) + " Hari";
            System.out.println(val);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return val;

    }



}
