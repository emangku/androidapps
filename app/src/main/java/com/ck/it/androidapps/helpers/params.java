package com.ck.it.androidapps.helpers;

/**
 * Created by Creef on 12/5/2017.
 */

public interface params {
    String USERNAME             = "USERNAME";
    String PASSWORD             = "PASSWORD";
    String REGIONAL             = "REGIONAL";
    String ULOGIN               = "uLogin";
    String UPASWD               = "uPaswd";
    //Info Lokasi
    String SITE_CODE            = "SITE_CODE";
    String USER_ID              = "USER_ID";
    String LATITUDE             = "LATITUDE";
    String LONGITUDE            = "LONGITUDE";
    String ALAMAT               = "ALAMAT";
    String DESA                 = "DESA";
    String KECAMATAN            = "KECAMATAN";
    String KABUPATEN            = "KABUPATEN";
    String PROVINSI             = "PROVINSI";
    String KODE_POS             = "KODE_POS";
    String SESSION_KEY          = "3k0_Mobile_Creative_IT";
    String ALAMAT_A             = "ALAMAT_A";
    String ALAMAT_B             = "ALAMAT_B";
    String ALAMAT_C             = "ALAMAT_C";
    String VIDEO                = "VIDEO";

    //info Kontak

    String NAMA_KONTAK              = "NAMA_KONTAK";
    String NOMOR_TELEPON            = "NOMOR_TELEPON";
    String K_ALAMAT                 = "ALAMAT";
    String HUBUNGAN_DENGAN_PEMILIK  = "HUBUNGAN_DENGAN_PEMILIK";

    // Info Kepemilikan

    String NAMA_PEMILIK             = "NAMA_PEMILIK";
    String BADAN_HUKUM              = "BADAN_HUKUM";
    String O_ALAMAT                 = "ALAMAT";
    String NOTLP_OR_FAX_OR_NOHP     = "NOTLP_OR_FAX_OR_NOHP";
    String EMAIL                    = "EMAIL";
    String S_EMAIL                  = "S_EMAIL";

    //info sewa

    String RENCANA_KERJASAMA        = "RENCANA_KERJASAMA";
    String HARGA_PENAWARAN_SEWA     = "HARGA_PENAWARAN_SEWA";
    String PERIODE_SEWA_START       = "PERIODE_SEWA_START";
    String PERIODE_SEWA_END         = "PERIODE_SEWA_END";
    String GRACE_PERIOD             = "GRACE_PERIOD";
    String STATUS_HARGA             = "STATUS_HARGA";
    String SERVICE_CHARGE           = "SERVICE_CHARGE";
    String BIAYA_PAJAK              = "BIAYA_PAJAK";
    String BIAYA_NOTARIS            = "BIAYA_NOTARIS";

    //info logalitas

    String SERTIFIKASI              = "SERTIFIKASI";
    String ATAS_NAMA                = "ATAS_NAMA";
    String LUAS_TANAH               = "LUAS_TANAH";
    String IJIN_MENDIRIKAN_BANGUNAN = "IJIN_MENDIRIKAN_BANGUNAN";
    String AA_ID                    = "AA_ID";
    String SA_ID                    = "SA_ID";
    String TYPE                     = "TYPE";
    String VALUE_OPTION             = "VALUE_OPTION";
    String VALUE_RANGE              = "VALUE_RANGE";

    String TIPE_BANGUNAN            = "TIPE_BANGUNAN";
    String POSISI_BANGUNAN          = "POSISI_BANGUNAN";
    String TAHUN_PEMBUATAN          = "TAHUN_PEMBUATAN";
    String UKURAN_LOKASI            = "UKURAN_LOKASI";
    String UKURAN_BANGUNAN          = "UKURAN_BANGUNAN";
    String LEBAR_MUKA_BANGUNAN      = "LEBAR_MUKA_BANGUNAN";
    String PANJANG_BANGUNAN         = "PANJANG_BANGUNAN";

    String CREATED_AT               = "created_at";
    String UPDATED_AT               = "updated_at";

    String RENT                     = "RENT";
    String RENOVATION               = "RENOVATION";
    String EQUIPMENT                = "EQUIPMENT";
    String TOTAL_INVESTMENT         = "TOTAL_INVESTMENT";

    String BEST                     = "BEST";
    String AVERAGE                  = "AVERAGE";
    String WORSE                    = "WORSE";
    String GP_STANDARD              = "GP_STANDARD";

    String NO_OF_STAFF              = "NO_OF_STAFF";
    String SL                       = "SL";
    String CSR                      = "CSR";
    String UTILITIES                = "UTILITIES";
    String REPAIR_MAINTENANCE       = "REPAIR_MAINTENANCE";
    String STORE_USE                = "STORE_USE";
    String OTHERS                   = "OTHERS";

    String STORE_ID                 = "STORE_ID";
    String SALES                    = "SALES";
    String GP                       = "GP";
    String EMPLOYMENT               = "EMPLOYMENT";
    String UTILITY                  = "UTILITY";
    String DEPRECIATION             = "DEPRECIATION";
    String NSC                      = "NSC";
    String IMAGE                    = "IMAGE";
    String DOCUMENT                 = "DOCUMENT";
    String DOC_NAME                 = "DOC_NAME";


    interface URL_METHOD{
        String LOGIN                    = "login";
        String SAVE_USER                = "saveuser";
        String SAVE_INFO_LOKASI         = "saveinfolokasi";
        String SAVE_INFO_KONTAK         = "saveinfokontak";
        String SAVE_INFO_KEPEMILIKAN    = "saveinfokepemilikan";
        String SAVE_INFO_SEWA           = "saveinfosewa";
        String SAVE_LEGALITAS           = "saveinfolegal";
        String GET_SITE_INFO            = "getsiteinfo";
        String SAVE_SITUASI_BANGUNAN    = "savesituasibangunan";
        String SAVE_SITE_ANALYSIS       = "savesiteanalysis";
        String SAVE_AREA_ANALYSIS       = "saveareaanalysis";
        String SAVE_AREA_ANALYSIS_IMG   = "saveareaanalysis_img";
        String GET_SITE                 = "get_site";
        String GET_SITE_A               = "get_sitea";
        String GET_AREA                 = "get_area";
        String GET_AREA_IMG             = "get_area_img";
        String GET_MAPPING              = "mapping";
        String COBA                     = "coba";
        String AUTH_PANEL               = "authpanel";
        String SIMPAN                   = "authpanel";
        String HISTORY                  = "history";
        String TEST                     = "test";
        String SAVE_DATA_MASTER         = "savedatamaster";
        String GET_MDATA                = "get_mdata";
        String SAVE_INVESTASI           = "saveinvestasi";
        String GET_INVESTASI            = "get_investasi";
        String SAVE_INCOME              = "saveincomming";
        String GET_INCOME               = "get_income";
        String SAVE_STORE_COST          = "savestorecost";
        String GET_STORE_COST           = "get_storecost";
        String SAVE_BANCHMARK           = "savebenchmark";
        String GET_BANCHMARK            = "get_benchmark";
        String CALCULATE                = "calculate";
        String VUPLOADS                 = "vuploads";
        String SAVE_MAPPING_AREA        = "save_mapping_area";
        String GET_HISTORY              = "get_history";

    }
    interface MValues{
        String loading_msg              = "Tunggu...";
        String uploading_msg            = "Uploading...";
        String field_error_msg          = "Tidak boleh kosong !";
        String option_msg               = "silahkan Pilih...!!";
        String toast_error_msg          = "Oppss, Error";
    }
}
