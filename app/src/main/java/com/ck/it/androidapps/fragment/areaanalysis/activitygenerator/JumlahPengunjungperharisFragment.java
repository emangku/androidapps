package com.ck.it.androidapps.fragment.areaanalysis.activitygenerator;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eko on 04/01/18.
 */

@SuppressWarnings("deprecation")
public class JumlahPengunjungperharisFragment extends Fragment implements View.OnClickListener {
    private View view;
    EditText viewJumlahPengunjungPerHaris;
    RadioGroup viewGroupJumlahPengunjungPerHaris;
    Button viewNextJumlahPengunjungPerHaris;
    String opt = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.jumlah_pengunjung_perhari_fragment, container, false);
        initView();
        return view;
    }

    void initView() {
        viewJumlahPengunjungPerHaris = (EditText) view.findViewById(R.id.viewJumlahPengunjungPerHaris);
        viewGroupJumlahPengunjungPerHaris = (RadioGroup) view.findViewById(R.id.viewGroupJumlahPengunjungPerHaris);
        viewGroupJumlahPengunjungPerHaris.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.jpphs_1:
                        opt = "1";
                        Toast.makeText(view.getContext(), opt, Toast.LENGTH_LONG).show();
                        break;
                    case R.id.jpphs_2:
                        opt = "2";
                        break;
                    case R.id.jpphs_3:
                        opt = "3";
                        break;
                    case R.id.jpphs_4:
                        opt = "4";
                        break;
                    case R.id.jpphs_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewNextJumlahPengunjungPerHaris = (Button) view.findViewById(R.id.viewNextJumlahPengunjungPerHaris);
        viewNextJumlahPengunjungPerHaris.setOnClickListener(this);
    }

    void saveData(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.AA_ID, "AA")
                .addBodyParameter(params.TYPE, "Jumlah Penunjung perhari")
                .addBodyParameter(params.VALUE_OPTION, viewJumlahPengunjungPerHaris.getText().toString())
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new JamOperasisFragment());
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(), "Opps Error : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextJumlahPengunjungPerHaris:
                if (opt.equals("")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(viewJumlahPengunjungPerHaris.getText().toString())) {
                    viewJumlahPengunjungPerHaris.setError(params.MValues.field_error_msg);
                } else {
                    saveData(params.URL_METHOD.SAVE_AREA_ANALYSIS);
                }
                break;
        }
    }
}
