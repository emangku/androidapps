package com.ck.it.androidapps.fragment.areaanalysis.kompetitor;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eko on 04/01/18.
 */

@SuppressWarnings("deprecation")
public class JarakKelokasiFragment extends Fragment implements View.OnClickListener {
    private View view;
    Button viewNextJarakKeLokasi;
    RadioGroup viewGroupJarakKeLokasi;
    EditText viewJarakKeLokasi;
    String opt = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.karakkelokasi_fragment, container, false);
        initView();
        return view;
    }

    void initView() {
        viewJarakKeLokasi = (EditText) view.findViewById(R.id.viewJarakKeLokasi);
        viewGroupJarakKeLokasi = (RadioGroup) view.findViewById(R.id.viewGroupJarakKeLokasi);
        viewGroupJarakKeLokasi.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.jkl_1:
                        opt = "1";
                        Toast.makeText(view.getContext(), opt, Toast.LENGTH_LONG).show();
                        break;
                    case R.id.jkl_2:
                        opt = "2";
                        break;
                    case R.id.jkl_3:
                        opt = "3";
                        break;
                    case R.id.jkl_4:
                        opt = "4";
                        break;
                    case R.id.jkl_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewNextJarakKeLokasi = (Button) view.findViewById(R.id.viewNextJarakKeLokasi);
        viewNextJarakKeLokasi.setOnClickListener(this);
    }

    void saveData(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
//                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
//                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.AA_ID, "AA")
                .addBodyParameter(params.TYPE, "Jarak Ke Lokasi")
                .addBodyParameter(params.VALUE_OPTION, viewJarakKeLokasi.getText().toString())
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new KelasSosialFragment());
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(), "Opps..Error : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextJarakKeLokasi:
                if (opt.equals("")) {
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (TextUtils.isEmpty(viewJarakKeLokasi.getText().toString())) {
                    viewJarakKeLokasi.setError(params.MValues.field_error_msg);
                } else {
                    saveData(params.URL_METHOD.SAVE_AREA_ANALYSIS);
                }
                break;
        }
    }
}
