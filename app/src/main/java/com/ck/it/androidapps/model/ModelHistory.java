package com.ck.it.androidapps.model;

/**
 * Created by Creef on 2/7/2018.
 */

public class ModelHistory {
    String SITE_CODE, USER_ID,
            LATITUDE, LONGITUDE,
            ALAMAT_A, DESA,
            KECAMATAN, KABUPATEN,
            PROVINSI, KODE_POS,
            created_at, updated_at;

    public ModelHistory() {
    }

    public ModelHistory(String SITE_CODE, String USER_ID,
                        String LATITUDE, String LONGITUDE,
                        String ALAMAT_A, String DESA,
                        String KECAMATAN, String KABUPATEN,
                        String PROVINSI, String KODE_POS,
                        String created_at, String updated_at) {
        this.SITE_CODE = SITE_CODE;
        this.USER_ID = USER_ID;
        this.LATITUDE = LATITUDE;
        this.LONGITUDE = LONGITUDE;
        this.ALAMAT_A = ALAMAT_A;
        this.DESA = DESA;
        this.KECAMATAN = KECAMATAN;
        this.KABUPATEN = KABUPATEN;
        this.PROVINSI = PROVINSI;
        this.KODE_POS = KODE_POS;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public String getSITE_CODE() {
        return SITE_CODE;
    }

    public void setSITE_CODE(String SITE_CODE) {
        this.SITE_CODE = SITE_CODE;
    }

    public String getUSER_ID() {
        return USER_ID;
    }

    public void setUSER_ID(String USER_ID) {
        this.USER_ID = USER_ID;
    }

    public String getLATITUDE() {
        return LATITUDE;
    }

    public void setLATITUDE(String LATITUDE) {
        this.LATITUDE = LATITUDE;
    }

    public String getLONGITUDE() {
        return LONGITUDE;
    }

    public void setLONGITUDE(String LONGITUDE) {
        this.LONGITUDE = LONGITUDE;
    }

    public String getALAMAT_A() {
        return ALAMAT_A;
    }

    public void setALAMAT_A(String ALAMAT_A) {
        this.ALAMAT_A = ALAMAT_A;
    }

    public String getDESA() {
        return DESA;
    }

    public void setDESA(String DESA) {
        this.DESA = DESA;
    }

    public String getKECAMATAN() {
        return KECAMATAN;
    }

    public void setKECAMATAN(String KECAMATAN) {
        this.KECAMATAN = KECAMATAN;
    }

    public String getKABUPATEN() {
        return KABUPATEN;
    }

    public void setKABUPATEN(String KABUPATEN) {
        this.KABUPATEN = KABUPATEN;
    }

    public String getPROVINSI() {
        return PROVINSI;
    }

    public void setPROVINSI(String PROVINSI) {
        this.PROVINSI = PROVINSI;
    }

    public String getKODE_POS() {
        return KODE_POS;
    }

    public void setKODE_POS(String KODE_POS) {
        this.KODE_POS = KODE_POS;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }
}
