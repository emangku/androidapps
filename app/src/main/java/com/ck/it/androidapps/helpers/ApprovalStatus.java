package com.ck.it.androidapps.helpers;

/**
 * Created by Creef on 12/19/2017.
 */

public enum ApprovalStatus {
    APPROVAL,
    REJECT,
    HOLD,
    INACTIVE,
    ACTIVE
}

//
//<item
//            android:id="@+id/projectplaning"
//                    android:icon="@drawable/ic_doc"
//                    android:title="PROJECT PLANING" />
//<item
//            android:id="@+id/project"
//                    android:icon="@drawable/ic_doc"
//                    android:title="PROJECT" />
//<item
//            android:id="@+id/monitoring"
//                    android:icon="@drawable/ic_doc"
//                    android:title="MONOTORING" />
//<item
//            android:id="@+id/photo"
//                    android:icon="@drawable/ic_doc"
//                    android:title="PHOTO DOCUMENT" />
