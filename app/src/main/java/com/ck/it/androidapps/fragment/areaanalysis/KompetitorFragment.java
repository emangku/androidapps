package com.ck.it.androidapps.fragment.areaanalysis;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.gpstracker.GpsTracker;
import com.ck.it.androidapps.util.utils;
import com.kosalgeek.android.photoutil.CameraPhoto;
import com.kosalgeek.android.photoutil.ImageLoader;

import java.io.FileNotFoundException;
import java.io.IOException;

import es.dmoral.toasty.Toasty;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Creef on 12/7/2017.
 */

public class KompetitorFragment extends Fragment implements View.OnClickListener {
    private View view;
    Spinner viewKelasSosialKompetitor,viewJalanOperasiKompetitor,viewHariBeroperasiKompetitor;
    ArrayAdapter<String> ksAdapter, joAdapter,hbAdapter;
    String[] kelassosial = {
            "-Kelas Sosial-",
            "Low","Middle","High"};
    String[] jamoperasi = {
            "-Jam Operasi ( Jam )-",
            "8","12","24"};
    String[] hariberoperasi = {
            "-Hari Beroperasi-",
            " S","S","R","K","J","S","M"};
    Button viewNextKompetitor;
    ImageView viewCaptureKompetitor;
    CameraPhoto cameraPhoto;
    int CAMERA_REQUEST = 1100;
    double latitude, longitude;
    String watermark = "";
    GpsTracker gps;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.kompetitor_fragment,container,false);
        initView();
        return view;
    }

    void initView(){
        new AlertDialog.Builder(view.getContext())
                .setTitle("Note")
                .setIcon(R.drawable.ic_info)
                .setView(R.layout.info_kompetitor)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                }).show();
        viewCaptureKompetitor = (ImageView) view.findViewById(R.id.viewCaptureKompetitor);
        viewCaptureKompetitor.setOnClickListener(this);
        cameraPhoto = new CameraPhoto(view.getContext().getApplicationContext());

        viewNextKompetitor = (Button) view.findViewById(R.id.viewNextKompetitor);
        viewNextKompetitor.setOnClickListener(this);

        viewKelasSosialKompetitor = (Spinner) view.findViewById(R.id.viewKelasSosialKompetitor);
        ksAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner,R.id.txtSpinner,kelassosial);
        viewKelasSosialKompetitor.setAdapter(ksAdapter);

        viewJalanOperasiKompetitor = (Spinner) view.findViewById(R.id.viewJalanOperasiKompetitor);
        joAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner,R.id.txtSpinner,jamoperasi);
        viewJalanOperasiKompetitor.setAdapter(joAdapter);

        viewHariBeroperasiKompetitor = (Spinner) view.findViewById(R.id.viewHariBeroperasiKompetitor);
        hbAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner,R.id.txtSpinner,hariberoperasi);
        viewHariBeroperasiKompetitor.setAdapter(hbAdapter);
        gps = new GpsTracker(view.getContext());
        if ( gps.canGetLocation() ){
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
        }else{
            gps.showSettingAlerts();
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextKompetitor:
                break;
            case R.id.viewCaptureKompetitor:
                try {
                    Intent intent = cameraPhoto.takePhotoIntent();
                    startActivityForResult(intent,CAMERA_REQUEST);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK){
            if (requestCode==CAMERA_REQUEST){
                String photoPath = cameraPhoto.getPhotoPath();
                try{
                    watermark = String.valueOf(latitude+"_"+longitude);
                    Toasty.warning(view.getContext(),"OBearhsail"+watermark, Toast.LENGTH_LONG).show();
                    Bitmap bitmap = ImageLoader.init().from(photoPath).requestSize(512,200).getBitmap();
                    Bitmap bitmap1 = utils.addWaterMark(view.getContext(),bitmap,watermark,0,0, Color.WHITE,80, 12,false);
                    viewCaptureKompetitor.setImageBitmap(bitmap1);
                }catch (FileNotFoundException e){
                    e.printStackTrace();
                }
            }else{
                Toasty.warning(view.getContext(),"Opps,.."+cameraPhoto.getPhotoPath(), Toast.LENGTH_LONG).show();
            }
        }
    }
}
