package com.ck.it.androidapps.fragment.siteanalysis;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 12/20/2017.
 */

@SuppressWarnings("deprecation")
public class BahanDindingFragment extends Fragment implements View.OnClickListener {
    private View view;
    String[] bahandinding = {"-Bahan Dinding-", "Bata", "Kayu", "Seng"};
    Spinner viewBahanDinding;
    ArrayAdapter<String> bdAdapter;
    String opt = "";
    String type_opt = "";
    RadioGroup optBaganDinding;
    Button viewNextBahanDinding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bahan_dinding, container, false);
        initView();
        return view;
    }

    void initView() {
        viewBahanDinding = (Spinner) view.findViewById(R.id.viewBahanDinding);
        bdAdapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, bahandinding);
        viewBahanDinding.setAdapter(bdAdapter);
        viewBahanDinding.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type_opt = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        optBaganDinding = (RadioGroup) view.findViewById(R.id.optBaganDinding);
        optBaganDinding.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.bd_1:
                        opt = "1";
                        break;
                    case R.id.bd_2:
                        opt = "2";
                        break;
                    case R.id.bd_3:
                        opt = "3";
                        break;
                    case R.id.bd_4:
                        opt = "4";
                        break;
                    case R.id.bd_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewNextBahanDinding = (Button) view.findViewById(R.id.viewNextBahanDinding);
        viewNextBahanDinding.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextBahanDinding:
                if (type_opt.equals("") || type_opt.equals("-Bahan Dinding-")) {
                    Toast.makeText(view.getContext().getApplicationContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else if (opt.equals("")) {
                    Toast.makeText(view.getContext().getApplicationContext(), params.MValues.option_msg, Toast.LENGTH_SHORT).show();
                } else {
                    saveBaganDinding(params.URL_METHOD.SAVE_SITE_ANALYSIS);
                }
//                getActivity().setTitle("Bahan Lantai");
//                utils.showFragment(view.getContext(),new BahanLantaiFragment());
                break;
        }
    }

    void saveBaganDinding(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.SA_ID, "SA-KB")
                .addBodyParameter(params.TYPE, "Bahan Dinding")
                .addBodyParameter(params.VALUE_OPTION, type_opt)
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                utils.showFragment(view.getContext(), new BahanLantaiFragment());

                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext().getApplicationContext(), "Oppss, Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
