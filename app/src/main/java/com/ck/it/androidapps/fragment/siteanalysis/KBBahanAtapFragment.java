package com.ck.it.androidapps.fragment.siteanalysis;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

@SuppressWarnings("deprecation")
public class KBBahanAtapFragment extends Fragment implements View.OnClickListener {
    private View view;
    String[] bahanatap = {"-Bahan Atap-","Genteng","Seng","Asbes"};
    Spinner viewBahanAtap;
    ArrayAdapter<String> baAdapter;
    RadioGroup optBaganAtap;
    Button viewNextBaganAtap;
    String opt = "";
    String type_opt = "";
    ProgressBar progressBar;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.bahan_atap, container, false);
        getActivity().setTitle(R.string.kondisi_bangunan);
        initView();
        return view;
    }

    void initView(){
        viewNextBaganAtap = (Button) view.findViewById(R.id.viewNextBaganAtap);
        viewNextBaganAtap.setOnClickListener(this);
        viewBahanAtap = (Spinner)view.findViewById(R.id.viewBahanAtap);
        baAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,bahanatap);
        viewBahanAtap.setAdapter(baAdapter);
        viewBahanAtap.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type_opt = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        optBaganAtap = (RadioGroup) view.findViewById(R.id.optBaganAtap);
        optBaganAtap.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId){
                    case R.id.ba_1:
                        opt = "1";
                        break;
                    case R.id.ba_2:
                        opt = "2";
                        break;
                    case R.id.ba_3:
                        opt = "3";
                        break;
                    case R.id.ba_4:
                        opt = "4";
                        break;
                    case R.id.ba_5:
                        opt = "5";
                        break;
                }
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextBaganAtap:
                if (type_opt.equals("") || type_opt.equals("-Bahan Atap-")){
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_LONG).show();
                }else if (opt==""){
                    Toast.makeText(view.getContext(), params.MValues.option_msg, Toast.LENGTH_LONG).show();
                }else {
                    saveBahanAtap(params.URL_METHOD.SAVE_SITE_ANALYSIS);
                }
                break;
        }
    }

    void saveBahanAtap(final String url){
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API+url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE,utils.getDataSession(view.getContext(),"sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.SA_ID, "SA")
                .addBodyParameter(params.TYPE, "Bahan Atap")
                .addBodyParameter(params.VALUE_OPTION, type_opt)
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")){
                                dialog.dismiss();
                                utils.showFragment(view.getContext(),new BahanDindingFragment());
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext(),"Opps Error : "+anError.getMessage(),Toast.LENGTH_SHORT).show();
                    }
                });
    }
}
