package com.ck.it.androidapps;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.ck.it.androidapps.helpers.WebServiceHelper;


public class Apps extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        WebServiceHelper.initialize();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
