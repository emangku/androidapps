package com.ck.it.androidapps.fragment.history;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.ui.RecyclerUIItemDecoration;
import com.ck.it.androidapps.adapter.SiteAnalysisAdapter;
import com.ck.it.androidapps.helpers.WebServiceHelper;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.model.siteanalysismodel.SiteAnalysisModel;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.RequestBody;

/**
 * Created by Creef on 1/16/2018.
 */

@SuppressWarnings("deprecation")
public class SiteAnalysisFragments extends Fragment implements SwipeRefreshLayout.OnRefreshListener,View.OnClickListener {
    private View view;
    String vTipe = "";
    String vPosisi = "";
    private static SiteAnalysisFragments analysisFragments;
    TextView viewTipeBangunan, viewPosisiBangunan,
            viewTahunPembuatan, viewUkuranlokasi,
            viewUkuranbangunan, viewLebarMukaBangunan,
            viewPanjangBangunan, viewBahanAtap,updateSiteAnalysis,
            viewBahanDinding, viewBahanLantai,
            viewHalaman, viewKapasitasListrik,
            viewJaringanAir, viewJaringanTelepon,
            viewToilet, viewTypeToilet,
            viewUkuranLebarAreaParkir, viewUkuranPanjangAreaParkir,
            viewKapasitasParkir, viewKualitasAkses,
            viewLebarTrotoar, viewKemiringanLokasi,
            viewSebelahKiri, viewSebelahKanan,
            viewVisibilityBangunanDariSisiKiri, viewVisibilityBangunanDariSisiKanan,
            viewKemudahanMenempatkanSign, viewPenghalangBangunan, viewPembatas_Barrier;
    SiteAnalysisAdapter adapter;
    RecyclerView recyclerView;
    List<SiteAnalysisModel> list;
    private SwipeRefreshLayout viewRefreshSiteA;
    ProgressDialog dialog;
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.site_analyisi_fragment, container, false);
        initView();
        return view;
    }

    void initView() {
        viewTipeBangunan = (TextView) view.findViewById(R.id.viewTipeBangunan);
        viewPosisiBangunan = (TextView) view.findViewById(R.id.viewPosisiBangunan);
        viewTahunPembuatan = (TextView) view.findViewById(R.id.viewTahunPembuatan);
        viewUkuranlokasi = (TextView) view.findViewById(R.id.viewUkuranlokasi);
        viewUkuranbangunan = (TextView) view.findViewById(R.id.viewUkuranbangunan);
        viewLebarMukaBangunan = (TextView) view.findViewById(R.id.viewLebarMukaBangunan);
        viewPanjangBangunan = (TextView) view.findViewById(R.id.viewPanjangBangunan);
        recyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
        viewRefreshSiteA = (SwipeRefreshLayout) view.findViewById(R.id.viewRefreshSiteA);
        viewRefreshSiteA.setOnRefreshListener(this);
        updateSiteAnalysis = (TextView)view.findViewById(R.id.updateSiteAnalysis);
        updateSiteAnalysis.setOnClickListener(this);
        if (!utils.getDataSession(view.getContext(),"sessionSTATUS").equals("MAKER")){
            updateSiteAnalysis.setVisibility(View.GONE);
        }
        showSiteAnalysis(params.URL_METHOD.GET_SITE_A);
//        new ShoData().execute((Void)null);
    }

    void showSiteAnalysis(String url) {
        dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionValKeySiteCode"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @SuppressLint("SetTextI18n")
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                if (!response.getJSONObject("data").getString("TIPE_BANGUNAN").equals("null")) {
                                    viewTipeBangunan.setText(response.getJSONObject("data").getString("TIPE_BANGUNAN"));
                                }
                                if (!response.getJSONObject("data").getString("POSISI_BANGUNAN").equals("null")) {
                                    viewPosisiBangunan.setText(response.getJSONObject("data").getString("POSISI_BANGUNAN"));
                                }
                                if (!response.getJSONObject("data").getString("TAHUN_PEMBUATAN").equals("null")) {
                                    viewTahunPembuatan.setText(response.getJSONObject("data").getString("TAHUN_PEMBUATAN"));
                                }
                                if (!response.getJSONObject("data").getString("UKURAN_LOKASI").equals("null")) {
                                    viewUkuranlokasi.setText(response.getJSONObject("data").getString("UKURAN_LOKASI") + " M");
                                }
                                if (!response.getJSONObject("data").getString("UKURAN_BANGUNAN").equals("null")) {
                                    viewUkuranbangunan.setText(response.getJSONObject("data").getString("UKURAN_BANGUNAN") + " M");
                                }
                                if (!response.getJSONObject("data").getString("LEBAR_MUKA_BANGUNAN").equals("null")) {
                                    viewLebarMukaBangunan.setText(response.getJSONObject("data").getString("LEBAR_MUKA_BANGUNAN") + " M");
                                }
                                if (!response.getJSONObject("data").getString("PANJANG_BANGUNAN").equals("null")) {
                                    viewPanjangBangunan.setText(response.getJSONObject("data").getString("PANJANG_BANGUNAN") + " M");
                                }
                                String res = response.getJSONObject("data").toString();
                                JSONObject mObj = new JSONObject(res);
                                System.out.println(mObj.getString("sa_data_by_site_code"));
                                JSONArray mArr = mObj.getJSONArray("sa_data_by_site_code");
                                list = new ArrayList<>();
                                for (int i = 0; i < mArr.length(); i++) {
                                    SiteAnalysisModel model = new SiteAnalysisModel();
                                    JSONObject Obj = mArr.getJSONObject(i);
                                    model.setID(Obj.getString("id"));
                                    model.setSITE_CODE(Obj.getString("SITE_CODE"));
                                    model.setUSER_ID(Obj.getString("USER_ID"));
                                    model.setSA_ID(Obj.getString("SA_ID"));
                                    model.setTYPE(Obj.getString("TYPE"));
                                    model.setVALUE_OPTION(Obj.getString("VALUE_OPTION"));
                                    model.setVALUE_RANGE(Obj.getString("VALUE_RANGE"));
                                    list.add(model);
                                    System.out.println("COBA :" + model.getVALUE_OPTION());
                                }
                            }//085780981966 / lulu
                            dialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println(e.getMessage());
                        }
                        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
                        adapter = new SiteAnalysisAdapter(view.getContext(), list);
                        recyclerView.addItemDecoration(new RecyclerUIItemDecoration(view.getContext(),LinearLayoutManager.VERTICAL,0));
                        recyclerView.setAdapter(adapter);
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(), "Oppss Error... Site An : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                    }
                });
    }

    @Override
    public void onRefresh() {
        showSiteAnalysis(params.URL_METHOD.GET_SITE_A);
        viewRefreshSiteA.setRefreshing(false);
    }

    void updateData(){
        String stts = "hide";
        String tipe = viewTipeBangunan.getText().toString();
        String posisi = viewPosisiBangunan.getText().toString();
        final View views = LayoutInflater.from(view.getContext()).inflate(R.layout.status_bangunan, null,false);
        final RadioGroup viewTipeBangunan,viewPosisiBangunan;
        RadioButton vRumahTinggal,vRuko,vMidBlock,vCorner;

        viewTipeBangunan = (RadioGroup)views.findViewById(R.id.viewTipeBangunan);
        viewTipeBangunan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId){
                    case R.id.vRuko:
                        vTipe = "Ruko";
                        break;
                    case R.id.vRumahTinggal:
                        vTipe = "Rumah Tinggal";
                        break;
                }
            }
        });

        viewPosisiBangunan = (RadioGroup)views.findViewById(R.id.viewPosisiBangunan);
        viewPosisiBangunan.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId){
                    case R.id.vCorner:
                        vPosisi = "Corner";
                        break;
                    case R.id.vMidBlock:
                        vPosisi = "Mid Block";
                        break;
                }
            }
        });

        vRumahTinggal = (RadioButton) views.findViewById(R.id.vRumahTinggal);
        vRuko = (RadioButton) views.findViewById(R.id.vRuko);
        vMidBlock = (RadioButton) views.findViewById(R.id.vMidBlock);
        vCorner = (RadioButton) views.findViewById(R.id.vCorner);

        if (tipe.equals("Ruko")){
            vRuko.setChecked(true);
        }
        if (tipe.equals("Rumah tinggal")){
            vRumahTinggal.setChecked(true);
        }

        if(posisi.equals("Corner")){
            vCorner.setChecked(true);
        }
        if(posisi.equals("Mid Block")){
            vMidBlock.setChecked(true);
        }


        final EditText viewTahunPembuatans,viewUkuranLokasis,viewUkuranBangunans,
                viewLebarMukaJalans,viewPanjangBangunans;
        Button viewNextStatusBangunan;
        LinearLayout viewEdit,viewbatal,viewUpdates;
        viewEdit = (LinearLayout)views.findViewById(R.id.viewEdit);
        viewbatal = (LinearLayout)views.findViewById(R.id.viewbatal);
        viewUpdates = (LinearLayout)views.findViewById(R.id.viewUpdates);
        viewNextStatusBangunan = (Button) views.findViewById(R.id.viewNextStatusBangunan);
        if (stts.equals("hide")){
            viewNextStatusBangunan.setVisibility(View.GONE);
            viewEdit.setVisibility(View.VISIBLE);
        }else if (stts.equals("") || !stts.equals("")){
            viewNextStatusBangunan.setVisibility(View.VISIBLE);
            viewEdit.setVisibility(View.GONE);
        }

        viewTahunPembuatans = (EditText) views.findViewById(R.id.viewTahunPembuatan);
        viewTahunPembuatans.setText(viewTahunPembuatan.getText().toString());
        viewUkuranLokasis = (EditText) views.findViewById(R.id.viewUkuranLokasi);
        viewUkuranLokasis.setText(viewUkuranlokasi.getText().toString());
        viewUkuranBangunans = (EditText) views.findViewById(R.id.viewUkuranBangunan);
        viewUkuranBangunans.setText(viewUkuranbangunan.getText().toString());
        viewLebarMukaJalans = (EditText) views.findViewById(R.id.viewLebarMukaJalan);
        viewLebarMukaJalans.setText(viewLebarMukaBangunan.getText().toString());
        viewPanjangBangunans = (EditText) views.findViewById(R.id.viewPanjangBangunan);
        viewPanjangBangunans.setText(viewPanjangBangunan.getText().toString());


        final AlertDialog dialogs = utils.customeAlertInputDialog(view.getContext(),views);
        viewbatal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogs.cancel();
            }
        });
        viewUpdates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AndroidNetworking.post(api.SERVER_API+"")
                        .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                        .addBodyParameter(params.SITE_CODE,utils.getDataSession(views.getContext(),"sessionValKeySiteCode"))
                        .addBodyParameter(params.USER_ID,utils.getDataSession(views.getContext(),"sessionUserID"))
                        .addBodyParameter(params.TIPE_BANGUNAN, vTipe)
                        .addBodyParameter(params.POSISI_BANGUNAN, vPosisi)
                        .addBodyParameter(params.TAHUN_PEMBUATAN, viewTahunPembuatans.getText().toString())
                        .addBodyParameter(params.UKURAN_LOKASI,viewUkuranLokasis.getText().toString())
                        .addBodyParameter(params.UKURAN_BANGUNAN,viewUkuranBangunans.getText().toString())
                        .addBodyParameter(params.LEBAR_MUKA_BANGUNAN,viewLebarMukaJalans.getText().toString())
                        .addBodyParameter(params.PANJANG_BANGUNAN,viewPanjangBangunans.getText().toString())
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getString("status").equals("true")){
                                        dialogs.cancel();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialogs.cancel();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Toast.makeText(views.getContext(),"Opps Error : "+anError.getMessage(),Toast.LENGTH_LONG).show();
                                dialogs.cancel();
                            }
                        });
            }
        });
        dialogs.show();
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.updateSiteAnalysis:
                updateData();
                break;
        }
    }

    class ShoData extends AsyncTask<Void, Void, String> {
        ProgressDialog dialog = utils.showLoadDialog(view.getContext(), "Tunggu...");
        String SITE_CODE = utils.getDataSession(view.getContext(), "sessionSiteCode");
        String response = "";
        String status = "";
        JSONObject mObject;
        JSONArray mArray;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
        }

        @Override
        protected String doInBackground(Void... param) {
            RequestBody body = new FormBody.Builder()
                    .add(params.SITE_CODE, "BDMS01180008")
                    .build();
            try {
                response = WebServiceHelper.doPOST(api.SERVER_API + "get_sitea", body, "");
                mObject = new JSONObject(response);
                status = "OK";
            } catch (Exception e) {
                status = "NOK";
                e.printStackTrace();
            }

            return status;
        }

        @Override
        protected void onPostExecute(String status) {
            super.onPostExecute(status);
            if (status.equals("OK")) {
                dialog.dismiss();
                try {
                    JSONObject object = mObject.getJSONObject("data");
                    System.out.println("JOSN OBJECT :" + object.getString("SITE_CODE"));
                    JSONArray sa_data_by_site_code = object.getJSONArray("sa_data_by_site_code");
                    list = new ArrayList<>();
                    for (int i = 0; i < sa_data_by_site_code.length(); i++) {
                        SiteAnalysisModel model = new SiteAnalysisModel();
                        JSONObject Obj = sa_data_by_site_code.getJSONObject(i);
                        model.setID(Obj.getString("id"));
                        model.setSITE_CODE(Obj.getString("SITE_CODE"));
                        model.setUSER_ID(Obj.getString("USER_ID"));
                        model.setSA_ID(Obj.getString("SA_ID"));
                        model.setTYPE(Obj.getString("TYPE"));
                        model.setVALUE_OPTION(Obj.getString("VALUE_OPTION"));
                        model.setVALUE_RANGE(Obj.getString("VALUE_RANGE"));
                        list.add(model);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                adapter = new SiteAnalysisAdapter(view.getContext(), list);
                recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
                recyclerView.setAdapter(adapter);
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (list != null){

        }
    }

    public static SiteAnalysisFragments getInstance() {
        return analysisFragments;
    }
}
