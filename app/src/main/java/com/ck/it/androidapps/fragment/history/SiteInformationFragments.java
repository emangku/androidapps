package com.ck.it.androidapps.fragment.history;

import android.app.Activity;
import android.app.ProgressDialog;
import android.icu.math.BigDecimal;
import android.icu.text.NumberFormat;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.Format;
import java.util.Locale;

/**
 * Created by Creef on 1/16/2018.
 */

@SuppressWarnings("deprecation")
public class SiteInformationFragments extends Fragment implements SwipeRefreshLayout.OnRefreshListener, View.OnClickListener {
    private View view;
    TextView viewID, viewLocations, viewSiteCode,
            viewAlamat_a, viewDesa,
            viewKecamatan, viewKabupaten,
            viewProvinsi, viewKodePos,
            viewNamaKontak, viewNomorTelepon,
            viewAlamat_b, viewHubunganDenganPemilik,
            viewNamaPemilik, viewBadanHukum,
            viewAlamat_c, viewNomorTeleponFaxNoHp,
            viewEmail, viewRencanaKerjaSama,
            viewHargaPenawaranSewa, viewPeriodeSewa,
            viewGracePeriod, viewStatusHarga,
            viewServiceChange, viewBiayaPajak,
            viewBiayaNotaris, viewSertifikasi,
            viewAtasNama, viewLuasTanah,
            viewIBM, viewUsername,
            viewSEmail, viewRegion, viewPeriodeSewaEndDate,
            viewInfoLokasiClick, viewInfoKontakClick, viewInfoKepemilikanClick,
            viewInfoLogalitasClick, viewInfoSewaClick;
    ImageView viewProfileIMG;
    String option = "";
    String option_a = "";
    String option_b = "";
    private SwipeRefreshLayout viewRefresh;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.site_info_hy, container, false);
        initView();
        return view;
    }

    void initView() {
        System.out.println("SITE CODE : " + utils.getDataSession(view.getContext(), "sessionValKeySiteCode"));
        System.out.println("USER ID : " + utils.getDataSession(view.getContext(), "sessionValKeyUserID"));
        viewRefresh = (SwipeRefreshLayout) view.findViewById(R.id.viewRefresh);
        viewRefresh.setOnRefreshListener(this);
        viewID = (TextView) view.findViewById(R.id.viewID);
        viewLocations = (TextView) view.findViewById(R.id.viewLocations);
        viewSiteCode = (TextView) view.findViewById(R.id.viewSiteCode);
        viewAlamat_a = (TextView) view.findViewById(R.id.viewAlamat_a);
        viewDesa = (TextView) view.findViewById(R.id.viewDesa);
        viewKecamatan = (TextView) view.findViewById(R.id.viewKecamatan);
        viewKabupaten = (TextView) view.findViewById(R.id.viewKabupaten);
        viewProvinsi = (TextView) view.findViewById(R.id.viewProvinsi);
        viewKodePos = (TextView) view.findViewById(R.id.viewKodePos);
        viewNamaKontak = (TextView) view.findViewById(R.id.viewNamaKontak);
        viewNomorTelepon = (TextView) view.findViewById(R.id.viewNomorTelepon);
        viewAlamat_b = (TextView) view.findViewById(R.id.viewAlamat_b);
        viewHubunganDenganPemilik = (TextView) view.findViewById(R.id.viewHubunganDenganPemilik);
        viewNamaPemilik = (TextView) view.findViewById(R.id.viewNamaPemilik);
        viewBadanHukum = (TextView) view.findViewById(R.id.viewBadanHukum);
        viewAlamat_c = (TextView) view.findViewById(R.id.viewAlamat_c);
        viewNomorTeleponFaxNoHp = (TextView) view.findViewById(R.id.viewNomorTeleponFaxNoHp);
        viewEmail = (TextView) view.findViewById(R.id.viewEmail);
        viewRencanaKerjaSama = (TextView) view.findViewById(R.id.viewRencanaKerjaSama);
        viewHargaPenawaranSewa = (TextView) view.findViewById(R.id.viewHargaPenawaranSewa);
        viewPeriodeSewa = (TextView) view.findViewById(R.id.viewPeriodeSewa);
        viewPeriodeSewaEndDate = (TextView) view.findViewById(R.id.viewPeriodeSewaEndDate);
        viewGracePeriod = (TextView) view.findViewById(R.id.viewGracePeriod);
        viewStatusHarga = (TextView) view.findViewById(R.id.viewStatusHarga);
        viewServiceChange = (TextView) view.findViewById(R.id.viewServiceChange);
        viewBiayaPajak = (TextView) view.findViewById(R.id.viewBiayaPajak);
        viewBiayaNotaris = (TextView) view.findViewById(R.id.viewBiayaNotaris);
        viewSertifikasi = (TextView) view.findViewById(R.id.viewSertifikasi);
        viewAtasNama = (TextView) view.findViewById(R.id.viewAtasNama);
        viewLuasTanah = (TextView) view.findViewById(R.id.viewLuasTanah);
        viewIBM = (TextView) view.findViewById(R.id.viewIBM);
        viewUsername = (TextView) view.findViewById(R.id.viewUsername);
        viewSEmail = (TextView) view.findViewById(R.id.viewSEmail);
        viewRegion = (TextView) view.findViewById(R.id.viewRegion);

        viewInfoLokasiClick = (TextView) view.findViewById(R.id.viewInfoLokasiClick);
        viewInfoLokasiClick.setOnClickListener(this);
        viewInfoKontakClick = (TextView) view.findViewById(R.id.viewInfoKontakClick);
        viewInfoKontakClick.setOnClickListener(this);
        viewInfoKepemilikanClick = (TextView) view.findViewById(R.id.viewInfoKepemilikanClick);
        viewInfoKepemilikanClick.setOnClickListener(this);
        viewInfoLogalitasClick = (TextView) view.findViewById(R.id.viewInfoLogalitasClick);
        viewInfoLogalitasClick.setOnClickListener(this);
        viewInfoSewaClick = (TextView) view.findViewById(R.id.viewInfoSewaClick);
        viewInfoSewaClick.setOnClickListener(this);

        if (!utils.getDataSession(view.getContext(),"sessionSTATUS").equals("MAKER")){
            viewInfoLokasiClick.setVisibility(View.GONE);
            viewInfoKontakClick.setVisibility(View.GONE);
            viewInfoKepemilikanClick.setVisibility(View.GONE);
            viewInfoLogalitasClick.setVisibility(View.GONE);
            viewInfoSewaClick.setVisibility(View.GONE);
        }

        viewProfileIMG = (ImageView) view.findViewById(R.id.viewProffileIMG);
        viewProfileIMG.setVisibility(View.GONE);
        showSiteInformation(params.URL_METHOD.GET_SITE);
    }

    void showSiteInformation(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionValKeySiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionValKeyUserID"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        System.out.println(response.toString());
                        try {
                            if (response.getString("status").equals("true")) {
                                if (!response.getJSONObject("data").getString("id").equals("null")) {
                                    viewID.setText(response.getJSONObject("data").getString("id"));
                                }
                                if (!response.getJSONObject("data").getString("USER_ID").equals("null")) {
                                    utils.setDataSession(view.getContext(), "sessionVuserId", response.getJSONObject("data").getString("USER_ID"));
                                }
                                if (!response.getJSONObject("data").getString(params.IMAGE).equals("null")) {
                                    Picasso.with(view.getContext())
                                            .load(api.SERVER_API_IMG + response.getJSONObject("data").getString(params.IMAGE))
                                            .resize(512, 800)
                                            .centerInside()
                                            .into(viewProfileIMG);
                                }
                                if (!response.getJSONObject("data").getString(params.USERNAME).equals("null")) {
                                    viewUsername.setText(response.getJSONObject("data").getString(params.USERNAME));
                                }
                                if (!response.getJSONObject("data").getString(params.REGIONAL).equals("null")) {
                                    viewRegion.setText(response.getJSONObject("data").getString(params.REGIONAL));
                                }
                                if (!response.getJSONObject("data").getString(params.S_EMAIL).equals("null")) {
                                    viewSEmail.setText(response.getJSONObject("data").getString(params.S_EMAIL));
                                }
                                if (!response.getJSONObject("data").getString(params.LATITUDE).equals("null") ||
                                        !response.getJSONObject("data").getString(params.LONGITUDE).equals("null")) {
                                    viewLocations.setText(response.getJSONObject("data").getString(params.LATITUDE)
                                            + "," + response.getJSONObject("data").getString(params.LONGITUDE));
                                    utils.setDataSession(view.getContext(), "sessionLat", response.getJSONObject("data").getString(params.LATITUDE));
                                    utils.setDataSession(view.getContext(), "sessionLong", response.getJSONObject("data").getString(params.LONGITUDE));
                                }
                                if (!response.getJSONObject("data").getString(params.SITE_CODE).equals("null")) {
                                    viewSiteCode.setText(response.getJSONObject("data").getString(params.SITE_CODE));
                                }
                                if (!response.getJSONObject("data").getString(params.ALAMAT_A).equals("null")) {
                                    viewAlamat_a.setText(response.getJSONObject("data").getString(params.ALAMAT_A));
                                }
                                if (!response.getJSONObject("data").getString(params.DESA).equals("null")) {
                                    viewDesa.setText(response.getJSONObject("data").getString(params.DESA));
                                }
                                if (!response.getJSONObject("data").getString(params.KECAMATAN).equals("null")) {
                                    viewKecamatan.setText(response.getJSONObject("data").getString(params.KECAMATAN));
                                }
                                if (!response.getJSONObject("data").getString(params.KABUPATEN).equals("null")) {
                                    viewKabupaten.setText(response.getJSONObject("data").getString(params.KABUPATEN));
                                }

                                if (!response.getJSONObject("data").getString(params.PROVINSI).equals("null")) {
                                    viewProvinsi.setText(response.getJSONObject("data").getString(params.PROVINSI));
                                }
                                if (!response.getJSONObject("data").getString(params.KODE_POS).equals("null")) {
                                    viewKodePos.setText(response.getJSONObject("data").getString(params.KODE_POS));
                                }

                                if (!response.getJSONObject("data").getString(params.NAMA_KONTAK).equals("null")) {
                                    viewNamaKontak.setText(response.getJSONObject("data").getString(params.NAMA_KONTAK));
                                }
                                if (!response.getJSONObject("data").getString(params.NOMOR_TELEPON).equals("null")) {
                                    viewNomorTelepon.setText(response.getJSONObject("data").getString(params.NOMOR_TELEPON));
                                }
                                if (!response.getJSONObject("data").getString(params.ALAMAT_B).equals("null")) {
                                    viewAlamat_b.setText(response.getJSONObject("data").getString(params.ALAMAT_B));
                                }
                                if (!response.getJSONObject("data").getString(params.HUBUNGAN_DENGAN_PEMILIK).equals("null")) {
                                    viewHubunganDenganPemilik.setText(response.getJSONObject("data").getString(params.HUBUNGAN_DENGAN_PEMILIK));
                                }
                                if (!response.getJSONObject("data").getString(params.NAMA_PEMILIK).equals("null")) {
                                    viewNamaPemilik.setText(response.getJSONObject("data").getString(params.NAMA_PEMILIK));
                                }
                                if (!response.getJSONObject("data").getString(params.BADAN_HUKUM).equals("null")) {
                                    viewBadanHukum.setText(response.getJSONObject("data").getString(params.BADAN_HUKUM));
                                }
                                if (!response.getJSONObject("data").getString(params.ALAMAT_C).equals("null")) {
                                    viewAlamat_c.setText(response.getJSONObject("data").getString(params.ALAMAT_C));
                                }
                                if (!response.getJSONObject("data").getString(params.NOTLP_OR_FAX_OR_NOHP).equals("null")) {
                                    viewNomorTeleponFaxNoHp.setText(response.getJSONObject("data").getString(params.NOTLP_OR_FAX_OR_NOHP));
                                }
                                if (!response.getJSONObject("data").getString(params.EMAIL).equals("null")) {
                                    viewEmail.setText(response.getJSONObject("data").getString(params.EMAIL));
                                }
                                if (!response.getJSONObject("data").getString(params.RENCANA_KERJASAMA).equals("null")) {
                                    viewRencanaKerjaSama.setText(response.getJSONObject("data").getString(params.RENCANA_KERJASAMA));
                                }
                                if (!response.getJSONObject("data").getString(params.HARGA_PENAWARAN_SEWA).equals("null")) {
                                    viewHargaPenawaranSewa.setText(response.getJSONObject("data").getString(params.HARGA_PENAWARAN_SEWA));
                                }
                                if (!response.getJSONObject("data").getString(params.PERIODE_SEWA_START).equals("null")) {
                                    viewPeriodeSewa.setText(response.getJSONObject("data").getString(params.PERIODE_SEWA_START));
                                }
                                if (!response.getJSONObject("data").getString(params.PERIODE_SEWA_END).equals("null")) {
                                    viewPeriodeSewaEndDate.setText(response.getJSONObject("data").getString(params.PERIODE_SEWA_END));
                                }
                                if (!response.getJSONObject("data").getString(params.GRACE_PERIOD).equals("null")) {
                                    viewGracePeriod.setText(response.getJSONObject("data").getString(params.GRACE_PERIOD));
                                }
                                if (!response.getJSONObject("data").getString(params.STATUS_HARGA).equals("null")) {
                                    viewStatusHarga.setText(response.getJSONObject("data").getString(params.STATUS_HARGA));
                                }
                                if (!response.getJSONObject("data").getString(params.SERVICE_CHARGE).equals("null")) {
                                    viewServiceChange.setText(response.getJSONObject("data").getString(params.SERVICE_CHARGE));
                                }
                                if (!response.getJSONObject("data").getString(params.BIAYA_PAJAK).equals("null")) {
                                    viewBiayaPajak.setText(response.getJSONObject("data").getString(params.BIAYA_PAJAK));
                                }
                                if (!response.getJSONObject("data").getString(params.BIAYA_NOTARIS).equals("null")) {
                                    viewBiayaNotaris.setText(response.getJSONObject("data").getString(params.BIAYA_NOTARIS));
                                }
                                if (!response.getJSONObject("data").getString(params.SERTIFIKASI).equals("null")) {
                                    viewSertifikasi.setText(response.getJSONObject("data").getString(params.SERTIFIKASI));
                                }
                                if (!response.getJSONObject("data").getString(params.ATAS_NAMA).equals("null")) {
                                    viewAtasNama.setText(response.getJSONObject("data").getString(params.ATAS_NAMA));
                                }
                                if (!response.getJSONObject("data").getString(params.LUAS_TANAH).equals("null")) {
                                    viewLuasTanah.setText(response.getJSONObject("data").getString(params.LUAS_TANAH));
                                }
                                if (!response.getJSONObject("data").getString(params.IJIN_MENDIRIKAN_BANGUNAN).equals("null")) {
                                    viewIBM.setText(response.getJSONObject("data").getString(params.IJIN_MENDIRIKAN_BANGUNAN));
                                }
                                dialog.dismiss();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println(e.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext().getApplicationContext(), "Opps.. Error.. Site Info : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    public String getFormateprice(String price) {
        Locale locale = new Locale("in", "ID");
        Format format = NumberFormat.getCurrencyInstance(locale);
        return format.format(new BigDecimal(price));
    }

    @Override
    public void onRefresh() {
        showSiteInformation(params.URL_METHOD.GET_SITE);
        viewRefresh.setRefreshing(false);
    }

    void updateInfoLokasi() {
        String hide = "hide";
        final View viewIL = LayoutInflater.from(view.getContext()).inflate(R.layout.locationinfo_fragment, null, false);
        final EditText viewGpslocation, viewSteCodes, mViewAlamatLoc, mViewDesa, mViewKecamatan, mViewKabupaten, mViewProvinsi, mViewKodePos;
        Button mViewNextLocationInfo;
        final ProgressBar vDialogILs;
        LinearLayout viewFrameUpdateIL, viewCancelIL, viewUpdateIL;
        viewFrameUpdateIL = (LinearLayout) viewIL.findViewById(R.id.viewFrameUpdateIL);
        viewCancelIL = (LinearLayout) viewIL.findViewById(R.id.viewCancelIL);
        viewUpdateIL = (LinearLayout) viewIL.findViewById(R.id.viewUpdateIL);

        mViewAlamatLoc = (EditText) viewIL.findViewById(R.id.viewAlamatLoc);
        mViewDesa = (EditText) viewIL.findViewById(R.id.viewDesa);
        mViewKecamatan = (EditText) viewIL.findViewById(R.id.viewKecamatan);
        mViewKabupaten = (EditText) viewIL.findViewById(R.id.viewKabupaten);
        mViewProvinsi = (EditText) viewIL.findViewById(R.id.viewProvinsi);
        mViewKodePos = (EditText) viewIL.findViewById(R.id.viewKodePos);
        viewGpslocation = (EditText) viewIL.findViewById(R.id.viewGpslocation);
        viewSteCodes = (EditText) viewIL.findViewById(R.id.viewSteCode);

        viewGpslocation.setText(viewLocations.getText().toString());
        viewSteCodes.setText(viewSiteCode.getText().toString());
        mViewAlamatLoc.setText(viewAlamat_a.getText().toString());
        mViewDesa.setText(viewDesa.getText().toString());
        mViewKecamatan.setText(viewKecamatan.getText().toString());
        mViewKabupaten.setText(viewKabupaten.getText().toString());
        mViewProvinsi.setText(viewProvinsi.getText().toString());
        mViewKodePos.setText(viewKodePos.getText().toString());
        mViewNextLocationInfo = (Button) viewIL.findViewById(R.id.viewNextLocationInfo);


        if (hide.equals("hide")) {
            mViewNextLocationInfo.setVisibility(View.GONE);
            viewFrameUpdateIL.setVisibility(View.VISIBLE);
        } else if (hide.equals("") || hide.equals("null")) {
            mViewNextLocationInfo.setVisibility(View.VISIBLE);
            viewFrameUpdateIL.setVisibility(View.GONE);
        }
        vDialogILs = (ProgressBar) viewIL.findViewById(R.id.vDialogILs);

        final AlertDialog vDialogIL = utils.customeAlertInputDialog(view.getContext(), viewIL);

        viewCancelIL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vDialogIL.cancel();
            }
        });

        viewUpdateIL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vDialogILs.setVisibility(View.VISIBLE);
                AndroidNetworking.post(api.SERVER_API+"updateinfolokasi")
                        .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                        .addBodyParameter(params.LATITUDE, utils.getDataSession(viewIL.getContext(), "sessionLat"))
                        .addBodyParameter(params.LATITUDE, utils.getDataSession(viewIL.getContext(), "sessionLong"))
                        .addBodyParameter(params.SITE_CODE, viewSteCodes.getText().toString())
                        .addBodyParameter(params.ALAMAT_A, mViewAlamatLoc.getText().toString())
                        .addBodyParameter(params.DESA, mViewDesa.getText().toString())
                        .addBodyParameter(params.KECAMATAN, mViewKecamatan.getText().toString())
                        .addBodyParameter(params.KABUPATEN, mViewKabupaten.getText().toString())
                        .addBodyParameter(params.PROVINSI, mViewProvinsi.getText().toString())
                        .addBodyParameter(params.KODE_POS, mViewKodePos.getText().toString())
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getString("status").equals("true")) {
                                        vDialogILs.setVisibility(View.GONE);
                                        vDialogIL.cancel();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    vDialogILs.setVisibility(View.GONE);
                                    vDialogIL.cancel();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Toast.makeText(viewIL.getContext(), "Opps error Site Info 2 : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                                vDialogILs.setVisibility(View.GONE);
                                vDialogIL.cancel();
                            }
                        });
            }
        });
        vDialogIL.show();
    }

    void updateInfoSewa() {
        String rk = viewRencanaKerjaSama.getText().toString();
        final View viewRI = LayoutInflater.from(view.getContext()).inflate(R.layout.edit_info_sewa, null, false);
        RadioGroup viewSelectedPlantIS;
        final ProgressBar vDialogs;
        RadioButton viewRadioSewaIS, viewRadioRevenueSharingIS, viewRadioFranchiseIS;
        viewRadioSewaIS = (RadioButton) viewRI.findViewById(R.id.viewRadioSewaIS);
        viewRadioRevenueSharingIS = (RadioButton) viewRI.findViewById(R.id.viewRadioRevenueSharingIS);
        viewRadioFranchiseIS = (RadioButton) viewRI.findViewById(R.id.viewRadioFranchiseIS);

        LinearLayout viewCancelIS, viewUpdateIS;
        viewCancelIS = (LinearLayout) viewRI.findViewById(R.id.viewCancelIS);
        viewUpdateIS = (LinearLayout) viewRI.findViewById(R.id.viewUpdateIS);

        final EditText viewHargaSewaIS, viewPeriodeIS, viewEndDateIS, viewGracePeriodIS,
                viewStatusHargaIS, viewServiceChangeIS, viewBiayaPajakIS, viewBiayaNotarisIS;

        viewHargaSewaIS = (EditText) viewRI.findViewById(R.id.viewHargaSewaIS);
        viewPeriodeIS = (EditText) viewRI.findViewById(R.id.viewPeriodeIS);
        viewEndDateIS = (EditText) viewRI.findViewById(R.id.viewEndDateIS);
        viewGracePeriodIS = (EditText) viewRI.findViewById(R.id.viewGracePeriodIS);
        viewStatusHargaIS = (EditText) viewRI.findViewById(R.id.viewStatusHargaIS);
        viewServiceChangeIS = (EditText) viewRI.findViewById(R.id.viewServiceChangeIS);
        viewBiayaPajakIS = (EditText) viewRI.findViewById(R.id.viewBiayaPajakIS);
        viewBiayaNotarisIS = (EditText) viewRI.findViewById(R.id.viewBiayaNotarisIS);
        viewSelectedPlantIS = (RadioGroup) viewRI.findViewById(R.id.viewSelectedPlantIS);
        vDialogs = (ProgressBar)viewRI.findViewById(R.id.vDialogs);
        final AlertDialog vDialogRI = utils.customeAlertInputDialog(view.getContext(), viewRI);

        viewSelectedPlantIS.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.viewRadioSewaIS:
                        option = "Sewa";
                        break;
                    case R.id.viewRadioRevenueSharingIS:
                        option = "Revenue Sharing";
                        break;
                    case R.id.viewRadioFranchiseIS:
                        option = "Franchise";
                        break;
                }
            }
        });

        if (rk.equals("Sewa")) {
            viewRadioSewaIS.setChecked(true);
        }
        if (rk.equals("Revenue Sharing")) {
            viewRadioRevenueSharingIS.setChecked(true);
        }
        if (rk.equals("Franchise")) {
            viewRadioFranchiseIS.setChecked(true);
        }

        viewHargaSewaIS.setText(viewHargaPenawaranSewa.getText().toString());
        viewPeriodeIS.setText(viewPeriodeSewa.getText().toString());
        viewEndDateIS.setText(viewPeriodeSewaEndDate.getText().toString());
        viewGracePeriodIS.setText(viewGracePeriod.getText().toString());
        viewStatusHargaIS.setText(viewStatusHarga.getText().toString());
        viewServiceChangeIS.setText(viewServiceChange.getText().toString());
        viewBiayaPajakIS.setText(viewBiayaPajak.getText().toString());
        viewBiayaNotarisIS.setText(viewBiayaNotaris.getText().toString());

        viewCancelIS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vDialogRI.cancel();
            }
        });
        viewUpdateIS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vDialogs.setVisibility(View.VISIBLE);
                AndroidNetworking.post(api.SERVER_API + "updateinfosewa")
                        .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                        .addBodyParameter(params.SITE_CODE, viewSiteCode.getText().toString())
                        .addBodyParameter(params.USER_ID, utils.getDataSession(viewRI.getContext(), "sessionVuserId"))
                        .addBodyParameter(params.RENCANA_KERJASAMA, option)
                        .addBodyParameter(params.HARGA_PENAWARAN_SEWA, viewHargaSewaIS.getText().toString())
                        .addBodyParameter(params.PERIODE_SEWA_START, viewPeriodeIS.getText().toString())
                        .addBodyParameter(params.PERIODE_SEWA_END, viewEndDateIS.getText().toString())
                        .addBodyParameter(params.GRACE_PERIOD, viewGracePeriodIS.getText().toString())
                        .addBodyParameter(params.STATUS_HARGA, viewStatusHargaIS.getText().toString())
                        .addBodyParameter(params.SERVICE_CHARGE, viewServiceChangeIS.getText().toString())
                        .addBodyParameter(params.BIAYA_PAJAK, viewBiayaPajakIS.getText().toString())
                        .addBodyParameter(params.BIAYA_NOTARIS, viewBiayaNotarisIS.getText().toString())
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getString("status").equals("true")) {
                                        vDialogs.setVisibility(View.GONE);
                                        vDialogRI.cancel();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    vDialogs.setVisibility(View.GONE);
                                    vDialogRI.cancel();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Toast.makeText(viewRI.getContext(), "Opps error Site Info update : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                                vDialogs.setVisibility(View.GONE);
                                vDialogRI.cancel();
                            }
                        });
            }
        });
        vDialogRI.show();

    }

    void updateInfoKontak() {
        String hide = "hide";
        final View viewCI = LayoutInflater.from(view.getContext()).inflate(R.layout.contact, null, false);
        LinearLayout viewFrameCI, viewCancelCI, viewUpdateCI;
        Button viewNextContact;
        final ProgressBar vDialogKontak;
        final EditText mViewNamaKontak, mViewNomorTelepon, mViewKAlamat, mViewHubunganDenganPemilik;


        mViewNamaKontak = (EditText) viewCI.findViewById(R.id.viewNamaKontak);
        mViewNomorTelepon = (EditText) viewCI.findViewById(R.id.viewNomorTelepon);
        mViewKAlamat = (EditText) viewCI.findViewById(R.id.viewKAlamat);
        mViewHubunganDenganPemilik = (EditText) viewCI.findViewById(R.id.viewHubunganDenganPemilik);

        viewFrameCI = (LinearLayout) viewCI.findViewById(R.id.viewFrameCI);
        viewCancelCI = (LinearLayout) viewCI.findViewById(R.id.viewCancelCI);
        viewUpdateCI = (LinearLayout) viewCI.findViewById(R.id.viewUpdateCI);
        viewNextContact = (Button) viewCI.findViewById(R.id.viewNextContact);
        vDialogKontak = (ProgressBar) viewCI.findViewById(R.id.vDialogKontak);
        final AlertDialog vDialogCI = utils.customeAlertInputDialog(view.getContext(), viewCI);

        if (hide.equals("hide")) {
            viewNextContact.setVisibility(View.GONE);
            viewFrameCI.setVisibility(View.VISIBLE);
        } else if (hide.equals("") || hide.equals("null")) {
            viewNextContact.setVisibility(View.VISIBLE);
            viewFrameCI.setVisibility(View.GONE);
        } else {
            viewNextContact.setVisibility(View.VISIBLE);
            viewFrameCI.setVisibility(View.GONE);
        }

        mViewNamaKontak.setText(viewNamaKontak.getText().toString());
        mViewNomorTelepon.setText(viewNomorTelepon.getText().toString());
        mViewKAlamat.setText(viewAlamat_b.getText().toString());
        mViewHubunganDenganPemilik.setText(viewHubunganDenganPemilik.getText().toString());

        viewCancelCI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vDialogCI.cancel();
            }
        });
        viewUpdateCI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vDialogKontak.setVisibility(View.VISIBLE);
                AndroidNetworking.post(api.SERVER_API+"updateinfokontak")
                        .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                        .addBodyParameter(params.SITE_CODE, viewSiteCode.getText().toString())
                        .addBodyParameter(params.USER_ID, utils.getDataSession(viewCI.getContext(), "sessionVuserId"))
                        .addBodyParameter(params.NAMA_KONTAK, mViewNamaKontak.getText().toString())
                        .addBodyParameter(params.NOMOR_TELEPON, mViewNomorTelepon.getText().toString())
                        .addBodyParameter(params.ALAMAT_B, mViewKAlamat.getText().toString())
                        .addBodyParameter(params.HUBUNGAN_DENGAN_PEMILIK, mViewHubunganDenganPemilik.getText().toString())
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (!response.getString("status").equals("true")) {
                                        vDialogKontak.setVisibility(View.GONE);
                                        vDialogCI.cancel();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    vDialogKontak.setVisibility(View.GONE);
                                    vDialogCI.cancel();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Toast.makeText(viewCI.getContext(), "Opps error Site Info Lok : "+anError.getMessage(), Toast.LENGTH_LONG).show();
                                vDialogKontak.setVisibility(View.GONE);
                                vDialogCI.cancel();
                            }
                        });
            }
        });
        vDialogCI.show();
    }

    void updateInfoLegalitas() {
        String hide = "hide";
        String sertifikat = viewSertifikasi.getText().toString();
        String IMB = viewIBM.getText().toString();
        final View viewIL = LayoutInflater.from(view.getContext()).inflate(R.layout.buildinglegality, null, false);
        RadioGroup viewSertifikasiIL, viewIjinMendirikanBnagunanIL;
        RadioButton viewSementaraIL, viewTetapIL, viewSHMIL, viewHGBIL, viewHGUIL, viewGIRIKIL;
        final EditText viewAtasNamaIL, viewLuasTanahIL;
        Button viewNextLegalityIL;
        final ProgressBar vDialogBLS;
        LinearLayout viewFrameIL, viewCancelIL, viewUpdateIL;
        viewFrameIL = (LinearLayout) viewIL.findViewById(R.id.viewFrameIL);
        viewCancelIL = (LinearLayout) viewIL.findViewById(R.id.viewCancelIL);
        viewUpdateIL = (LinearLayout) viewIL.findViewById(R.id.viewUpdateIL);

        viewNextLegalityIL = (Button) viewIL.findViewById(R.id.viewNextLegality);
        viewSertifikasiIL = (RadioGroup) viewIL.findViewById(R.id.viewSertifikasi);
        viewIjinMendirikanBnagunanIL = (RadioGroup) viewIL.findViewById(R.id.viewIjinMendirikanBnagunan);

        viewSementaraIL = (RadioButton) viewIL.findViewById(R.id.viewSementara);
        viewTetapIL = (RadioButton) viewIL.findViewById(R.id.viewTetap);

        viewSHMIL = (RadioButton) viewIL.findViewById(R.id.viewSHM);
        viewHGBIL = (RadioButton) viewIL.findViewById(R.id.viewHGB);
        viewHGUIL = (RadioButton) viewIL.findViewById(R.id.viewHGU);
        viewGIRIKIL = (RadioButton) viewIL.findViewById(R.id.viewGIRIK);

        viewAtasNamaIL = (EditText) viewIL.findViewById(R.id.viewAtasNama);
        viewLuasTanahIL = (EditText) viewIL.findViewById(R.id.viewLuasTanah);
        vDialogBLS = (ProgressBar) viewIL.findViewById(R.id.vDialogBLS);
        final AlertDialog vDialogIL = utils.customeAlertInputDialog(view.getContext(), viewIL);

        if (hide.equals("hide")) {
            viewNextLegalityIL.setVisibility(View.GONE);
            viewFrameIL.setVisibility(View.VISIBLE);
        } else if (hide.equals("") || hide.equals("null")) {
            viewNextLegalityIL.setVisibility(View.VISIBLE);
            viewFrameIL.setVisibility(View.GONE);
        } else {
            viewNextLegalityIL.setVisibility(View.VISIBLE);
            viewFrameIL.setVisibility(View.GONE);
        }
        if (sertifikat.equals("SHM")) {
            viewSHMIL.setChecked(true);
        }
        if (sertifikat.equals("HGU")) {
            viewHGUIL.setChecked(true);
        }
        if (sertifikat.equals("HGB")) {
            viewHGBIL.setChecked(true);
        }
        if (sertifikat.equals("GIRIK")) {
            viewGIRIKIL.setChecked(true);
        }
        if (IMB.equals("Sementara")) {
            viewSementaraIL.setChecked(true);
        }
        if (IMB.equals("Tetap")) {
            viewTetapIL.setChecked(true);
        }
        viewAtasNamaIL.setText(viewAtasNama.getText().toString());
        viewLuasTanahIL.setText(viewLuasTanah.getText().toString());

        viewSertifikasiIL.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.viewSHM:
                        option_a = "SHM";
                        break;
                    case R.id.viewHGB:
                        option_a = "HGB";
                        break;
                    case R.id.viewHGU:
                        option_a = "HGU";
                        break;
                    case R.id.viewGIRIK:
                        option_a = "GIRIK";
                        break;
                }
            }
        });
        viewIjinMendirikanBnagunanIL.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.viewTetap:
                        option_b = "Tetap";
                        break;
                    case R.id.viewSementara:
                        option_b = "Sementara";
                        break;
                }
            }
        });
        viewCancelIL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vDialogIL.cancel();
            }
        });
        viewUpdateIL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vDialogBLS.setVisibility(View.VISIBLE);
                AndroidNetworking.post(api.SERVER_API+"updateinfolegal")
                        .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                        .addBodyParameter(params.SITE_CODE, viewSiteCode.getText().toString())
                        .addBodyParameter(params.USER_ID, utils.getDataSession(viewIL.getContext(), "sessionVuserId"))
                        .addBodyParameter(params.SERTIFIKASI, option_a)
                        .addBodyParameter(params.ATAS_NAMA, viewAtasNamaIL.getText().toString())
                        .addBodyParameter(params.LUAS_TANAH, viewLuasTanahIL.getText().toString())
                        .addBodyParameter(params.IJIN_MENDIRIKAN_BANGUNAN, option_b)
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getString("status").equals("true")) {
                                        vDialogBLS.setVisibility(View.GONE);
                                        vDialogIL.cancel();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    vDialogBLS.setVisibility(View.GONE);
                                    vDialogIL.cancel();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Toast.makeText(viewIL.getContext(), "Opps Error : " + anError.getMessage(), Toast.LENGTH_LONG).show();
                                vDialogBLS.setVisibility(View.GONE);
                                vDialogIL.cancel();
                            }
                        });
            }
        });
        vDialogIL.show();
    }

    void updateInfoKepemilikan() {
        String hide = "hide";
        final View viewIK = LayoutInflater.from(view.getContext()).inflate(R.layout.ownershipinformation, null, false);
        final EditText viewNamaPemilikIK, viewBadanHukumIK, viewOAlamatIK, viewNoTelpFaxHoHpIK, viewEmailIK;
        Button viewNextOwnerIK = (Button) viewIK.findViewById(R.id.viewNextOwner);
        LinearLayout viewFrameIK, viewCancelIK, viewUpdateIK;
        final ProgressBar vDialogIKS;
        viewFrameIK = (LinearLayout) viewIK.findViewById(R.id.viewFrameIK);
        viewCancelIK = (LinearLayout) viewIK.findViewById(R.id.viewCancelIK);
        viewUpdateIK = (LinearLayout) viewIK.findViewById(R.id.viewUpdateIK);

        viewNamaPemilikIK = (EditText) viewIK.findViewById(R.id.viewNamaPemilik);
        viewBadanHukumIK = (EditText) viewIK.findViewById(R.id.viewBadanHukum);
        viewOAlamatIK = (EditText) viewIK.findViewById(R.id.viewOAlamat);
        viewNoTelpFaxHoHpIK = (EditText) viewIK.findViewById(R.id.viewNoTelpFaxHoHp);
        viewEmailIK = (EditText) viewIK.findViewById(R.id.viewEmail);
        vDialogIKS = (ProgressBar)viewIK.findViewById(R.id.vDialogIKS);
        final AlertDialog vDialogIk = utils.customeAlertInputDialog(view.getContext(), viewIK);
        if (hide.equals("hide")) {
            viewFrameIK.setVisibility(View.VISIBLE);
            viewNextOwnerIK.setVisibility(View.GONE);
        } else if (hide.equals("") && hide.equals("null")) {
            viewFrameIK.setVisibility(View.GONE);
            viewNextOwnerIK.setVisibility(View.VISIBLE);
        } else {
            viewFrameIK.setVisibility(View.GONE);
            viewNextOwnerIK.setVisibility(View.VISIBLE);
        }
        viewNamaPemilikIK.setText(viewNamaPemilik.getText().toString());
        viewBadanHukumIK.setText(viewBadanHukum.getText().toString());
        viewOAlamatIK.setText(viewAlamat_c.getText().toString());
        viewNoTelpFaxHoHpIK.setText(viewNomorTeleponFaxNoHp.getText().toString());
        viewEmailIK.setText(viewSEmail.getText().toString());

        viewCancelIK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vDialogIk.cancel();
            }
        });
        viewUpdateIK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                vDialogIKS.setVisibility(View.VISIBLE);
                AndroidNetworking.post(api.SERVER_API+"updateinfokepemilikan")
                        .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                        .addBodyParameter(params.SITE_CODE, viewSiteCode.getText().toString())
                        .addBodyParameter(params.USER_ID,utils.getDataSession(viewIK.getContext(), "sessionVuserId"))
                        .addBodyParameter(params.NAMA_PEMILIK, viewNamaPemilikIK.getText().toString())
                        .addBodyParameter(params.BADAN_HUKUM, viewBadanHukumIK.getText().toString())
                        .addBodyParameter(params.ALAMAT_C, viewOAlamatIK.getText().toString())
                        .addBodyParameter(params.NOTLP_OR_FAX_OR_NOHP, viewNoTelpFaxHoHpIK.getText().toString())
                        .addBodyParameter(params.EMAIL, viewEmailIK.getText().toString())
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getString("status").equals("true")){
                                        vDialogIKS.setVisibility(View.GONE);
                                        vDialogIk.cancel();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    vDialogIKS.setVisibility(View.GONE);
                                    vDialogIk.cancel();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                Toast.makeText(view.getContext(), "Opps error", Toast.LENGTH_LONG).show();
                                vDialogIKS.setVisibility(View.GONE);
                                vDialogIk.cancel();
                            }
                        });
            }
        });
        vDialogIk.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewInfoLokasiClick:
                updateInfoLokasi();
                break;
            case R.id.viewInfoKontakClick:
                updateInfoKontak();
                break;
            case R.id.viewInfoKepemilikanClick:
                updateInfoKepemilikan();
                break;
            case R.id.viewInfoSewaClick:
                updateInfoSewa();
                break;
            case R.id.viewInfoLogalitasClick:
                updateInfoLegalitas();
                break;
        }
    }
}
