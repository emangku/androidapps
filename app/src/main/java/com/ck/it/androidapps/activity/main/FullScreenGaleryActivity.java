package com.ck.it.androidapps.activity.main;

import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.ui.BottomSheetFragment;
import com.ck.it.androidapps.helpers.api;
import com.squareup.picasso.Picasso;

@SuppressWarnings("ConstantConditions")
public class FullScreenGaleryActivity extends AppCompatActivity {
    String urlgambar;
    FloatingActionButton fab;
    LinearLayout layoutBottomSheet;
    Button btnBottomSheet;
    BottomSheetBehavior sheetBehavior;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_full_screen_galery);
        getSupportActionBar().hide();

        ImageView fullGambar = (ImageView) findViewById(R.id.fullGambar);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            urlgambar = extras.getString("url");
            Picasso.with(this)
                    .load(api.SERVER_API_IMG + urlgambar)
                    .into(fullGambar);
        }
        init();
    }

    void init() {
        layoutBottomSheet = (LinearLayout) findViewById(R.id.bottom_sheet);
//        sheetBehavior = BottomSheetBehavior.from(layoutBottomSheet);
//        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
//            @Override
//            public void onStateChanged(@NonNull View bottomSheet, int newState) {
//                switch (newState) {
//                    case BottomSheetBehavior.STATE_HIDDEN:
//                        break;
//                    case BottomSheetBehavior.STATE_EXPANDED:
//                        break;
//                    case BottomSheetBehavior.STATE_COLLAPSED:
//                        break;
//                    case BottomSheetBehavior.STATE_DRAGGING:
//                        break;
//                    case BottomSheetBehavior.STATE_SETTLING:
//                        break;
//                }
//            }
//
//            @Override
//            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
//
//            }
//        });

        BottomSheetFragment fragment = new BottomSheetFragment();
        fragment.show(getSupportFragmentManager(),fragment.getTag());
        fab = (FloatingActionButton) findViewById(R.id.viewDataComments);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BottomSheetFragment fragment = new BottomSheetFragment();
                fragment.show(getSupportFragmentManager(),fragment.getTag());
            }
        });
    }
}
