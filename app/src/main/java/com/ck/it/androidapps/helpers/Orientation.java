package com.ck.it.androidapps.helpers;

/**
 * Created by Creef on 12/19/2017.
 */

public enum Orientation {
    VERTICAL,
    HORIZONTAL;
}
