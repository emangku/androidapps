package com.ck.it.androidapps.fragment.areaanalysis;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.ck.it.androidapps.R;
import com.ck.it.androidapps.util.utils;

/**
 * Created by Creef on 12/7/2017.
 */

public class DemografiFragment extends Fragment implements View.OnClickListener{
    private View view;
    Spinner viewKelasSosialEkonomi,viewJenisKonsumen,viewgayaHidup;
    Button viewNextDemografi;
    String[] sosialekonomio = {
            "-Kelas Sosial Ekonomi-",
            "Kelas Sosial Atas",
            "Kelas Sosial Menengah",
            "Kelas Sosial Bawah"};
    String[] jeniskonsumen = {
            "-Jenis Konsumen-",
            "Tourist",
            "Non-tourist"};
    String[] gayahidup = {
            "-Gaya Hidup-",
            "Modern",
            "Semi-Modern"};

    ArrayAdapter<String> skAdapter, jkAdapter,ghAdapter;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.demografi_fragment,container,false);
        initView();
        return view;
    }

    void initView(){
        viewNextDemografi = (Button)view.findViewById(R.id.viewNextDemografi);
        viewNextDemografi.setOnClickListener(this);

        viewKelasSosialEkonomi = (Spinner) view.findViewById(R.id.viewKelas_SosialEkonomi);
        skAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,sosialekonomio);
        viewKelasSosialEkonomi.setAdapter(skAdapter);

        viewJenisKonsumen = (Spinner)view.findViewById(R.id.viewJenisKonsumen);
        jkAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,jeniskonsumen);
        viewJenisKonsumen.setAdapter(jkAdapter);

        viewgayaHidup = (Spinner)view.findViewById(R.id.viewgayaHidup);
        ghAdapter = new ArrayAdapter<String>(view.getContext(),R.layout.list_spiner,R.id.txtSpinner,gayahidup);
        viewgayaHidup.setAdapter(ghAdapter);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.viewNextDemografi:
                utils.showFragmentThree(view.getContext(),new KualtasLingkunganFragment());
                getActivity().setTitle("Kualitas Lingkungan");
                break;
        }
    }
}
