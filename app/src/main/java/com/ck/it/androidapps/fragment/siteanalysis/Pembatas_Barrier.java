package com.ck.it.androidapps.fragment.siteanalysis;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.fragment.areaanalysis.demografi.JumlahPendudukFragment;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by eko on 03/01/18.
 */

@SuppressWarnings("deprecation")
public class Pembatas_Barrier extends Fragment implements View.OnClickListener {
    private View view;
    String[] pembatas =  {
                "-Pembatas (Barrier)-",
                "Pagar Pembatas",
                "Trotoar",
                "Parit",
                "Jalan",
                "Halte",
                "Jembatan Penyebrangan"};
    Spinner viewPembatasBarrier;
    ArrayAdapter<String> adapter;
    Button viewNextPembetasBarrier;
    RadioGroup viewGroupPembatasBarrier;
    String type_opt = "";
    String opt = "";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pembatas_barrier, container, false);
        initView();
        return view;
    }


    void initView() {
        viewPembatasBarrier = (Spinner) view.findViewById(R.id.viewPembatasBarrier);
        adapter = new ArrayAdapter<String>(view.getContext(), R.layout.list_spiner, R.id.txtSpinner, pembatas);
        viewPembatasBarrier.setAdapter(adapter);
        viewPembatasBarrier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                type_opt = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        viewGroupPembatasBarrier = (RadioGroup) view.findViewById(R.id.viewGroupPembatasBarrier);
        viewGroupPembatasBarrier.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                switch (checkedId) {
                    case R.id.pb_1:
                        opt = "1";
                        break;
                    case R.id.pb_2:
                        opt = "2";
                        break;
                    case R.id.pb_3:
                        opt = "3";
                        break;
                    case R.id.pb_4:
                        opt = "4";
                        break;
                    case R.id.pb_5:
                        opt = "5";
                        break;
                }
            }
        });
        viewNextPembetasBarrier = (Button) view.findViewById(R.id.viewNextPembetasBarrier);
        viewNextPembetasBarrier.setOnClickListener(this);
    }

    void saveData(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(view.getContext(), params.URL_METHOD.SAVE_AREA_ANALYSIS);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), "sessionSiteCode"))
                .addBodyParameter(params.USER_ID, utils.getDataSession(view.getContext(), "sessionUserID"))
                .addBodyParameter(params.SA_ID, "SA")
                .addBodyParameter(params.TYPE, "Pembatas (Barrier)")
                .addBodyParameter(params.VALUE_OPTION, type_opt)
                .addBodyParameter(params.VALUE_RANGE, opt)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
//                                startActivity(new Intent(view.getContext(), HomeActivity.class));
                                utils.showFragment(view.getContext(), new JumlahPendudukFragment());
                                getActivity().setTitle("Demografi");
                            }
                        } catch (JSONException e) {
                            dialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.dismiss();
                        Toast.makeText(view.getContext().getApplicationContext(), "Oppss, Error", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewNextPembetasBarrier:
                if (type_opt.equals("") || type_opt.equals("-Pembatas (Barrier)-")) {
                    Toast.makeText(view.getContext().getApplicationContext(), params.MValues.option_msg, Toast.LENGTH_LONG).show();
                } else if (opt.equals("")) {
                    Toast.makeText(view.getContext().getApplicationContext(), params.MValues.option_msg, Toast.LENGTH_LONG).show();
                } else {
                    saveData(params.URL_METHOD.SAVE_SITE_ANALYSIS);
                }
                break;
        }
    }
}
