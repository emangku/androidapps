package com.ck.it.androidapps.activity.main;


import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.telephony.SmsManager;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.fragment.history.AreaAnalysisFragments;
import com.ck.it.androidapps.fragment.history.MappingAreaFragments;
import com.ck.it.androidapps.fragment.history.SiteAnalysisFragments;
import com.ck.it.androidapps.fragment.history.SiteInformationFragments;
import com.ck.it.androidapps.fragment.history.VideoFragments;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;


@SuppressWarnings("deprecation")
public class HistoryActivity extends AppCompatActivity {
    HistoryActivity historyActivity;
    BottomNavigationView bottomNavigationView;
    TabLayout tabLayout;
    ViewPager viewPager;
    FragmentAdapter adapter;
    Fragment fragment;
    String status = "";
    SmsManager smsManager;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String TAG = "MainActivity";
    String USER_ID = "";
    String APPROVER_1 = "";
    String APPROVER_2 = "";
    String APPROVER_3 = "";
    String APPROVER_4 = "";
    String APPROVER_5 = "";
    String APPROVER_6 = "";
    String APPROVER_7 = "";
    String APPROVER_8 = "";
    String BOD = "";
    String SITE_CODE = "";
    String LATITUDE = "";
    String LONGITUDE = "";
    String ALAMAT_A = "";
    String DESA = "";
    String KECAMATAN = "";
    String KABUPATEN = "";
    String PROVINSI = "";
    String KODE_POS = "";
    String STATUS = "";
    String COMMENT = "";
    String created_at = "";
    String updated_at = "";
    String num_direction = "";
    String num_origin = "";
    String val_approve = "";
    String info = "";
    String msg = "";
    String app1 = "";
    String app2 = "";
    String app3 = "";
    String app4 = "";
    String app5 = "";
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ProgressBar mRegistrationProgressBar;
    private TextView mInformationTextView;
    private boolean isReceiverRegistered;
    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.approve:
                    status = "1";
                    info = "APPROVE";
                    add();
                    return true;
                case R.id.hold:
                    status = "2";
                    info = "HOLD";
                    add();
                    return true;
                case R.id.reject:
                    status = "3";
                    info = "REJECT";
                    add();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        initView();
    }

    void initView() {
        bottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        tabLayout = (TabLayout) findViewById(R.id.tab_layout);

        msg = "SITE LOCATION " + utils.getDataSession(HistoryActivity.this, "sessionValKeySiteCode") + "\n"
                + "Telah di " + info + " oleh : " + utils.getDataSession(HistoryActivity.this, "sessionUserLogin");

        smsManager = SmsManager.getDefault();
        viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setAdapter(new FragmentAdapter(getSupportFragmentManager()));
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        showDataApproval("get_approval");
        System.out.println("MAKER : " + utils.getDataSession(HistoryActivity.this, "sessionSTATUS"));
        System.out.println("APPROVAL : " + utils.getDataSession(HistoryActivity.this, "sessionSTATUS"));
        if (utils.getDataSession(HistoryActivity.this, "sessionSTATUS").equals("MAKER")) {
            bottomNavigationView.setVisibility(View.GONE);
        } else if (utils.getDataSession(HistoryActivity.this, "sessionSTATUS").equals("APPROVAL")) {
            bottomNavigationView.setVisibility(View.VISIBLE);
            if (utils.getDataSession(HistoryActivity.this, "sessionUSER_STATUS").equals("APPROVAL1")) {
                app1 = "1";
                app2 = "0";
                app3 = "0";
                app4 = "0";
                app5 = "0";
                System.out.println(app1);
                System.out.println(app2);
                System.out.println(app3);
                System.out.println(app4);
                System.out.println(app5);
                num_direction = "087878760877";
                num_origin = "081905043077";
//                num_direction = "081286541280";
//                num_origin = "081286541280";
            } else if (utils.getDataSession(HistoryActivity.this, "sessionUSER_STATUS").equals("APPROVAL2")) {
                app1 = APPROVER_1;
                app2 = "1";
                app3 = "0";
                app4 = "0";
                app5 = "0";
                System.out.println(app1);
                System.out.println(app2);
                System.out.println(app3);
                System.out.println(app4);
                System.out.println(app5);
                num_direction = "081905043005";
                num_origin = "087878760877";
//                num_direction = "081286541280";
//                num_origin = "081286541280";
            } else if (utils.getDataSession(HistoryActivity.this, "sessionUSER_STATUS").equals("APPROVAL3")) {
                app1 = APPROVER_1;
                app2 = APPROVER_2;
                app3 = "1";
                app4 = "0";
                app5 = "0";
                System.out.println(app1);
                System.out.println(app2);
                System.out.println(app3);
                System.out.println(app4);
                System.out.println(app5);
                num_direction = "081905043000";
                num_origin = "081905043001";
//                num_direction = "081286541280";
//                num_origin = "081286541280";
            } else if (utils.getDataSession(HistoryActivity.this, "sessionUSER_STATUS").equals("APPROVAL4")) {
                app1 = APPROVER_1;
                app2 = APPROVER_2;
                app3 = APPROVER_3;
                app4 = "1";
                app5 = "0";
                System.out.println(app1);
                System.out.println(app2);
                System.out.println(app3);
                System.out.println(app4);
                System.out.println(app5);
                num_direction = "081905043000";
                num_origin = "081905043001";
//                num_direction = "081286541280";
//                num_origin = "081286541280";
            } else if (utils.getDataSession(HistoryActivity.this, "sessionUSER_STATUS").equals("APPROVAL5")) {
                app1 = APPROVER_1;
                app2 = APPROVER_2;
                app3 = APPROVER_3;
                app4 = APPROVER_4;
                app5 = "1";
                System.out.println(app1);
                System.out.println(app2);
                System.out.println(app3);
                System.out.println(app4);
                System.out.println(app5);
                num_direction = "";
                num_origin = "";
            }
        }
//https://android.jlelse.eu/android-adding-badge-or-count-to-the-navigation-drawer-84c93af1f4d9
//        if (APPROVER_1.equals("0")){
//            bottomNavigationView.setVisibility(View.VISIBLE);
//            if (utils.getDataSession(HistoryActivity.this,"sessionUSER_STATUS").equals("APPROVAL1")){
////                num_direction = "087878760877";
////                num_origin = "081905043077";
//                num_direction = "081286541280";
//                num_origin = "081286541280";
//            }
//        }
//
//        if (APPROVER_2.equals("0")){
//            bottomNavigationView.setVisibility(View.VISIBLE);
//            if (utils.getDataSession(HistoryActivity.this,"sessionUSER_STATUS").equals("APPROVAL2")){
////                num_direction = "081905043005";
////                num_origin = "087878760877";
//                num_direction = "081286541280";
//                num_origin = "081286541280";
//            }
//        }
//        if (APPROVER_3.equals("0")){
//            bottomNavigationView.setVisibility(View.VISIBLE);
//            if (utils.getDataSession(HistoryActivity.this,"sessionUSER_STATUS").equals("APPROVAL3")){
////                num_direction = "081905043001";
////                num_origin = "081905043005";
//                num_direction = "081286541280";
//                num_origin = "081286541280";
//            }
//        }
//        if (APPROVER_4.equals("0")){
//            bottomNavigationView.setVisibility(View.VISIBLE);
//            if (utils.getDataSession(HistoryActivity.this,"sessionUSER_STATUS").equals("APPROVAL4")){
////                num_direction = "081905043000";
////                num_origin = "081905043001";
//                num_direction = "081286541280";
//                num_origin = "081286541280";
//            }
//        }
//
//        if (APPROVER_5.equals("0")){
//            bottomNavigationView.setVisibility(View.VISIBLE);
//            if (utils.getDataSession(HistoryActivity.this,"sessionUSER_STATUS").equals("APPROVAL4")){
//                num_direction = "";
//                num_origin = "";
//            }
//        }
//        if (APPROVER_6.equals("0")){
//            bottomNavigationView.setVisibility(View.VISIBLE);
//        }else{
//            bottomNavigationView.setVisibility(View.GONE);
//        }
//        if (APPROVER_7.equals("0")){
//            bottomNavigationView.setVisibility(View.VISIBLE);
//        }else{
//            bottomNavigationView.setVisibility(View.GONE);
//        }
//        if (APPROVER_8.equals("0")){
//            bottomNavigationView.setVisibility(View.VISIBLE);
//        }else{
//            bottomNavigationView.setVisibility(View.GONE);
//        }
    }

    public class FragmentAdapter extends FragmentStatePagerAdapter {
        private FragmentManager fragmentManager;
        private String[] titleTabs =
                new String[]{"Site Information",
                        "Site Analysis",
                        "Area Analysis",
                        "Mapping Area",
                        "Squence Video"};

        public FragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titleTabs[position];
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new SiteInformationFragments();
                case 1:
                    return new SiteAnalysisFragments();
                case 2:
                    return new AreaAnalysisFragments();
                case 3:
                    return new MappingAreaFragments();
                case 4:
                    return new VideoFragments();
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return titleTabs.length;
        }
    }


    void updateApprove(String url, String SITECODE,
                       String USERID, String APV1,
                       String APV2, String APV3,
                       String APV4, String APV5,
                       String STATUS, String comment, final AlertDialog dialog) {
        //utils.getDataSession(HistoryActivity.this, "sessionSiteCode")
        // utils.getDataSession(HistoryActivity.this, "sessionUserID")
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, SITECODE)
                .addBodyParameter(params.USER_ID, USERID)
                .addBodyParameter("APPROVER_1", APV1)
                .addBodyParameter("APPROVER_2", APV2)
                .addBodyParameter("APPROVER_3", APV3)
                .addBodyParameter("APPROVER_4", APV4)
                .addBodyParameter("APPROVER_5", APV5)
                .addBodyParameter("STATUS", STATUS)
                .addBodyParameter("COMMENT", comment)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("ture")) {
//                                smsManager.sendTextMessage(num_direction, num_origin, msg, null, null);
//                                Toast.makeText(getApplicationContext(), "Pesan telah terkirim", Toast.LENGTH_LONG).show();
                                dialog.cancel();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.cancel();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        dialog.cancel();
                        Toast.makeText(HistoryActivity.this, "Opps Error : " + anError.getMessage(), Toast.LENGTH_LONG).show();
                    }
                });
    }

    void showDataApproval(String url) {
        final ProgressDialog dialog = utils.showLoadDialog(HistoryActivity.this, params.MValues.loading_msg);
        dialog.show();
        AndroidNetworking.post(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(HistoryActivity.this, "sessionValKeySiteCode"))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                if (!response.getJSONObject("data").getString("SITE_CODE").equals("null")) {
                                    SITE_CODE = response.getJSONObject("data").getString("SITE_CODE");
                                }
                                if (!response.getJSONObject("data").getString("USER_ID").equals("null")) {
                                    USER_ID = response.getJSONObject("data").getString("USER_ID");
                                }
                                if (!response.getJSONObject("data").getString("LATITUDE").equals("null")) {
                                    LATITUDE = response.getJSONObject("data").getString("LATITUDE");
                                }
                                if (!response.getJSONObject("data").getString("LONGITUDE").equals("null")) {
                                    LONGITUDE = response.getJSONObject("data").getString("LONGITUDE");
                                }
                                if (!response.getJSONObject("data").getString("ALAMAT_A").equals("")) {
                                    ALAMAT_A = response.getJSONObject("data").getString("ALAMAT_A");
                                }

                                if (!response.getJSONObject("data").getString("DESA").equals("null")) {
                                    DESA = response.getJSONObject("data").getString("DESA");
                                }
                                if (!response.getJSONObject("data").getString("KECAMATAN").equals("null")) {
                                    KECAMATAN = response.getJSONObject("data").getString("KECAMATAN");
                                }
                                if (!response.getJSONObject("data").getString("KABUPATEN").equals("null")) {
                                    KABUPATEN = response.getJSONObject("data").getString("KABUPATEN");
                                }
                                if (!response.getJSONObject("data").getString("PROVINSI").equals("null")) {
                                    PROVINSI = response.getJSONObject("data").getString("PROVINSI");
                                }
                                if (!response.getJSONObject("data").getString("KODE_POS").equals("null")) {
                                    KODE_POS = response.getJSONObject("data").getString("KODE_POS");
                                }
                                if (!response.getJSONObject("data").getString("created_at").equals("null")) {
                                    created_at = response.getJSONObject("data").getString("created_at");
                                }
                                if (!response.getJSONObject("data").getString("updated_at").equals("null")) {
                                    updated_at = response.getJSONObject("data").getString("updated_at");
                                }
                                if (!response.getJSONObject("data").getString("APPROVER_1").equals("null")) {
                                    APPROVER_1 = response.getJSONObject("data").getString("APPROVER_1");
                                    if (APPROVER_1.equals("1")) {
                                        bottomNavigationView.setVisibility(View.GONE);
                                    }
                                }
                                if (!response.getJSONObject("data").getString("APPROVER_2").equals("null")) {
                                    APPROVER_2 = response.getJSONObject("data").getString("APPROVER_2");
                                    if (APPROVER_1.equals("1")) {
                                        bottomNavigationView.setVisibility(View.GONE);
                                    }
                                }
                                if (!response.getJSONObject("data").getString("APPROVER_3").equals("null")) {
                                    APPROVER_3 = response.getJSONObject("data").getString("APPROVER_3");
                                    if (APPROVER_1.equals("1")) {
                                        bottomNavigationView.setVisibility(View.GONE);
                                    }
                                }
                                if (!response.getJSONObject("data").getString("APPROVER_4").equals("null")) {
                                    APPROVER_4 = response.getJSONObject("data").getString("APPROVER_4");
                                    if (APPROVER_1.equals("1")) {
                                        bottomNavigationView.setVisibility(View.GONE);
                                    }
                                }
                                if (!response.getJSONObject("data").getString("APPROVER_5").equals("null")) {
                                    APPROVER_5 = response.getJSONObject("data").getString("APPROVER_5");
                                    if (APPROVER_1.equals("1")) {
                                        bottomNavigationView.setVisibility(View.GONE);
                                    }
                                }
                                if (!response.getJSONObject("data").getString("STATUS").equals("null")) {
                                    STATUS = response.getJSONObject("data").getString("STATUS");
                                }
                                if (!response.getJSONObject("data").getString("COMMENT").equals("null")) {
                                    COMMENT = response.getJSONObject("data").getString("COMMENT");
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(getApplicationContext(), "Opps Error A " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
    }

    void add() {
        final View view = LayoutInflater.from(HistoryActivity.this).inflate(R.layout.popup_dialog_action, null);
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(HistoryActivity.this);
        mBuilder.setTitle("Comment");
        mBuilder.setView(view);
        final AlertDialog dialog = mBuilder.create();
        final AppCompatEditText viewComment = (AppCompatEditText) view.findViewById(R.id.viewComment);
        LinearLayout viewCancel = (LinearLayout) view.findViewById(R.id.viewCancel);
        LinearLayout viewApproved = (LinearLayout) view.findViewById(R.id.viewApproved);


        viewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        viewApproved.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                updateApprove("approve",
//                        utils.getDataSession(HistoryActivity.this, "sessionSiteCode"),
//                        utils.getDataSession(HistoryActivity.this, "sessionUserID"),
//                        app1,app2,app3,app4,app5,status,viewComment.getText().toString(),dialog);
                AndroidNetworking.post(api.SERVER_API + "approve")
                        .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                        .addBodyParameter(params.SITE_CODE, utils.getDataSession(HistoryActivity.this, "sessionValKeySiteCode"))
                        .addBodyParameter(params.USER_ID, utils.getDataSession(HistoryActivity.this, "sessionValKeyUserID"))
                        .addBodyParameter("APPROVER_1", app1)
                        .addBodyParameter("APPROVER_2", app2)
                        .addBodyParameter("APPROVER_3", app3)
                        .addBodyParameter("APPROVER_4", app4)
                        .addBodyParameter("APPROVER_5", app5)
                        .addBodyParameter("STATUS", STATUS)
                        .addBodyParameter("COMMENT", viewComment.getText().toString())
                        .build()
                        .getAsJSONObject(new JSONObjectRequestListener() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getString("status").equals("true")) {
                                        dialog.cancel();
                                        smsManager.sendTextMessage(num_direction, num_origin, msg, null, null);
                                        showDataApproval("get_approval");
//                                            Toast.makeText(getApplicationContext(), "Pesan telah terkirim", Toast.LENGTH_LONG).show();

                                    } else if (response.getString("status").equals("false")) {
                                        Toast.makeText(getApplicationContext(), "Failed..", Toast.LENGTH_LONG).show();
                                        dialog.cancel();
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    dialog.cancel();
                                }
                            }

                            @Override
                            public void onError(ANError anError) {
                                dialog.cancel();
                                System.out.println("ERROR : " + anError.toString());
                                Toast.makeText(HistoryActivity.this, "Opps Error : " + anError.getMessage(), Toast.LENGTH_LONG).show();
                            }
                        });
            }
        });
        dialog.show();
    }
}
