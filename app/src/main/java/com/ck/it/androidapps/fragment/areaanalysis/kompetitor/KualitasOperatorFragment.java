package com.ck.it.androidapps.fragment.areaanalysis.kompetitor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ck.it.androidapps.R;

/**
 * Created by Creef on 1/15/2018.
 */

public class KualitasOperatorFragment  extends Fragment {
    private View view;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.kualitas_operato_fragment, container, false);
        return super.onCreateView(inflater, container, savedInstanceState);
    }
}
