package com.ck.it.androidapps.activity.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.util.utils;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Creef on 3/26/2018.
 */

@SuppressWarnings("deprecation")
public class BottomSheetFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private View view;
    Button viewAddComment;
    LinearLayout viewComentLin;
    ProgressBar viewProgress;
    ImageView viewRefresh;
    TextView txt;
    String data = "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.";
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.content_comments, container, false);
        init();
        return view;
    }

    void init() {
        viewAddComment = (Button) view.findViewById(R.id.viewAddComment);
        viewAddComment.setOnClickListener(this);
        viewComentLin = (LinearLayout) view.findViewById(R.id.viewComentLin);
        viewProgress = (ProgressBar) view.findViewById(R.id.viewProgress);
        viewRefresh = (ImageView) view.findViewById(R.id.viewRefresh);
        viewRefresh.setOnClickListener(this);
        txt = (TextView)view.findViewById(R.id.viewTxtComent);
        txt.setText(Html.fromHtml("<p align='justify'>"+ data +"</p>"));

    }

    void showData(String url) {
        viewAddComment.setVisibility(View.GONE);
        viewComentLin.setVisibility(View.GONE);
        viewRefresh.setVisibility(View.GONE);
        viewProgress.setVisibility(View.VISIBLE);

        AndroidNetworking.post(api.SERVER_API + url)
                .addBodyParameter(params.SITE_CODE, utils.getDataSession(view.getContext(), ""))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                viewProgress.setVisibility(View.GONE);
                                viewAddComment.setVisibility(View.VISIBLE);
                                viewComentLin.setVisibility(View.VISIBLE);
                                viewRefresh.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            viewProgress.setVisibility(View.GONE);
                            viewAddComment.setVisibility(View.VISIBLE);
                            viewComentLin.setVisibility(View.VISIBLE);
                            viewRefresh.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(view.getContext(), "Oppss Error : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                        viewProgress.setVisibility(View.GONE);
                        viewAddComment.setVisibility(View.VISIBLE);
                        viewComentLin.setVisibility(View.VISIBLE);
                        viewRefresh.setVisibility(View.VISIBLE);
                    }
                });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewAddComment:
                break;
            case R.id.viewRefresh:
                showData("");
                break;
        }
    }
}
