package com.ck.it.androidapps.activity.maps;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.ck.it.androidapps.R;
import com.ck.it.androidapps.activity.main.HistoryActivity;
import com.ck.it.androidapps.gpstracker.GpsTracker;
import com.ck.it.androidapps.helpers.ScrrenCapturType;
import com.ck.it.androidapps.helpers.api;
import com.ck.it.androidapps.helpers.params;
import com.ck.it.androidapps.ssl.SSLSocket;
import com.ck.it.androidapps.util.PermissionUtils;
import com.ck.it.androidapps.util.utils;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.*;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import github.nisrulz.screenshott.ScreenShott;

@SuppressWarnings("deprecation")
public class MappingAreaActivity extends FragmentActivity
        implements OnMapReadyCallback,
        GoogleMap.OnMyLocationButtonClickListener,
        GoogleMap.OnMyLocationChangeListener,
        ActivityCompat.OnRequestPermissionsResultCallback,
        View.OnClickListener {
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    double latitude, longitude;
    GpsTracker gps;
    private boolean mPermissionDenied = false;
    private GoogleMap mMap;
    Location vlocation;
    LatLng position;
    PolygonOptions options;
    String myPoliLine;
    Toolbar toolbar;
    SupportMapFragment mapFragment;
    LinearLayout viewTakeCaputureView;
    ImageView viewCapture;
    File file, saveFile;
    Uri uri;
    Bitmap viewRootCapture;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapping_area);
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        init();
    }

    void init() {
        viewTakeCaputureView = (LinearLayout) findViewById(R.id.viewTakeCaputureView);
        viewCapture = (ImageView) findViewById(R.id.viewCapture);
        viewCapture.setOnClickListener(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.mipmap.mark);
        position = new LatLng(-6.2201346, 106.8640497);
//        mMap.addMarker(new MarkerOptions().position(position).icon(icon));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16.f));
        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        CameraPosition.Builder builder = new CameraPosition.Builder();
        builder.zoom(16.f);
        builder.target(position);
        enableMyLocation();


    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(this, LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMyLocationButtonEnabled(true);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.map_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onMyLocationButtonClick() {
        gps = new GpsTracker(MappingAreaActivity.this);
        if (gps.canGetLocation()) {
            latitude = gps.getLatitude();
            longitude = gps.getLongitude();
            this.onMyLocationChange(gps.getLocation());
        } else {
            gps.showSettingAlerts();
        }
        return false;
    }

    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        if (mPermissionDenied) {
            showMissingPermissionError();
            mPermissionDenied = false;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getSupportFragmentManager(), "dialog");
    }

    @Override
    public void onMyLocationChange(Location location) {
        location = gps.getLocation();
        BitmapDescriptor icon = BitmapDescriptorFactory.fromResource(R.mipmap.mark);
        position = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().position(position).icon(icon));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16.f));
        mMap.setBuildingsEnabled(true);
        mMap.setIndoorEnabled(true);
        mMap.getUiSettings().setIndoorLevelPickerEnabled(true);
        mMap.setOnMyLocationButtonClickListener(this);
        CameraPosition.Builder builder = new CameraPosition.Builder();
        builder.zoom(16.f);
        builder.target(position);
        enableMyLocation();
    }

    void addPolygon() {
        options = new PolygonOptions();
        options.strokeColor(R.color.colorPrimary);
        options.add(position);
        options.strokeWidth(10000);
        mMap.addPolygon(options);
    }

    void optMenu() {
        PopupMenu popup = new PopupMenu(MappingAreaActivity.this, viewCapture);
        popup.getMenuInflater().inflate(R.menu.capture_opt, popup.getMenu());
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.viewFull:
//                        takeScreenCapture(ScrrenCapturType.FULL);
                        break;
                    case R.id.viewCustome:
//                        takeScreenCapture(ScrrenCapturType.CUSTOME);
                        break;
                }
                return true;
            }
        });
        popup.show();
    }

    void screenCapture() {
        String lat = "Tst_123";
        View view = getWindow().getDecorView().getRootView();
        view.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(view.getDrawingCache());
        try {
            saveToGallery(lat, bitmap);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    void saveToGallery(String imgName, Bitmap bm) throws IOException {
        File path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES + "/screen"); //Creates app specific folder
        path.mkdirs();
        File imageFile = new File(path, imgName + ".png");
        uploadMappingArea(params.URL_METHOD.SAVE_MAPPING_AREA, imageFile);
        FileOutputStream out = new FileOutputStream(imageFile);
        System.out.print(out.getChannel());
        try {
            bm.compress(Bitmap.CompressFormat.PNG, 100, out); // Compress Image
            out.flush();
            out.close();
        } catch (Exception e) {
            throw new IOException();
        }
    }

    void takeImage() {
        try {
//            viewRootCapture = ScreenShott.getInstance().takeScreenShotOfRootView(mMap);
            file = ScreenShott.getInstance().saveScreenshotToPicturesFolder(MappingAreaActivity.this, viewRootCapture,
                    utils.getDataSession(this, "sessionSiteCode") + "_" +
                            utils.getDataSession(this, "sessionUserID") + "_" +
                            latitude + "_" + longitude + ".jpg");
            String f = file.getAbsolutePath();
            File file1 = new File(f);
            uploadMappingArea(params.URL_METHOD.SAVE_MAPPING_AREA, file1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void snapShootTakeImage() {
        GoogleMap.SnapshotReadyCallback callback = new GoogleMap.SnapshotReadyCallback() {
            @Override
            public void onSnapshotReady(Bitmap bitmap) {
                viewRootCapture = bitmap;
                OutputStream outputStream = null;
                String path = System.currentTimeMillis() + ".jpeg";

                try {
                    outputStream = openFileOutput(path, MODE_WORLD_READABLE);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outputStream);
                    outputStream.flush();
                    outputStream.close();
                    file = getFileStreamPath(path);
                    uploadMappingArea(params.URL_METHOD.SAVE_MAPPING_AREA, file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        };
        mMap.snapshot(callback);
    }

    void uploadMappingArea(String url, File files) {
        final ProgressDialog dialog = utils.showLoadDialog(MappingAreaActivity.this, params.MValues.uploading_msg);
        dialog.show();
        AndroidNetworking.upload(api.SERVER_API + url)
                .setOkHttpClient(SSLSocket.getUnsafeOkHttpClient().build())
                .addMultipartParameter("SITE_CODE", utils.getDataSession(MappingAreaActivity.this, "sessionSiteCode"))
                .addMultipartParameter("USER_ID", utils.getDataSession(MappingAreaActivity.this, "sessionUserID"))
                .addMultipartFile(params.IMAGE, files)
                .addMultipartParameter(params.LATITUDE, String.valueOf(latitude))
                .addMultipartParameter(params.LONGITUDE, String.valueOf(longitude))
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            if (response.getString("status").equals("true")) {
                                dialog.dismiss();
                                startActivity(new Intent(MappingAreaActivity.this, HistoryActivity.class));
                                finish();
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            dialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(MappingAreaActivity.this, "Opps error... : " + anError.getMessage(), Toast.LENGTH_SHORT).show();
                        dialog.dismiss();
                    }
                });
    }

    private void takeScreenCapture(ScrrenCapturType scrrenCapturType) {
        Bitmap b = null;
        switch (scrrenCapturType) {
            case FULL:
                b = utils.getScreenShot(viewTakeCaputureView);
                break;
            case CUSTOME:
                b = utils.getScreenShot(viewTakeCaputureView);
                if (b != null) {
                    saveFile = utils.getMainDirectoryName(this);
                    file = utils.store(b,
                            utils.getDataSession(this, "sessionSiteCode") + "_" +
                                    utils.getDataSession(this, "sessionUserID") + "_" +
                                    latitude + "_" + longitude + "" + scrrenCapturType + ".jpg", saveFile);
                } else {
                    Toast.makeText(this, "Opps...", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }

    private void shareScreenshot(File file) {
        Uri uri = Uri.fromFile(file);
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("image/*");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, "");
        intent.putExtra(Intent.EXTRA_STREAM, uri);//pass uri here
        startActivity(Intent.createChooser(intent, ""));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.viewCapture:
                snapShootTakeImage();
                break;
        }
    }
}
